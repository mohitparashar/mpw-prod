(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.emergency_notification = {

    /**
     * Attach method for this behavior.
     *
     * @param {object} context
     *   The context for which the behavior is being executed. This is either
     *   the full page or a piece of HTML that was just added through Ajax.
     * @param {object} settings
     *   An array of settings (added through drupal_add_js()). Instead of
     *   accessing Drupal.settings directly you should use this because of
     *   potential modifications made by the Ajax callback that also produced
     *   'context'.
     */
    attach: function (context, settings) {

      if (!$.isEmptyObject(settings.emergency_notification_settings)) {
        var en_settings = settings.emergency_notification_settings;

        if (typeof en_settings.enable_notification !== 'undefined' &&
          en_settings.enable_notification) {
          var self = this;

          self.setCookies(context, en_settings);
          self.tabLink(context, en_settings);
          self.dismissPopup(context);
          self.togglePopup(context);
        }
      }
    },

    /**
     * Settings emergency notification cookies.
     *
     * @param {object} context
     *   The context for which the behavior is being executed. This is either
     *   the full page or a piece of HTML that was just added through Ajax.
     * @param {object} en_settings
     *   An array of emergency notification settings.
     */
    setCookies: function (context, en_settings) {

      if (typeof en_settings.form_submission !== 'undefined' &&
        en_settings.form_submission !== $.cookie('emergency_notification_id')) {

        $.cookie('emergency_notification_dismiss', 0);
        $.cookie('emergency_notification_id', en_settings.form_submission);
      }
    },

    /**
     * Adds tabbable link to top of body.
     *
     * @param {object} context
     *   The context for which the behavior is being executed. This is either
     *   the full page or a piece of HTML that was just added through Ajax.
     * @param {object} en_settings
     *   An array of emergency notification settings.
     */
    tabLink: function (context, en_settings) {

      if (typeof en_settings.notice_title !== 'undefined' &&
        !$.cookie('emergency_notification_dismiss')) {
        var title = en_settings.notice_title;

        $('body', context).prepend(
          '<a href="#emergency-popup" class="emergency-popup-tabbable ' +
          'visually-hidden focusable skip-link">' + title + '</a>');
      }
    },

    /**
     * Dismisses emergency notification.
     *
     * @param {object} context
     *   The context for which the behavior is being executed. This is either
     *   the full page or a piece of HTML that was just added through Ajax.
     */
    dismissPopup: function (context) {

      $('#emergency-popup-wrapper .dismiss-btn', context).click(function () {
        $('#emergency-notification', context)
          .removeClass('open-popup')
          .addClass('dismissed');

        $('#emergency-popup-wrapper .emergency-popup', context).slideUp();

        $.cookie('emergency_notification_dismiss', 1);
        $('.emergency-popup-tabbable', context).remove();

        return false;
      });
    },

    /**
     * Adds JS functionality for toggling emergency notification.
     *
     * @param {object} context
     *   The context for which the behavior is being executed. This is either
     *   the full page or a piece of HTML that was just added through Ajax.
     */
    togglePopup: function (context) {

      if ($.cookie('emergency_notification_dismiss') === '0') {
        $('#emergency-notification', context).addClass('open-popup');
        $('#emergency-notification', context).removeClass('dismissed');
      }

      $('#emergency-popup-wrapper .open-btn', context).click(function () {
        $('#emergency-popup-wrapper .emergency-popup', context).slideToggle();

        return false;
      });
    }
  };
})(jQuery, Drupal);
