<?php

namespace Drupal\emergency_notification;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines EmergencyNotificationManager class.
 */
class EmergencyNotificationManager {

  /**
   * The emergency_notification.settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a EmergencyNotificationManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('emergency_notification.settings');
  }

  /**
   * Gets CSS file content data.
   *
   * @return string
   *   Returns configurable CSS as string.
   */
  public function getCss() {
    return $this->buildConfigCss();
  }

  /**
   * Gets emergency_notification.settings config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Return configuration settings for emergency_notification.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Builds configuration CSS off settings.
   *
   * @return string
   *   Return CSS string.
   */
  private function buildConfigCss() {
    $css_content = '';

    $css_items = [
      'overlay' => [
        'selectors' => [
          'div#emergency-popup-overlay',
        ],
        'property_values' => [
          'background-color' => $this->config->get('overlay_color'),
        ],
      ],
      'background' => [
        'selectors' => [
          'div.open-popup .emergency-popup',
          'div.dismissed #emergency-popup-wrapper',
          'div.dismissed button.open-btn',
        ],
        'property_values' => [
          'background-color' => $this->config->get('background_color'),
        ],
      ],
      'foreground' => [
        'selectors' => [
          'div.dismissed button.open-btn',
          'div#emergency-notification .emergency-popup h6',
          'div#emergency-notification .emergency-popup h5',
          'div#emergency-notification .emergency-popup h4',
          'div#emergency-notification .emergency-popup h3',
          'div#emergency-notification .emergency-popup h2',
          'div#emergency-notification .emergency-popup h1',
          'div#emergency-notification .emergency-popup a',
          'div#emergency-notification .emergency-popup',
        ],
        'property_values' => [
          'color' => $this->config->get('foreground_color'),
        ],
      ],
    ];

    // Loop thought CSS representational items to build CSS content.
    foreach ($css_items as $css_item) {
      $seletors = implode(',', $css_item['selectors']);
      $property_values = '';

      foreach ($css_item['property_values'] as $property => $value) {
        $property_values .= "{$property}: $value;";
      }

      $css_content .= "{$seletors} {{$property_values}}";
    }

    return $css_content;
  }

}
