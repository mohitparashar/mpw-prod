(function ($, Drupal) {

  "use strict";

  var county = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  // prefetch: '/mpw-solrautocomplete-county?county=los angeles',
	  remote: {
	    url: '/mpw-solrautocomplete-county?county=%QUERY',
	    wildcard: '%QUERY'
	  },
	  cache: false,
      limit: 10
	});

  var address = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  // prefetch: '/mpw-solrautocomplete-address?address=los angeles',
	  remote: {
	    url: '/mpw-solrautocomplete-address?address=%QUERY',
	    wildcard: '%QUERY'
	  },
	  cache: false,
      limit: 10
	});

  var zip = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  // prefetch: '/mpw-solrautocomplete-zip?zip=los angeles',
	  remote: {
	    url: '/mpw-solrautocomplete-zip?zip=%QUERY',
	    wildcard: '%QUERY'
	  },
	  cache: false,
      limit: 10
	});

  var city = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  // prefetch: '/mpw-solrautocomplete-city?city=los angeles',
	  remote: {
	    url: '/mpw-solrautocomplete-city?city=%QUERY',
	    wildcard: '%QUERY',
	  },
	  cache: false,
      limit: 10
	});


	jQuery(document).ready(function(){
		jQuery('#multiple-datasets .typeahead').typeahead({
		  minLength: 1,
		  highlight: true,
		},
		{
		  name: 'city',
		  display: 'city',
		  source: city,
		  templates: {
		    header: '<h3 class="league-name">City</h3>'
		  }
		},
		{
		  name: 'address',
		  display: 'address',
		  source: address,
		  templates: {
		    header: '<h3 class="league-name">Address</h3>'
		  }
		},
		{
		  name: 'county',
		  display: 'county',
		  source: county,
		  templates: {
		    header: '<h3 class="league-name">County</h3>'
		  }
		},
		{
		  name: 'zip',
		  display: 'zip',
		  source: zip,
		  templates: {
		    header: '<h3 class="league-name">Zip</h3>'
		  }
		}
		).bind('typeahead:selected', function(obj, selected, name) {
		    jQuery("#multiple-datasets").submit();
		    return false;
		});
	});

})(jQuery, Drupal);



