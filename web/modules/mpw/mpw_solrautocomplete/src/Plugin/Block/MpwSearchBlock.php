<?php
/**
 * @file
 * Contains \Drupal\mpw_solrautocomplete\Plugin\Block\MpwSearchBlock.
 */
namespace Drupal\mpw_solrautocomplete\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
/**
 * Provides a 'mpw search' block.
 *
 * @Block(
 *   id = "mpw_search_block",
 *   admin_label = @Translation("Mpw Search block"),
 *   category = @Translation("MPW")
 * )
 */
class MpwSearchBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\mpw_solrautocomplete\Form\MpwAutoComplete');
    $form['#id'] = 'multiple-datasets';
    $form['#attached']['library'][] = 'mpw_solrautocomplete/typeahead';

    return $form;
   }
}