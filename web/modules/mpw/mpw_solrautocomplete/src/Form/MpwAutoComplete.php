<?php  
/**  
 * @file  
 * Contains Drupal\mpw_solrautocomplete\Form\SolrAutoComplete.  
 */  
namespace Drupal\mpw_solrautocomplete\Form; 

use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;  

class MpwAutoComplete extends ConfigFormBase { 

  /**  
   * {@inheritdoc}  
   */  
  protected function getEditableConfigNames() {  
    return [  
     'mpw.autocomplete',  
    ];  
  }  

  /**  
  * {@inheritdoc}  
  */  
  public function getFormId() {  
    return 'mpw_autocomplete';  
  }  

	/**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $keyword = \Drupal::request()->get('keyword');

    $form['#attributes']['id'] = 'multiple-datasets';
    $form['keywords'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search by City or Zip Code.'),
      '#attributes' => ['class' => ['typeahead']],
      //'#title_display' => 'invisible',
      '#placeholder' => $this->t('Search by City or Zip Code.'),
      '#default_value' => $keyword,
      //'#autocomplete_route_name' => 'mpw_solrautocomplete.autocomplete',
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['#attached']['library'][] = 'mpw_solrautocomplete/typeahead';
    $form['#cache'] = ['max-age' => 0];
    $form['#cache']['contexts'][] = 'session';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $form_state->setRedirect('view.solr_property_listing.page_1', [
      'keyword' => $form_state->getValue('keywords'),
    ]);
  }
}  