<?php
/**  
 * @file  
 * Contains Drupal\mpw_solrautocomplete\Form\SolrAutoComplete.  
 */
namespace Drupal\mpw_solrautocomplete\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Minimalcode\Search\Criteria;


// class MpwSolr extends SolrConnectorPluginBase {
class MpwSolrAutoComplete {

 	public function handleAutoCompleteResult(Request $request) {

		// County -  Los Angeles - twm_X3b_en_dependent_locality 
		// City – Glendale - twm_X3b_en_locality
		// Address – 201 N. Brand Blvd - twm_X3b_en_address_line1
		// Zip: 91203  - twm_X3b_en_postal_code

		$results = [];
	    $input = '';

		$param = $request->query->all();

		if (isset($param['county'])) {
			$input = $param['county'];
			$section = 'twm_X3b_en_dependent_locality';
		}
		elseif (isset($param['address'])) {
			$input = $param['address'];
			$section = 'twm_X3b_en_address_line1';
		}
		elseif (isset($param['zip'])) {
			$input = $param['zip'];
			$section = 'twm_X3b_en_postal_code';
		}
		elseif (isset($param['city'])) {
			$input = $param['city'];
			$section = 'twm_X3b_en_locality';
		}

	    // Get the typed string from the URL, if it exists.
	    if (!$input) {
	      return new JsonResponse($results);
	    }
	    $with_space_input = Xss::filter($input);
	    $input = explode(" ", $with_space_input);

		if (count($input) > 1) {
	    	$criteria = Criteria::where($section)->sloppy($with_space_input, 2);
		}
		else {
			$criteria = Criteria::where($section)->startsWith($input[0]);
		}

 		$data = file_get_contents('http://138.68.8.96:8983/solr/mpw/select?q=' . urlencode($criteria->getQuery()) . '&wt=json');
		$res = json_decode($data);

		$results = [];
		if (isset($res->response->docs)) {
			foreach ($res->response->docs as $key => $value) {
				//array_push($results, $value->tm_X3b_en_address_line1[0]);
				$results[] = [
			      'county' => $value->twm_X3b_en_dependent_locality[0],
			      'address' => $value->twm_X3b_en_address_line1[0],
			      'zip' => $value->twm_X3b_en_postal_code[0],
			      'city' => $value->twm_X3b_en_locality[0],
				];
			}
		}
	    return new JsonResponse($results);
 	}

 	public function handleAutoCompleteResultCounty (Request $request) {

 		$results = [];
	    $input = '';

		$param = $request->query->all();
	    
		if (isset($param['county'])) {
			$input = $param['county'];
			$section = 'twm_X3b_en_dependent_locality';
		}

	    // Get the typed string from the URL, if it exists.
	    if (!$input) {
	      return new JsonResponse($results);
	    }
	    $with_space_input = Xss::filter($input);
	    $input = explode(" ", $with_space_input);
	    $input = array_filter($input);

		if (count($input) > 1) {
	    	//$criteria = Criteria::where($section)->sloppy($with_space_input, 2);
	    	$criteria = Criteria::where($section)->contains($input);
		}
		else {
			$criteria = Criteria::where($section)->startsWith($input[0]);
		}
	   
 		$data = file_get_contents('http://138.68.8.96:8983/solr/mpw/select?q=' . urlencode($criteria->getQuery()) . '&wt=json');
		$res = json_decode($data);

		$results = [];
		if (isset($res->response->docs)) {
			foreach ($res->response->docs as $key => $value) {
				//array_push($results, $value->tm_X3b_en_address_line1[0]);
				$results[] = [
			      'county' => str_replace(["."], [""], trim($value->twm_X3b_en_dependent_locality[0])),
				];
			}
		}

		$filter_results = $this->array_unique_multidimensional($results);
	    return new JsonResponse($filter_results);
 	}

 	public function handleAutoCompleteResultAddress (Request $request) {
 		$results = [];
	    $input = '';

		$param = $request->query->all();
	    
		if (isset($param['address'])) {
			$input = $param['address'];
			$section = 'twm_X3b_en_address_line1';
		}

	    // Get the typed string from the URL, if it exists.
	    if (!$input) {
	      return new JsonResponse($results);
	    }
	    $with_space_input = Xss::filter($input);
	    $input = explode(" ", $with_space_input);
	    $input = array_filter($input);

		if (count($input) > 1) {
	    	//$criteria = Criteria::where($section)->sloppy($with_space_input, 2);
	    	$criteria = Criteria::where($section)->contains($input);
		}
		else {
			$criteria = Criteria::where($section)->startsWith($input[0]);
		}
	   
 		$data = file_get_contents('http://138.68.8.96:8983/solr/mpw/select?q=' . urlencode($criteria->getQuery()) . '&wt=json');
		$res = json_decode($data);

		$results = [];
		if (isset($res->response->docs)) {
			foreach ($res->response->docs as $key => $value) {
				//array_push($results, $value->tm_X3b_en_address_line1[0]);
				$results[] = [
			      'address' => str_replace(".", "", trim($value->twm_X3b_en_address_line1[0])),
				];
			}
		}
		$filter_results = $this->array_unique_multidimensional($results);
	    return new JsonResponse($filter_results);
 	}

 	public function handleAutoCompleteResultZip (Request $request) {
 		$results = [];
	    $input = '';

		$param = $request->query->all();

		if (isset($param['zip'])) {
			$input = $param['zip'];
			$section = 'twm_X3b_en_postal_code';
		}

	    // Get the typed string from the URL, if it exists.
	    if (!$input) {
	      return new JsonResponse($results);
	    }
	    $with_space_input = Xss::filter($input);
	    $input = explode(" ", $with_space_input);
	    $input = array_filter($input);

		if (count($input) > 1) {
	    	//$criteria = Criteria::where($section)->sloppy($with_space_input, 2);
	    	$criteria = Criteria::where($section)->contains($input);
		}
		else {
			$criteria = Criteria::where($section)->startsWith($input[0]);
		}
	   
 		$data = file_get_contents('http://138.68.8.96:8983/solr/mpw/select?q=' . urlencode($criteria->getQuery()) . '&wt=json');
		$res = json_decode($data);

		$results = [];
		if (isset($res->response->docs)) {
			foreach ($res->response->docs as $key => $value) {
				//array_push($results, $value->tm_X3b_en_address_line1[0]);
				$results[] = [
			      'zip' => str_replace(".", "", trim($value->twm_X3b_en_postal_code[0])),
				];
			}
		}
		$filter_results = $this->array_unique_multidimensional($results);
	    return new JsonResponse($filter_results);
 	}

 	public function handleAutoCompleteResultCity (Request $request) {
 		$results = [];
	    $input = '';

		$param = $request->query->all();

		if (isset($param['city'])) {
			$input = $param['city'];
			$section = 'twm_X3b_en_locality';
		}

	    // Get the typed string from the URL, if it exists.
	    if (!$input) {
	      return new JsonResponse($results);
	    }
	    $with_space_input = Xss::filter($input);
	    $input = explode(" ", $with_space_input);
	    $input = array_filter($input);

		if (count($input) > 1) {
	    	//$criteria = Criteria::where($section)->sloppy($with_space_input, 3);
	    	$criteria = Criteria::where($section)->contains($input);
		}
		else {
			$criteria = Criteria::where($section)->startsWith($input[0]);
		}
	   //print_r($criteria);exit;
 		$data = file_get_contents('http://138.68.8.96:8983/solr/mpw/select?q=' . urlencode($criteria->getQuery()) . '&wt=json');
		$res = json_decode($data);

		$results = [];
		if (isset($res->response->docs)) {
			foreach ($res->response->docs as $key => $value) {
				//array_push($results, $value->tm_X3b_en_address_line1[0]);
				$results[] = [
			      'city' => str_replace(["."], [""], trim($value->twm_X3b_en_locality[0])),
				];
			}
			
			$filter_results = $this->array_unique_multidimensional($results);
		}
	    return new JsonResponse($filter_results);
 	}

 	function array_unique_multidimensional($input) {
    	$serialized = array_map('serialize', $input);
    	$unique = array_unique($serialized);
    	return array_intersect_key($input, $unique);
	}
}
