<?php

namespace Drupal\twig_custom_extension\Twig;

use Drupal\views\Views;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Render\Element;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;

/**
 * Provides functions within Twig templates.
 */
class TwigCustomExtension extends \Twig_Extension {
    /**
    * {@inheritdoc}
    */
    public function getName() {
        return 'twig_custom_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals() {
        return array(
            'get' => $_GET,
            'post' => $_POST,
            'session' => $_SESSION
        );
    }

    /**
    * {@inheritdoc}
    */
    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('var_dump', array($this, 'var_dump'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('echo_kint', array($this, 'echo_kint'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('entityQuery', array($this, 'entityQuery'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getTaxonomyTermById', array($this, 'getTaxonomyTermById'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getChildTerms', array($this, 'getChildTerms'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getTermRelatedNodeIds', array($this, 'getTermRelatedNodeIds'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('loadNode', array($this, 'loadNode'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getNodeLink', array($this, 'getNodeLink'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getNodeAlias', array($this, 'getNodeAlias'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getFileUrl', array($this, 'getFileUrl'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('renderNode', array($this, 'renderNode'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('renderViewByName', array($this, 'renderViewByName'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getNodeId', array($this, 'getNodeId'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('renderForm', array($this, 'renderForm'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('renderMenu', array($this, 'renderMenu'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('loadImageStyle', array($this, 'loadImageStyle'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getDrupalRenderArrayChildrenKeys', array($this, 'getDrupalRenderArrayChildrenKeys'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getDrupalRenderArrayChildren', array($this, 'getDrupalRenderArrayChildren'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getIdsRenderNodesSortedTitles', array($this, 'getIdsRenderNodesSortedTitles'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getDomains', array($this, 'getDomains'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getSessionValues', array($this, 'getSessionValues'), array('is_safe' => array('html'))),
            // site-specific functions here
            new \Twig_SimpleFunction('getEntityInfoByParagraphId', array($this, 'getEntityInfoByParagraphId'), array('is_safe' => array('html')))
        );
    }

    public function var_dump($var, $mode = 'standard') {
        if($mode === 'standard') {
            ob_start();
            var_dump($var);
            $content = ob_get_contents();
            ob_end_clean();
            return '<pre>'.$content.'</pre>';
        }
        elseif($mode === 'print_r') return '<pre>'.print_r($var, true).'</pre>';
        else return null;
    }

    public function echo_kint($var, $extreme_mode = false) {
        echo kint($var);
        if($extreme_mode) exit;
    }

    /**
     * Generic function for entityQuery
     *
     * Example conditions:
     * ->condition('status', 1)
     * ->condition('changed', REQUEST_TIME, '<')
     * ->condition('title', 'cat', 'CONTAINS')
     * ->condition('field_tags.entity.name', 'cats')
     *
     * And $conditions array will be like this:
     * $conditions = array(
     *     array(p1, p2),
     *     array(p1, p2, p3),
     *     array(p1, p2, p3),
     *     array(p1, p2)
     * )
     * */
    public function entityQuery($entity = 'node', $conditions = array()) {
        $query = \Drupal::entityQuery($entity);
        if(!empty($conditions)) foreach($conditions as $condition) $query = call_user_func_array(array($query, 'condition'), $condition);
        return $query->execute();
    }

    public function getTaxonomyTermById($tid) {
        if(empty($tid)) return null;
        //return taxonomy_term_load($tid);
        return \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid);
    }

    public function getChildTerms($tid) {
        if(empty($tid)) return null;
        return \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($tid);//->loadTree('partner_type');
    }

    public function getTermRelatedNodeIds($tid, $node_type, $node_term_ref_field) {
        if(empty($tid)) return null;
        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', $node_type)
            ->condition($node_term_ref_field.'.entity.tid', $tid)
            ->execute();
        return $query;
    }

    public function getNodeId($node_type) {
        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', $node_type)
            ->execute();
        return $query;
    }

    public function getIdsRenderNodesSortedTitles($nids) {
        $titles = array();
        foreach ($nids as $nid) {
            $node = Node::load($nid);
            $title =  $node->getTitle();
            $titles[] = $title;
        }
       $titles_nids = array_combine($nids, $titles);
        asort($titles_nids);
        return array_keys($titles_nids);
    }

    public function loadNode($nid) {
        if(empty($nid)) return null;
        return Node::load($nid);
    }

    /**
     * Gets node's URL by ID
     * Returns alias if exists, otherwise node's standard link - /node/{nid}
     *
     * $options example:
     * $options = array(
     *     'absolute' => true
     * )
     * */
    public function getNodeLink($nid, $options = array()) {
        $default_options = array();
        if(!empty($options) && is_array($options)) $default_options = array_merge($default_options, $options);
        $url = Url::fromRoute('entity.node.canonical', ['node' => $nid], $default_options);
        return $url->toString();
    }

    public function getNodeAlias($nid) {
        $alias_data = \Drupal::service('path_alias.repository')->load($nid);
        return $alias_data && array_key_exists('alias', $alias_data) ? $alias_data['alias'] : null;
    }

    public function getFileUrl($uri) {
        return file_create_url($uri);
    }

    public function renderNode($nid) {
        if(empty($nid)) return null;
        //\Drupal::entityManager()->getStorage('node')->load($nid);
        $node = $this->loadNode($nid);
        $node_view = Drupal::entityTypeManager()->getViewBuilder('node')->view() ($node);
        return \Drupal::service('renderer')->render($node_view);
    }

    public function renderMenu($menu_name) {
        $menu_tree = \Drupal::menuTree();

        // Build the typical default set of menu tree parameters.
        $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);

        // Load the tree based on this set of parameters.
        $tree = $menu_tree->load($menu_name, $parameters);

        // Transform the tree using the manipulators you want.
        $manipulators = array(
          // Only show links that are accessible for the current user.
          array('callable' => 'menu.default_tree_manipulators:checkAccess'),
          // Use the default sorting of menu links.
          array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
        );
        $tree = $menu_tree->transform($tree, $manipulators);

        // Finally, build a renderable array from the transformed tree.
        $menu = $menu_tree->build($tree);

        return  array('#markup' => \Drupal::service('renderer')->render($menu));
    }

    public function renderViewByName($name, $display_id = null) {
        if(empty($name)) return null;
        $view = Views::getView($name);
        return $view->render($display_id);
    }

    /**
     * Renders a form.
     *
     * We should pass desired form's class, eg. Drupal\praemium_forms\Form\ContactForm,
     * but twig's autoescaping removes "\" characters, so we should use "/"
     * character in argument string, then replace it back to "\"
     *
     * @param $form_class
     * @return mixed
     */
    public function renderForm($form_class) {
        $correct_form_class = str_replace('/', '\\', $form_class);
        try {
            return \Drupal::formBuilder()->getForm($correct_form_class);
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    public function loadImageStyle($style, $img_public_path) {
        if(empty($style) || empty($img_public_path)) return null;
        return ImageStyle::load($style)->buildUrl($img_public_path);
    }

    public function getDrupalRenderArrayChildrenKeys($array) {
        return Element::children($array);
    }

    public function getDrupalRenderArrayChildren($array) {
        $children_keys = $this->getDrupalRenderArrayChildrenKeys($array);
        $result = array();
        foreach($children_keys as $key) $result[$key] = $array[$key];
        return $result;
    }

    public function getDomains() {
        try{
            return \Drupal::service('domain.loader')->loadMultiple();
        }
        catch(\Exception $e) {return null;}
    }

    public function getSessionValues($storage, $key = null, $type = 'private') {
        $store = 'user.private_tempstore';
        if($type === 'shared') $store = 'user.shared_tempstore';

        $tempstore = \Drupal::service($store)->get($storage);
        //$tempstore->set('my_variable_name', 'test');// to set a value in storage

        if($key) return $tempstore->get($key);
        return $tempstore;
    }

    // Site-specific functions here
    /**
     * Gets node ID by it's inner paragraph's ID
     *
     * $options example:
     * $options = array(
     *     'table' => 'node__field_paragraphs',
     *     'table_alias' => 'nfp',
     *     'fields' => array('entity_id')
     * )
     *
     * $conditions example:
     * array( array('field_paragraphs_target_id', 1) )
     * */
    public function getEntityInfoByParagraphId($options = null, $conditions = array()) {
        $default_options = array(
            'table' => 'node__field_paragraphs',
            'table_alias' => 'nfp',
            'fields' => array('entity_id')
        );
        if(!empty($options) && is_array($options)) $default_options = array_merge($default_options, $options);

        $query = \Drupal::service('database')->select($default_options['table'], $default_options['table_alias'])
            ->fields($default_options['table_alias'], $default_options['fields']);

        if(!empty($conditions)) foreach($conditions as $condition) $query = call_user_func_array(array($query, 'condition'), $condition);
        return $query->execute();
    }
    // End of site-specific functions
}
