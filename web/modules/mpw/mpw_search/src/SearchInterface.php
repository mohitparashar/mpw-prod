<?php

namespace Drupal\mpw_search;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for mpw_search search plugins.
 */
interface SearchInterface extends PluginInspectionInterface {

  /**
   * Return the name of the search.
   *
   * @return string
   *   Title.
   */
  public function getTitle();

  /**
   * Search for keyword and return results.
   *
   * @param string $keyword
   *   Searching for keyword.
   *
   * @return array
   *   Renderable search element.
   */
  public function getSearch($keyword);

  /**
   * Return renderable more link snippet.
   *
   * @param string $keyword
   *   Keyword argument.
   *
   * @return array
   *   Links.
   */
  public function getMoreLinks($keyword);

}
