<?php

namespace Drupal\mpw_search;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Search plugin manager.
 */
class SearchManager extends DefaultPluginManager implements SearchManagerInterface {

  /**
   * Constructs an SearchManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/mpw_search/Search', $namespaces, $module_handler, 'Drupal\mpw_search\SearchInterface', 'Drupal\mpw_search\Annotation\Search');
    $this->setCacheBackend($cache_backend, 'mpw_search_searches');
  }

}
