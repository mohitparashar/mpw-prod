<?php

namespace Drupal\mpw_search\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a search item annotation object.
 *
 * @Annotation
 */
class Search extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the search.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

}
