<?php

namespace Drupal\mpw_search;

use Drupal\Component\Plugin\PluginBase;
use Drupal\views\Views;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a base for view-based SearchPlugins.
 */
class SearchBase extends PluginBase implements SearchInterface {

  use StringTranslationTrait;

  protected $viewName = NULL;
  protected $searchExecuted = FALSE;

  /**
   * The view.
   *
   * @var \Drupal\views\ViewExecutable|null
   */
  protected $view = NULL;
  protected $viewDisplayId = NULL;
  protected $baseEntityType = NULL;
  protected $resultViewMode = NULL;
  protected $isEmpty = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSearch($keyword) {
    $view = Views::getView($this->viewName);
    if (!$view || !$view->access($this->viewDisplayId)) {
      return [];
    }

    return [
      '#type' => 'view',
      '#name' => $this->viewName,
      '#display_id' => $this->viewDisplayId,
      '#arguments' => [$keyword],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMoreLinks($keyword) {
    return [];
  }

}
