<?php

namespace Drupal\mpw_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mpw_search\SearchManagerInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Utility\Unicode;

/**
 * Class SearchController.
 *
 * @package Drupal\mpw_search\Controller
 */
class SearchController extends ControllerBase {

  /**
   * mpw Search Manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $searchManager;

  /**
   * Controller Constructor.
   *
   * @param \Drupal\mpw_search\SearchManagerInterface $search_manager
   *   mpw search manager.
   */
  public function __construct(SearchManagerInterface $search_manager) {
    $this->searchManager = $search_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mpw_search')
    );
  }

  /**
   * Sitewidesearch.
   *
   * @return array
   *   Render array.
   */
  public function sitewideSearch() {
    $keyword = Unicode::mb_strtolower(Xss::filter($parameters = \Drupal::request()->query->get('keyword')));

    $render = [];

    foreach ($this->searchManager->getDefinitions() as $search_plugin_id => $search_definition) {

      $render[$search_plugin_id] = [
        '#lazy_builder' => [
          self::class . ':lazySectionBuilder',
          [
            $search_plugin_id,
            $keyword,
          ],
        ],
        '#create_placeholder' => TRUE,
      ];
    }

    return $render;
  }

  /**
   * Lazy builder.
   *
   * @param string $search_plugin_id
   *   Plugin ID of mpw Search.
   * @param string $keyword
   *   Keyword to search.
   *
   * @return array
   *   Renderable array.
   */
  public static function lazySectionBuilder($search_plugin_id, $keyword) {

    $searchManager = \Drupal::service('plugin.manager.mpw_search');

    /** @var \Drupal\mpw_search\SearchInterface $search_plugin */
    $search_plugin = $searchManager->createInstance($search_plugin_id);

    return [
      '#theme' => 'unified_search_section',
      '#title' => $search_plugin->getTitle(),
      '#results' => $search_plugin->getSearch($keyword),
      '#buttons' => $search_plugin->getMoreLinks($keyword),
    ];
  }

}
