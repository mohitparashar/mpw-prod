<?php

namespace Drupal\mpw_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Class UnifiedSearchForm.
 *
 * @package Drupal\mpw_search\Form
 */
class UnifiedSearchForm extends FormBase {
  protected $classBase = 'mpw--search-';

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new UnifiedSearchBlockForm.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unified_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#action'] = Url::fromRoute('mpw_search.search_controller_sitewideSearch')->toString();
    $form['#token'] = FALSE;
    $form['#method'] = 'get';

    $form['#attributes']['role'] = 'search';

    $site_config = $this->config('system.site');

    $form['keyword'] = [
      '#type' => 'search',
      '#title' => $this->t(
        'Search @mpw_site_name',
        [
          '@mpw_site_name' => $site_config->get('name'),
        ],
        [
          'context' => 'mpw_search_unified_title',
        ]
      ),
      '#placeholder' => $this->t(
        'SEarch Criteria',
        [
          'context' => 'mpw_search_unified_placeholder',
        ]
      ),
      '#maxlength' => 128,
      '#size' => 20,
      '#weight' => -1,
    ];

    $form['submit'] = [
      'button' => [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => [
          'class' => [
            'form-submit',
          ],
        ],
        '#value' => $this->t(
          'submit',
          [],
          [
            'context' => 'mpw_search_unified_submit',
          ]
        ),
        '#weight' => 0,
        '#name' => 'unified-search-submit',
      ],
    ];

    $this->renderer->addCacheableDependency($form, $site_config);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
