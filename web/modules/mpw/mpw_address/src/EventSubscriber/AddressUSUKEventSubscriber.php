<?php

namespace Drupal\mpw_address\EventSubscriber;

use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddressUSUKEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onAddressFormat'];
    return $events;
  }

  /**
   * Alters the address format field.
   *
   * @param \Drupal\address\Event\onAddressFormat $event
   */
  public function onAddressFormat(AddressFormatEvent $event) {
    $definition = $event->getDefinition();

    if ($definition['country_code'] == 'US') {
      // Place %dependentLocality" after %addressLine2 in the format.
      $format = $definition['format'];
      $format = str_replace('%addressLine2', "%addressLine2\n%dependentLocality", $format);
      $definition['format'] = $format;

      $event->setDefinition($definition);
    }
  }

}
