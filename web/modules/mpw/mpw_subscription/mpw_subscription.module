<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Entity\User;
use Drupal\mpw_subscription\Constants\Constants;
use Drupal\mpw_subscription\Event\MpwNodeInsertEvent;
use Drupal\mpw_subscription\Controller\MpwRecurringSubscription;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\mpw_subscription\Controller\MpwPayFlowSubscription;
use Drupal\mpw_subscription\PropertyPathAlias;


function mpw_subscription_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $current_user = \Drupal::currentUser();
  $user = \Drupal\user\Entity\User::load($current_user->id());

  $user_roles = $user->getRoles();
  if(in_array("broker", $user_roles) && ($form_id == 'node_property_form' || $form_id == 'node_property_edit_form')) {

    // Froce check checkbox
    $form['field_show_broker_information']['widget']['value']['#default_value'] = 1;
    $form['field_show_broker_information']['widget']['value']['#attributes']['disabled'] = 'disabled';
    // Make manditory to fix broker info
    $form['field_brokers_owners']['widget']['0']['subform']['field_company']['widget']['0']['value']['#required'] = TRUE;
    $form['field_brokers_owners']['widget']['0']['subform']['field_contact_name']['widget']['0']['value']['#required'] = TRUE;
    $form['field_brokers_owners']['widget']['0']['subform']['field_phone_number']['widget']['0']['value']['#required'] = TRUE;
    $form['field_brokers_owners']['widget']['0']['subform']['field_email']['widget']['0']['value']['#required'] = TRUE;
    $form['field_brokers_owners']['widget']['0']['top']['links']['#access'] = FALSE;

    $form['field_geolocation']['widget']['#required'] = FALSE;
    $form['field_geolocation']['widget']['0']['#required'] = FALSE;

    $form['field_referenced_city_page']['widget']['0']['target_id']['#required'] = FALSE;

    $form['field_geolocation']['#access'] = FALSE;
    $form['field_referenced_city_page']['#access'] = FALSE;

    $form['field_featured']['#access'] = FALSE;
    $form['field_weight']['#access'] = FALSE;
    $form['field_video']['#access'] = FALSE;
    $form['field_meta_tags']['#access'] = FALSE;
    $form['field_show_broker_information']['#access'] = FALSE;
    $form['field_property_terms']['#access'] = FALSE;
    $form['status']['widget']['value']['#default_value'] = 0;

    $form['field_avaibility']['widget']['0']['#description'] = '';
    $form['field_avaibility']['widget']['0']['caption']['#access'] = FALSE;
    $form['field_property_listing']['#access'] = FALSE;
    // unset($form['field_featured']);
    // unset($form['field_weight']);
    // unset($form['field_video']);
    // unset($form['field_meta_tags']);
    foreach (array_keys($form['actions']) as $action) {
      if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
        $form['actions'][$action]['#submit'][] = 'mpw_subscription_node_form_submit';
      }
    }
    $form['#validate'][] = 'property_node_validate';

    $query = \Drupal::entityQuery('node')
      ->condition('type', 'property')
      ->condition('uid', $user->id());
    $nids = $query->execute();

    if (!$nids) {
      $form['field_property_listing']['#access'] = FALSE;
    }

    if ($user->field_elite_member->value) {
      $form['field_property_listing']['#access'] = FALSE;
    }
  }
  else {
    $form['field_property_listing']['#access'] = FALSE;
  }

  if ($form_id == 'user_form') {
    $form['#attached']['library'][] = 'mpw_subscription/timezone_forms';
    if(in_array("broker", $user_roles)) {
      $form['field_elite_member']['#access'] = FALSE;
    }
  }
}

/**
 * Helper function to validate if there is any pervious added nodes.
 */
function property_node_validate (&$form, FormStateInterface $form_state) {
  $entity = $form_state->getFormObject()->getEntity();
  $query = \Drupal::entityQuery('node')
      ->condition('type', 'property')
      ->condition('uid', $entity->getOwnerId());
  $nids = $query->execute();
  $database = \Drupal::database();
  $query = $database->select('mpw_rc_subscription', 'mpw_rc');
  $query->condition('mpw_rc.uid', $entity->getOwnerId());
  $query->fields('mpw_rc', ['nid']);
  $number_of_rows = $query->countQuery()->execute()->fetchField();
  $scount = (is_array($number_of_rows)) ? count($number_of_rows) : $number_of_rows;
  $ncount = (is_array($nids)) ? count($nids) : $nids;
  if ($entity->isNew() && ($ncount > $scount)) {
    $form_state->setErrorByName('title', t('There is already unsubscribe property exists in databsse, please subscribe them.'));
  }
}

/**
 * Invoke subscription on node basis.
 */
function mpw_subscription_node_form_submit (&$form, FormStateInterface $form_state) {
  // Get entity
  $entity = $form_state->getFormObject()->getEntity();
  // Database connection.
  $database = \Drupal::database();
  // Select subscription.
  $query = $database->select('mpw_rc_subscription', 'mpw_rc');
  // Add extra detail to this query object: a condition, fields and a range.
  $query->condition('mpw_rc.nid', $entity->id());
  $query->fields('mpw_rc', ['nid']);
  $number_of_rows = $query->countQuery()->execute()->fetchField();
  $scount = (is_array($number_of_rows)) ? count($number_of_rows) : $number_of_rows;
  if ($scount < 1) {
    // Record subscription entry.
    $payflow_subscription = new MpwPayFlowSubscription();

    $user = \Drupal\user\Entity\User::load($entity->getOwnerId());
    // Construct user data.
    $user_data = [
      'firstname' => $user->get("field_profile_name")->value,
      'lastname' => $user->get("field_last_name")->value,
      'email' => $user->get('mail')->value,
      'address' => $user->get("field_street_address")->address_line1,
      'city' => $user->get("field_street_address")->locality,
      'state' => $user->get("field_street_address")->administrative_area,
      'zip' => $user->get("field_street_address")->postal_code,
      // Plan
      'plan' => 'bl',
      'nid' => $entity->id()
    ];

    $server_response = [
      'RPREF' => 'Allow',
      'RESPMSG' => 'Active',
      'PROFILEID' => $entity->getOwnerId(),
      'RESPMSG'=> 'verified',
    ];

    // Record Subscription.
    $payflow_subscription->recordSubscription($user_data, $server_response, $entity->getOwnerId());
  }
  // return TRUE;
  //  Old
  // $entity = $form_state->getFormObject()->getEntity();
  // $query = \Drupal::entityQuery('node')
  //     ->condition('type', 'property')
  //     ->condition('uid', $entity->getOwnerId());
  // $nids = $query->execute();
  //   // echo count($nids);exit;
  // if (count($nids) <= 1) {
  //   // update current subscription.
  //   $connection = \Drupal::service('database');
  //   $connection->update('mpw_rc_subscription')
  //     ->fields([
  //       'nid' => $entity->id(),
  //     ])
  //     ->condition('uid', $entity->getOwnerId())
  //     ->execute();
  // }
  // else {
  //   $database = \Drupal::database();
  //   // Select subscription.
  //   $query = $database->select('mpw_rc_subscription', 'mpw_rc');
  //   // Add extra detail to this query object: a condition, fields and a range.
  //   $query->condition('mpw_rc.nid', $entity->id());
  //   $query->fields('mpw_rc', ['nid']);
  //   $number_of_rows = $query->countQuery()->execute()->fetchField();
  //   $scount = (is_array($number_of_rows)) ? count($number_of_rows) : $number_of_rows;
  //   if ($scount < 1) {
  //     // redirect to new subscription.
  //     $subscription = new MpwRecurringSubscription();
  //     if ($entity->field_property_listing->value == '29') {
  //       $subscription->payRcProperty($entity->getOwnerId(), $entity->id(), Constants::PLAN_CODE_BASIC);
  //     }
  //     elseif ($entity->field_property_listing->value == '99') {
  //       $subscription->payRcConcierge($entity->getOwnerId(), $entity->id(), Constants::PLAN_CODE_CONCIERGE);
  //     }
  //     else {
  //       $subscription->payRcElite($entity->getOwnerId(), $entity->id(), Constants::PLAN_CODE_ELITE);
  //     }
  //   }
  // }
}

/**
 * Implements hook_field_widget_multivalue_form_alter().
 *
 * Here we tell Drupal that when we want to run some code when the field widget gets built.
 */
function mpw_subscription_field_widget_multivalue_form_alter(&$elements, \Drupal\Core\Form\FormStateInterface $form_state, $context) {
  // We only want to mess with field_banner_image
  if ($context['items']->getName() == 'field_banner_image' || $context['items']->getName() == 'field_image') {

    // If this is a multivalue field, we want to modify every instance of the field on the form,
    // so we iterate.
    foreach (\Drupal\Core\Render\Element::children($elements) as $key => $child) {

      // Set a custom function that will run when processing this widget.
      $elements[$key]['#process'][] = '_mpw_subscription_remove_alt_field';
    }
  }
}

/**
 * #process callback for disabling alt on image field.
 * Defined in mymodule_field_widget_multivalue_form_alter().
 */
function _mpw_subscription_remove_alt_field($form_element, \Drupal\Core\Form\FormStateInterface $form_state) {
  $form_element['alt']['#title'] = t('Write a description for your original image.');
  $form_element['alt']['#description'] = '';

  return $form_element;
}

/**
 * Implements hook_mail().
 */
function mpw_subscription_mail($key, &$message, $params) {
 $options = array(
   'langcode' => $message['langcode'],
 );
 switch ($key) {
  case 'create_property':
     $message['from'] = $params['from'];
     $message['subject'] = t('Listing has been created: @title', array('@title' => $params['node_title']), $options);
     $message['body'][] = 'Title: <br>' . $params['node_title'];
     $message['body'][] = '<br><br>Content:' . $params['message'];
     $message['body'][] = 'Property: <br>' . '<a href="https://myperfectworkplace.com/' . $params['url'] . '">'. $params['node_title'] . '</a>';
     $message['body'][] = '<p>Regards<br>MyPerfect Workplace<p>';
     break;
  case 'published_property':
      //$message['from'] = \Drupal::config('system.site')->get('mail');
      $message['from'] = $params['from'];
      $message['subject'] = t('Article Published: @title', array('@title' => $params['node_title']), $options);
      $message['body'][] = 'Title: <br>' . $params['node_title'];
      $message['body'][] = '<br><br>Content:' . $params['message'];
      $message['body'][] = 'Property: <br>' . '<a href="https://myperfectworkplace.com/' . $params['url'] . '">'. $params['node_title'] . '</a>';
      $message['body'][] = '<p>Regards<br>MyPerfect Workplace<p>';
    break;
 }
}

/**
 * Implements hook_entity_insert().
 */
function mpw_subscription_entity_insert(EntityInterface $entity) {
  $current_user = \Drupal::currentUser();
  $user = \Drupal\user\Entity\User::load($current_user->id());
  $user_roles = $user->getRoles();

  if ($entity->getEntityTypeId() !== 'node' || ($entity->getEntityTypeId() === 'node' && $entity->bundle() !== 'property' && in_array("broker", $user_roles))) {
    return;
  }

  $mailManager = \Drupal::service('plugin.manager.mail');
  $module = 'mpw_subscription';
  $key = 'create_property';
  $to = \Drupal::config('system.site')->get('mail');
  $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/'.$entity->id());
  $params['from'] = \Drupal::currentUser()->getEmail();
  $params['node_title'] = $entity->label();
  $params['message'] = $entity->get('body')->value;
  $params['url'] = $alias;

  $langcode = \Drupal::currentUser()->getPreferredLangcode();
  $send = true;
  $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
  if ($result['result'] !== true) {
    Drupal\Core\Messenger\MessengerInterface::addMessage(t('There was a problem sending your message and it was not sent.'), 'error');
  }
  else {
    Drupal\Core\Messenger\MessengerInterface::addMessage(t('Your message has been sent.'));
  }
}

/**
 * Implements hook_entity_update().
 */
function mpw_subscription_node_update(EntityInterface $entity) {
  if ($entity->getEntityTypeId() !== 'node' || ($entity->getEntityTypeId() === 'node' && $entity->bundle() !== 'property')) {
    return;
  }
  elseif (isset($entity->original->status) && $entity->original->get('status')->value == 0 && $entity->get('status')->value == 1) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'mpw_subscription';
    $key = 'published_property';
    /** @var \Drupal\user\UserInterface $user */
    $user = $entity->getOwner();
    $email = $user->getEmail();
    $to = $email;
    $params['from'] = \Drupal::config('system.site')->get('mail');
    $params['node_title'] = $entity->label();
    $params['message'] = $entity->get('body')->value;
    $params['url'] = $entity->toUrl()->toString();

    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      Drupal\Core\Messenger\MessengerInterface::addMessage(t('There was a problem sending your message and it was not sent.'), 'error');
    }
    else {
      Drupal\Core\Messenger\MessengerInterface::addMessage(t('Your message has been sent.'));
    }
  }
}

/**
 * Implements hook_views_data().
 */
function mpw_subscription_views_data() {
  $data['views']['table']['group'] = t('Mpw Subscription');

  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['pay_id'] = [
    'title' => t('Paypal Pay ID'),
    'help' => t('Paypal id for unique each subscription.'),
    'field' => [
      'id' => 'pay_id',
    ],
  ];

  $data['views']['payer_id'] = [
    'title' => t('Paypal Payer ID'),
    'help' => t('Paypal payer id for unique each subscription.'),
    'field' => [
      'id' => 'payer_id',
    ],
  ];

  $data['views']['product_id'] = [
    'title' => t('Plan ID'),
    'help' => t('User product plan id.'),
    'field' => [
      'id' => 'product_id',
    ],
  ];

  return $data;
}


/**
 * Implements hook_token_info().
 */
function mpw_subscription_token_info() {
  $types['user'] = [
    'name' => t('Users'),
    'description' => t('Tokens related to individual user accounts.'),
    'needs-data' => 'user',
  ];
  $user['display-name-ucfirst'] = [
    'name' => t("Display Name UC First"),
    'description' => t("The display name of the user account as first character upper case."),
  ];

  return [
    'types' => $types,
    'tokens' => ['user' => $user],
  ];
}


/**
 * Implements hook_tokens().
 */
function mpw_subscription_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $token_service = \Drupal::token();
  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $replacements = [];

  if ($type == 'user' && !empty($data['user'])) {
    /** @var \Drupal\user\UserInterface $account */
    $account = $data['user'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Basic user account information.

        case 'display-name-ucfirst':
          $replacements[$original] = ucfirst($account->getDisplayName());
          if ($account->isAnonymous()) {
            $bubbleable_metadata->addCacheableDependency(\Drupal::config('user.settings'));
          }
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function mpw_subscription_form_user_register_form_alter(&$form, FormStateInterface &$form_state) {
  //$form['#theme'] = ['user_register_form '];
  $form['actions']['submit']['#submit'][] = 'mpw_user_register_submit_handler';
}

function mpw_user_register_submit_handler($form, FormStateInterface $form_state) {
  $form_state->setRedirect('entity.node.canonical', ['node' => 358]);
}

/**
 * Alter Pathauto-generated aliases before saving.
 *
 * @param string $alias
 *   The automatic alias after token replacement and strings cleaned.
 * @param array $context
 *   An associative array of additional options, with the following elements:
 *   - 'module': The module or entity type being aliased.
 *   - 'op': A string with the operation being performed on the object being
 *     aliased. Can be either 'insert', 'update', 'return', or 'bulkupdate'.
 *   - 'source': A string of the source path for the alias (e.g. 'node/1').
 *     This can be altered by reference.
 *   - 'data': An array of keyed objects to pass to token_replace().
 *   - 'type': The sub-type or bundle of the object being aliased.
 *   - 'language': A string of the language code for the alias (e.g. 'en').
 *     This can be altered by reference.
 *   - 'pattern': A string of the pattern used for aliasing the object.
 */
function mpw_subscription_pathauto_alias_alter(&$alias, array &$context) {
  // Add a suffix so that all aliases get saved as 'content/my-title.html'.
  if ($new_alias = (new PropertyPathAlias())->generate($alias, $context)) {
    $alias = $new_alias;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function mpw_subscription_form_node_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'node_property_form') {
    $form['actions']['submit']['#submit'][] = '_mpw_subscription_node_submit_redirect';
  }
}

/**
 * Extra node form submit handler. Done this way to override the default.
 */
function _mpw_subscription_node_submit_redirect($form, &$form_state) {
  if (isset($_GET['destination'])) {
    unset($_GET['destination']);
  }

  $uid = \Drupal::currentUser()->id();
  $nid = $form_state->getValue('nid');

  $redirect_path = '/subscription/'.$nid.'/'.$uid.'/upgrade';
  $url = url::fromUserInput($redirect_path);

  /** @var \Drupal\Core\Form\FormState $form_state  */
  $form_state->setRedirectUrl($url);
}
