(function($, undefined) {

	document.body.addEventListener( 'click', function ( event ) {
  		if( event.srcElement.id == 'terms-condition-lb' ) {
	    	jQuery.colorbox({
	            iframe:true,
	            href:'/real-estate-website-terms-conditions',
	            width:'700px',
	            height:'450px',
	            open:true
 	       	});
	  	};
	});

	// jQuery('.terms-condition-lb').on('click', function(e) {
	//        jQuery.colorbox({
	//            iframe:true,
	//            href:'/real-estate-website-terms-conditions',
	//            width:'700px',
	//            height:'450px',
	//            open:true
	//        });
	//    });
})(jQuery);