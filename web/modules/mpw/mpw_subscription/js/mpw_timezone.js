(function($, undefined) {

	jQuery(document).ready(function() {

		jQuery("select[name=timezone]").children().remove("optgroup[label='Africa']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Antarctica']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Arctic']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Asia']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Atlantic']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Australia']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Europe']");
		jQuery("select[name=timezone]").children().remove("optgroup[label='Indian']");
		
		jQuery("select[name=timezone] optgroup option").each(function() {
			var tz = jQuery(this).val();
		    if (tz != 'America/New_York' &&
		    	tz != 'America/Chicago' &&
		    	tz != 'America/Denver' &&
		    	tz != 'America/Phoenix' && 
		    	tz != 'America/Los_Angeles' && 
		    	tz != 'America/Anchorage' &&
		    	tz != 'America/Adak' &&
		    	tz != 'Pacific/Honolulu')
		    jQuery(this).remove();
		});

		jQuery("select[name=timezone] option").each(function() {
			if (jQuery(this).val() == 'UTC') {
				jQuery(this).remove();
			}
		});
	});

})(jQuery);