<?php

namespace Drupal\mpw_subscription;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mpw_subscription\Constants\Constants;

use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;



class PaypalExpress {

  private $enableSandbox;
  private $apiContext;
  private $connection;

  public function __construct($amountPayable) {
    $this->amountPayable = $amountPayable;
  }

  public function pay() {

    $this->apiContext = $this->getApiContext(Constants::CLIENT_ID, Constants::CLIENT_SECRET, Constants::SANDBOX_MODE);

    $payment = $this->setPayment();

    try {
      $payment->create($this->apiContext);
    }
    catch (Exception $e) {
      throw new Exception('Unable to create link for payment');
    }

    header('location:' . $payment->getApprovalLink());
    exit(1);
  }

  private function getApiContext($clientId, $clientSecret) {
    $apiContext = new ApiContext(
        new OAuthTokenCredential($clientId, $clientSecret)
    );

    $apiContext->setConfig([
        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
    ]);
    return $apiContext;
  }

  private function setAmount() {
    $amount = new Amount();
    $setamount = $amount->setCurrency('USD')
      ->setTotal($this->amountPayable);
    return $setamount;
  }

  private function setTransaction() {
    $amount = $this->setAmount();
    $invoiceNumber = uniqid();
    $transaction = new Transaction();
    $setTransaction = $transaction->setAmount($amount)
    ->setDescription('Some description about the payment being made')
    ->setInvoiceNumber($invoiceNumber);
    return $setTransaction;
  }

  private function setRedirect() {
    $redirectUrls = new RedirectUrls();
    $setRedirect = $redirectUrls->setReturnUrl(Constants::RETURN_URL)
    ->setCancelUrl(Constants::CANCEL_URL);
    return $setRedirect;
  }

  private function setPayment() {
    $payer = new Payer();
    $payer->setPaymentMethod('paypal');

    $transaction = $this->setTransaction();
    $redirectUrls = $this->setRedirect();

    $payment = new Payment();
    $setpayment = $payment->setIntent('sale')
      ->setPayer($payer)
      ->setTransactions([$transaction])
      ->setRedirectUrls($redirectUrls);
    return $setpayment;
  }

}
