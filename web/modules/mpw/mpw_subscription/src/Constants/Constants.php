<?php

/*
 *
 */
namespace Drupal\mpw_subscription\Constants;

/**
 * Description of Constants
 *
 */
class Constants {
  const CLIENT_ID = 'ASZJju7HdKGG4QIr6hLvdsvfLj1OoSwOOou8Ip8GxB_8O4M7S05AFAs4NiAclttVDWnH_YD_H9kS_KOb';
  const CLIENT_SECRET = 'EBNeB1mh5erfN0BVdJ1UXqb4fyd_5_260WXVBe5jvkmUcDk8KeUX5j4j9jzYslah-K02Fu97vjOLRVDX';
  const RETURN_URL = "https://myperfectworkplace.com/pay-rc-return";
  const CANCEL_URL = "https://myperfectworkplace.com/pay-rc-cancel";
  const PLAN_NAME_BASIC = "Basic Listing";
  const PLAN_NAME_CONCIERGE = "Concierge Listing";
  const PLAN_NAME_ELITE = "Enterprise Listing";
  const PLAN_DESCRIPTION_BASIC = "Basic No-Frills Package";
  const PLAN_DESCRIPTION_CONCIERGE = "The Standard Package";
  const PLAN_DESCRIPTION_ELITE = "Elite! Enough Said!";
  const PLAN_CODE_BASIC = 1;
  const PLAN_CODE_CONCIERGE = 2;
  const PLAN_CODE_ELITE = 3;
  const SANDBOX_MODE = FALSE;
}
