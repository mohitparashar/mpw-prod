<?php
namespace Drupal\mpw_subscription\Plugin\WebformHandler;

use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\node\Entity\Node;

/**
 * Emails a webform submission.
 *
 * @WebformHandler(
 *   id = "mpw_lead_email",
 *   label = @Translation("Leads email"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission to a node author email address."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class MpwEmailWebformHandler extends EmailWebformHandler {

  public function sendMessage(WebformSubmissionInterface $webform_submission, array $message) {

    $source_node = $webform_submission->getSourceUrl()->getRouteParameters();
    $nid = $source_node['node'];
    $node = Node::load($nid);
    $owner_mail = [];
    if ($node && ($nid != '130') && $node->hasField('field_brokers_owners')) {
      $paragraph = $node->field_brokers_owners->getValue();
      $show_broker_info = $node->field_show_broker_information->value;
      // Loop through the result set.
      foreach ($paragraph as $element) {
        $paragraph_field = \Drupal\paragraphs\Entity\Paragraph::load($element['target_id']);
        $email = $paragraph_field->field_email->getValue();
        $secondary_email = $paragraph_field->field_secondary_email->getValue();
      }
  
      if (isset($email[0]['value'])) {
        $owner_mail[] = $email[0]['value'];
      }

      if (isset($secondary_email[0]['value'])) {
        $owner_mail[] = $secondary_email[0]['value'];
      }

      $message['to_mail'] = implode(",", $owner_mail);
    }

    // parent::sendMessage($webform_submission, $message);
    if($show_broker_info) {
      parent::sendMessage($webform_submission, $message);
    }
  }

}
