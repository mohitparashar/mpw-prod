<?php

namespace Drupal\mpw_subscription\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\node\Entity\Node;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "mpw_subscription",
 *   label = @Translation("Submit leads"),
 *   category = @Translation("Webform Handler"),
 *   description = @Translation("Submit leads to custom table"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class MpwWebformHandler extends WebformHandlerBase {

  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    $webform = $this->getWebform();
    $form_data = $webform_submission->getData();

    $source_node = $webform_submission->getSourceUrl()->getRouteParameters();
    $nid = $source_node['node'];
    $node = Node::load($nid);

    if ($node && ($nid != '130')) {
      $uid = $node->getOwnerId();

      // Extra data from value.
      foreach ($form_data as $key => $value) {
        $$key = $value;
      }

      if (!empty($form_data['email'])) {
        $connection = \Drupal::service('database');

        $connection->insert('mpw_subscription_leads')
          ->fields([
            'payer_id',
            'nid',
            'name',
            'phone',
            'email',
            'message',
            'created',
          ])
          ->values([
            'payer_id' => $uid,
            'nid' => $nid,
            'name' => $name. ' ' . $last_name,
            'phone' => $phone,
            'email' => $email,
            'message' => 'Company Name : ' . $company_name .'</br>' .
                         'Company Size : ' . $company_size .'</br>' .
                         'Location of interest : ' . $location_of_interest_enter_city .'</br>'.
                         'Message : ' . $message,
            'created' => Drupal::time()->getRequestTime(),
          ])
          ->execute();
      }
    }

  }
}
