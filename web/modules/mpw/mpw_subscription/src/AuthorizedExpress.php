<?php

namespace Drupal\mpw_subscription;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class AuthorizedExpress {

    private $APILoginId;
    private $APIKey;
    private $refId;
    private $merchantAuthentication;
    public $responseText;
    private $connection;

    public function __construct() {
        // $this->APILoginId = "4C59VgcQ";
        // $this->APIKey = "63xTv4Q2e2R4Bs6T";

        $this->APILoginId = "78bD34ZJm5";
        $this->APIKey = "373UMpg6qkPa92V6";
        $this->refId = 'ref' . time();
        $this->merchantAuthentication = $this->setMerchantAuthentication();
        $this->responseText = array("1"=>"Approved", "2"=>"Declined", "3"=>"Error", "4"=>"Held for Review");
        $this->connection = \Drupal::service('database');
    }

    public function setMerchantAuthentication() {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->APILoginId);
        $merchantAuthentication->setTransactionKey($this->APIKey);
        // Return merchant object.
        return $merchantAuthentication;
    }

    public function setCreditCard($cardDetails) {
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($cardDetails['card_number']);
        $creditCard->setExpirationDate($cardDetails['expiry_date']);
        $creditCard->setCardCode($cardDetails['secuirty_code']);
        $paymentType = new AnetAPI\PaymentType();
        $paymentType->setCreditCard($creditCard);
        // Return payment object
        return $paymentType;
    }

    public function setCustomerOrderRequestType() {
        $order = new AnetAPI\OrderType();
        $order->setInvoiceNumber(rand());
        $order->setDescription('MPW Subscription');
        // Return order object
        return $order;
    }

    public function setCustomerAddressRequestType($cardDetails) {
        // Set the customer's Bill To address
        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setFirstName($cardDetails['first_name']);
        $customerAddress->setLastName($cardDetails['last_name']);
        $customerAddress->setCompany("");
        $customerAddress->setAddress($cardDetails['address']);
        $customerAddress->setCity($cardDetails['city']);
        $customerAddress->setState($cardDetails['state']);
        $customerAddress->setZip($cardDetails['zip_code']);
        $customerAddress->setCountry("USA");
        // Return customer address object
        return $customerAddress;
    }

    function setCustomerDataRequestType($cardDetails) {
        // Set the customer's identifying information
        $customerData = new AnetAPI\CustomerDataType();
        $customerData->setType("individual");
        $customerData->setId("");
        $customerData->setEmail($cardDetails['email']);
        // Return customer data object.
        return $customerData;
    }

    public function setTransactionRequestType($paymentType, $customerAddress, $customerData, $customerOrder, $amount) {
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($customerOrder);
        $transactionRequestType->setPayment($paymentType);
        $transactionRequestType->setBillTo($customerAddress);
        $transactionRequestType->setCustomer($customerData);
        // Return transaction request object.
        return $transactionRequestType;
    }

    public function chargeCreditCard($cardDetails) {
        $paymentType = $this->setCreditCard($cardDetails);
        $customerAddress = $this->setCustomerAddressRequestType($cardDetails);
        $customerData = $this->setCustomerDataRequestType($cardDetails);
        $customerOrder = $this->setCustomerOrderRequestType();
        $transactionRequestType = $this->setTransactionRequestType($paymentType, $customerAddress, $customerData, $customerOrder, $cardDetails['subscription_amount']);
        // Create request.
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($this->refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        // Return response.
        return $response;
    }

    public function insertTranscation($payload, $transactionId, $authCode, $responseCode, $paymentStatus, $uid) {
    $id = $this->connection->insert('mpw_subscription')
      ->fields([
        'product_id',
        'txn_id',
        'auth_code',
        'response_code',
        'payment_gross',
        'currency_code',
        'payer_id',
        'payer_name',
        'payer_email',
        'payment_status',
        'subscription_start',
        'subscription_end'
      ])
      ->values([
        'product_id' => 1,
        'txn_id' => $transactionId,
        'auth_code' => $authCode,
        'response_code' => $responseCode,
        'payment_gross' => $payload['subscription_amount'],
        'currency_code' => 'US',
        'payer_id' => $uid,
        'payer_name' => $payload['first_name'] . ' ' . $payload['last_name'],
        'payer_email' => $payload['email'],
        'payment_status' => $paymentStatus,
        'subscription_start' => Drupal::time()->getRequestTime(),
        'subscription_end' => strtotime("+30 days"),
      ])
    ->execute();
    return $id;
  }

  public function insertFailTranscation($payload, $error_code, $error_msg, $paymentStatus, $uid) {
    $id = $this->connection->insert('mpw_fail_subscription')
      ->fields([
        'product_id',
        'error_code',
        'error_msg',
        'payer_id',
        'payer_name',
        'payer_email',
        'payment_status',
      ])
      ->values([
        'product_id' => 1,
        'error_code' => $error_code,
        'error_msg' => $error_msg,
        'payer_id' => $uid,
        'payer_name' => $payload['first_name'] . ' ' . $payload['last_name'],
        'payer_email' => $payload['email'],
        'payment_status' => $paymentStatus,
      ])
    ->execute();
    return $id;
  }

  public function insertUser($payload) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $user = \Drupal\user\Entity\User::create();

    //Mandatory settings
    $user->setPassword('abc456');
    $user->enforceIsNew();
    $user->setEmail($payload['email']);
    $name = self::registrationCleanupUsername($payload['email']);
    $user->setUsername($name); //This username must be unique and accept only a-Z,0-9, - _ @ .

    //Optional settings
    $user->set("init", 'email');
    $user->set("langcode", $language);
    $user->set("preferred_langcode", $language);
    $user->set("preferred_admin_langcode", $language);
    $user->addRole('broker', 'authenticate');
    $user->activate();

    //Save user
    $user->save();
    _user_mail_notify('register_no_approval_required', $user);
    return $user->id();
 }

 static function registrationCleanupUsername($name) {
    // Strip illegal characters.
    $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);
    $split_name = explode('@', $name);

    $name = $split_name[0];

    // Strip leading and trailing spaces.
    $name = trim($name);

    // Convert any other series of spaces to a single underscore.
    $name = preg_replace('/\s+/', '_', $name);

    return $name;
  }
}
