<?php

namespace Drupal\mpw_subscription\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;

use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use Drupal\mpw_subscription\Constants\Constants;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class SuspendSubscriptionForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $nid;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $uid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL, $nid  = NULL, $uid  = NULL) {
    $this->id = $id;
    $this->nid = $nid;
    $this->uid = $uid;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->suspendSubscription($this->id);
    $url = \Drupal\Core\Url::fromRoute('mpw_subscription.subscription')
          ->setRouteParameters(array('user' => $this->uid));

    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_suspend_subscription_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $url = \Drupal\Core\Url::fromRoute('mpw_subscription.subscription')
          ->setRouteParameters(array('user' => $this->uid));
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to suspend subscription for %id?', ['%id' => $this->id]);
  }


  /**
   * Helper function.
   */
  function suspendSubscription() {
    $apiContext = new ApiContext(
        new OAuthTokenCredential(Constants::CLIENT_ID, Constants::CLIENT_SECRET)
    );

    $apiContext->setConfig([
        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
    ]);
    //Create an Agreement State Descriptor, explaining the reason to suspend.
    $agreementStateDescriptor = new AgreementStateDescriptor();
    $agreementStateDescriptor->setNote("Suspending the agreement");
    // Fetch the agreement object
    $createdAgreement = Agreement::get($this->id, $apiContext);
    //print_r($createdAgreement);exit;
    if ($createdAgreement->state != 'Suspended') {
      try {
          $createdAgreement->suspend($agreementStateDescriptor, $apiContext);
          // Lets get the updated Agreement Object
          $agreement = Agreement::get($createdAgreement->getId(), $apiContext);

          \Drupal::database()->update('mpw_rc_subscription')
            ->condition('nid' , $this->nid)
            ->condition('pay_id' , $this->id)
            ->fields([
              'subscription_state' => 'Suspended',
            ])
            ->execute();

          $node = \Drupal\node\Entity\Node::load($this->nid);
          $node->setPublished(false);
          $node->save();

          $target_id = $node->get('field_brokers_owners')->first()->getValue()['target_id'];
          // Load paragraph.
          $paragraph = Paragraph::load($target_id);
          // Do something with the $paragraph_field_value
          // Update the field.
          // Update paragraph fields.
          $paragraph->set('field_contact_name', '');
          $paragraph->set('field_email', '');
          $paragraph->set('field_phone_number', '');
          $paragraph->set('field_company', '');
          $paragraph->set('field_secondary_contact_name', '');
          $paragraph->set('field_secondary_email', '');
          $paragraph->set('field_secondary_phone_number', '');
          $paragraph->set('field_secondary_company_name', '');
          // Save the Paragraph.
          $paragraph->save();

      }
      catch (Exception $ex) {
        Drupal\Core\Messenger\MessengerInterface::addMessage("Error occurred. Please try again.");
      }
      Drupal\Core\Messenger\MessengerInterface::addMessage("Subscription has been suspended successfully.");
      return true;
    }
    else {
      Drupal\Core\Messenger\MessengerInterface::addMessage("Your subscription is already suspended.");
      return true;
    }
  }

}
