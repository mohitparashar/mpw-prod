<?php

/**
 * @file
 * Contains \Drupal\mpw_subscription\Form\SubscriptionForm.
 */

namespace Drupal\mpw_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\mpw_subscription\AuthorizedExpress;
use Drupal\mpw_subscription\Controller\MpwSubscription;
use Drupal\mpw_subscription\Controller\MpwPayFlowSubscription;

/**
 * Contribute form.
 */
class UpgradeSubscriptionForm extends FormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $nid;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $uid;

  /**
   * user object.
   *
   * @var object
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mpw_upgrade_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid  = NULL, $uid  = NULL) {
    $this->nid = $nid;
    $this->uid = $uid;
    $this->user = \Drupal\user\Entity\User::load($this->uid);
    $config = $this->config('mpw_subscription.settings');
    $plan_price = $this->user->get('field_elite_member')->value ? $config->get('elite_listing') : $config->get('standard_listing');
    $subscription_type = $this->user->get('field_elite_member')->value ? 3 : 2;
    // Subscription form.
    $form['#prefix'] = '<div class="upgrade-subscription-form"><br><br><div class="row">';
    $form['#suffix'] = '</div></div>';

    $form['debug'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['debug-out'],
      ],
    ];

    $form['heading'] = [
      '#type' => 'markup',
      '#markup' => '<div class="process-heading">
        <p>Items for purchase: Monthly Recurring Membership on My Perfect Workplace</p>
        <p><strong>Total Payment:</strong> $' . $plan_price . '/mo</p>
      </div>',
    ];

    $form['card_number'] = array(
      '#type' => 'creditfield_cardnumber',
      '#title' => t('Card Number'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => '4111111111111111'
      ]
    );

    $form['expiry_date'] = array(
      '#type' => 'creditfield_expiration',
      '#title' => t('Expiry Date'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => '2024-01'
      ]
    );
    $form['secuirty_code'] = array(
      '#type' => 'creditfield_cardcode',
      '#title' => t('Security Code'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => '1234'
      ]
    );

    $form['subscription_amount'] = array(
      '#type' => 'hidden',
      '#value' => $plan_price,
    );

    $form['subscription_type'] = array(
      '#type' => 'hidden',
      '#value' => $subscription_type,
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pay Securely'),
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $form['terms'] = [
      '#type' => 'markup',
      '#markup' => '<div class="process-heading">
        <p>Clicking "Pay" below authorizes us to charge your card for the total show above.</p>
        <p><strong>Please click only once. This step may take up to 60 seconds.</strong></p>
      </div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $database = \Drupal::database();
    $query = $database->select('mpw_rc_subscription', 'mpw_rc');
    $query->condition('mpw_rc.uid', $this->uid);
    $query->condition('mpw_rc.nid', $this->nid);
    $query->condition('mpw_rc.product_id', $form_state->getValue('subscription_type'));
    $query->condition('mpw_rc.subscription_state', ['verified','Approved'], 'IN');
    $query->fields('mpw_rc', ['nid']);
    $number_of_rows = $query->countQuery()->execute()->fetchField();
    $scount = (is_array($number_of_rows)) ? count($number_of_rows) : $number_of_rows;
    if ($scount) {
      $form_state->setErrorByName('card_number', t('There is already a subscription for this property, please check again.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    list($year, $month) = explode("-", $form_state->getValue('expiry_date'));
    $expiry_date = $month . '/' . substr($year, 2);

    $payflow_subscription = new MpwPayFlowSubscription();

    $user = $this->user;
    $subscription_type = $form_state->getValue('subscription_type');

    $user_data = [
      'firstname' => $user->get("field_profile_name")->value,
      'lastname' => $user->get("field_last_name")->value,
      'email' => $user->get("mail")->value,
      'phone' => $user->get("field_phone")->value,
      'address' => $user->get("field_street_address")->address_line1,
      'city' => $user->get("field_street_address")->locality,
      'zip' => $user->get("field_street_address")->administrative_area,
      'state' => $user->get("field_street_address")->postal_code,
      'plan' => $subscription_type,
      'creditCard' => $form_state->getValue('card_number'),
      'expirationDate'  => $expiry_date,
      'cvc' => $form_state->getValue('secuirty_code'),
    ] ;

    $card_response = $payflow_subscription->chargeCreditCard($user_data);

    if ($card_response['RESULT'] == '1') {
      $response['status'] = 'error';
      $response['message'] = $card_response['RESPMSG'];
    }
    elseif ($card_response['RESULT'] == '0') {
      // [RESULT] => 0
      // [RPREF] => R8350D1DCAE5
      // [PROFILEID] => RT0000000002
      // [RESPMSG] => Approved
      $this->updateUserPlan($this->uid, $this->nid, $card_response, $subscription_type);
      // Return success.
      $this->messenger()->addMessage($this->t('Property has been upgraded successfully!'));
    }
    else {
      $error = explode(":", $card_response['RESPMSG']);
      $msg = trim($error[1]);
      $this->messenger()->addMessage($this->t($msg));
    }
    // Redirect user to subscription
    $url = \Drupal\Core\Url::fromRoute('mpw_subscription.subscription')
          ->setRouteParameters(array('user' => $this->uid));
    $form_state->setRedirectUrl($url);
  }

  public function updateUserPlan($uid, $nid, $server_response, $subscription_type) {
    \Drupal::service('database')->update('mpw_rc_subscription')
      ->fields([
        'product_id' => $subscription_type,
        'pay_id' => $server_response['RPREF'],
        'subscription_state' => $server_response['RESPMSG'],
        'payer_id' => $server_response['PROFILEID'],
        'pay_status'=> $server_response['RESPMSG'],
        'next_billing_date' => date("Y-m-d H:i:s", strtotime("+30 days")),
        'last_payment_date' => date("Y-m-d H:i:s", strtotime("+1 days")),
        'final_payment_date' => date("Y-m-d H:i:s", strtotime("+365 days")),
      ])
      ->condition('uid', $uid)
      ->condition('nid', $nid)
      ->execute();
  }
}
