<?php
/**
 * @file
 * Contains \Drupal\mpw_subscription\Form\SubscriptionForm.
 */

namespace Drupal\mpw_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\mpw_subscription\AuthorizedExpress;
use Drupal\mpw_subscription\Controller\MpwSubscription;


/**
 * Contribute form.
 */
class SubscriptionForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mpw_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $subscription_type = \Drupal::request()->query->get('s');

    $form['#prefix'] = '<div id="subscription_modal_form">';
    $form['#suffix'] = '</div>';

    $form['debug'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['debug-out'],
      ],
    ];


    $form['heading'] = [
      '#type' => 'markup',
      '#markup' => '<div class="process-heading">
        <p>Items for purchase: Member ship on myperfectworkplace</p>
        <p><strong>Total Payment:</strong> $' . $subscription_type .
        '</p>
      </div>',
    ];
    $form['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#required' => TRUE,
    );

    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#required' => TRUE,
    );

    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => 'Email',
      '#required' => TRUE,
      '#prefix' => '<div id="user-email-result"></div>',
      '#ajax' => array(
         'callback' => '::checkUserEmailValidation',
         'effect' => 'fade',
         'event' => 'change',
          'progress' => array(
             'type' => 'throbber',
             'message' => NULL,
          ),
      ),
    );

    $form['role'] = [
      '#type' => 'select',
      '#title' => t('Roles'),
      '#options' => [
        '1' => t('I’m A Listing Agent'),
        '2' => t('I’m a Coworking Operator'),
        '3' => t('Other'),
      ],
    ];

    $form['card_number'] = array(
      '#type' => 'creditfield_cardnumber',
      '#title' => t('Card Number'),
      '#required' => TRUE,
    );

    $form['expiry_date'] = array(
      '#type' => 'creditfield_expiration',
      '#title' => t('Expiry Date'),
      '#required' => TRUE,
    );
    $form['secuirty_code'] = array(
      '#type' => 'creditfield_cardcode',
      '#title' => t('Secuirty Code'),
      '#required' => TRUE,
    );

    $form['address'] = array(
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#required' => TRUE,
    );

    $form['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#required' => TRUE,
    );

    $form['zip_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Zip/Postal Code'),
      '#required' => TRUE,
    );

    $form['state'] = array(
      '#type' => 'textfield',
      '#title' => t('State/Province'),
      '#required' => TRUE,
    );

    $form['country'] = [
      '#type' => 'select',
      '#title' => t('Country'),
      '#options' => [
        'us' => t('United State'),
      ],
    ];

    $form['subscription_amount'] = array(
      '#type' => 'hidden',
      '#value' => 1,
    );

    $form['subscription_type'] = array(
      '#type' => 'hidden',
      '#value' => ($subscription_type == 29) ? 1 : 2,
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pay Securely'),
      '#attributes' => [
        "onclick" => "jQuery(this).addClass('disabled');",
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $form['terms'] = [
      '#type' => 'markup',
      '#markup' => '<div class="process-heading">
        <p>Clicking "Pay" below authroizes us to charge your card for the total show above.</p>
        <p><strong>Please click only once. This step may take up to 60 seconds.</strong></p>
      </div>',
    ];

    return $form;
  }


  public function checkUserEmailValidation(array $form, FormStateInterface $form_state) {
   $ajax_response = new AjaxResponse();
   $text = "";
  // Check if User or email exists or not
   if (user_load_by_name($form_state->getValue('email')) || user_load_by_mail($form_state->getValue('email'))) {
     $text = 'Email address is already exists, please <a href="/user/login">login</a> using same email address.';
   }

   $ajax_response->addCommand(new HtmlCommand('#user-email-result', $text));
   return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (user_load_by_name($form_state->getValue('email')) || user_load_by_mail($form_state->getValue('email'))) {
     $form_state->setErrorByName('name="email"', t('User already exists'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new ReplaceCommand('#subscription_modal_form', $form));
      //$response->addCommand(new ReplaceCommand('#debug-out', $form ));
      //$response->addCommand(new PrependCommand('#debug-out', 'Forms contains an error', []));
    }
    else {
      $payload = [];
      $keys = ['first_name','last_name','email','role','card_number','expiry_date','secuirty_code','address','city','zip_code','state','country','subscription_amount','subscription_type'];
      foreach ($form_state->getValues() as $key => $value) {
        if (in_array($key, $keys)){
          $payload[$key] = $value;
        }
      }

      $authorizeNetPayment = new AuthorizedExpress();
      $payresponse = $authorizeNetPayment->chargeCreditCard($payload);

      if ($payresponse != null) {
        $transaction_response = $payresponse->getTransactionResponse();

        if ($transaction_response != null) {
          $transactionId = $transaction_response->getTransId();
          $responseCode = $transaction_response->getResponseCode();
          $paymentStatus = $authorizeNetPayment->responseText[$transaction_response->getResponseCode()];
        }

        if (($transaction_response != null) && ($transaction_response->getResponseCode()=="1")) {
            $authCode = $transaction_response->getAuthCode();
            $paymentResponse = $transaction_response->getMessages()[0]->getDescription();
            $reponseType = "Transaction Successful";
            $message = "Thank you for registering with, please check the mail for further details.";
            $message .= "Your transaction has been approved. <br/>Keep transaction id  : " . $transaction_response->getTransId() . " for refrence" . "\n";

            $uid = $authorizeNetPayment->insertUser($payload);
            $authorizeNetPayment->insertTranscation($payload, $transactionId, $authCode, $responseCode, $paymentStatus, $uid);
        }
        else {
            $authCode = "";
            $error_code = "0";
            $error_msg = "0";
            $reponseType = "Error";
            $message = "Transcation Failed!" . "</br>";
            if ($transaction_response != null && $transaction_response->getErrors() != null) {
              $error_code = $transaction_response->getErrors()[0]->getErrorCode();
              $error_msg = $transaction_response->getErrors()[0]->getErrorText();

              $message .= " Error code : " . $transaction_response->getErrors()[0]->getErrorCode() . "</br>";
              $message .= " Error message : " . $transaction_response->getErrors()[0]->getErrorText() . "</br>";
            } else {
              $error_code = $payresponse->getMessages()->getMessage()[0]->getCode();
              $error_msg = $payresponse->getMessages()->getMessage()[0]->getText();

              $message .= " Error code : " . $payresponse->getMessages()->getMessage()[0]->getCode() . "</br>";
              $message .= " Error message : " . $payresponse->getMessages()->getMessage()[0]->getText() . "</br>";
            }

            $authorizeNetPayment->insertFailTranscation($payload, $error_code, $error_msg, 'Fail', 0);
        }

        $response->addCommand(new OpenModalDialogCommand($reponseType, $message, ['width' => 800]));

      }
      else {
          $reponseType = "Error";
          $message= "Charge Credit Card Null response returned";
          $response->addCommand(new OpenModalDialogCommand($reponseType, $message, ['width' => 800]));
      }
    }

    return $response;
  }
}
