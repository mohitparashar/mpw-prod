<?php

namespace Drupal\mpw_subscription\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

use Drupal\mpw_subscription\Controller\MpwPayFlowSubscription;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class CancelPayflowSubscriptionForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $nid;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $uid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL, $nid  = NULL, $uid  = NULL) {
    $this->id = $id;
    $this->nid = $nid;
    $this->uid = $uid;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $message = $this->suspendSubscription($this->id, $this->nid, $this->uid);
    // Show message.
    Drupal\Core\Messenger\MessengerInterface::addMessage($message['message']);
    // Redirect to subscription
    $url = \Drupal\Core\Url::fromRoute('mpw_subscription.subscription')
          ->setRouteParameters(array('user' => $this->uid));

    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_payflow_subscription_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $url = \Drupal\Core\Url::fromRoute('mpw_subscription.subscription')
          ->setRouteParameters(array('user' => $this->uid));
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to De-active subscription for %id?', ['%id' => $this->id]);
  }

  /**
   * Helper function.
   */
  function suspendSubscription($profileid, $uid, $nid) {
    $payflow_subscription = new MpwPayFlowSubscription();
    $card_response = $payflow_subscription->cancelSubscription($profileid);

    if ($card_response['RESULT'] == '0') {
      $response['status'] = 'success';
      $response['message'] = 'Subscription has been cancel successfully!';
      // Update user subscription
      // Change status of subscription
      \Drupal::database()->update('mpw_rc_subscription')
       ->fields(array('subscription_state' => 'Canceled'))
       ->condition('uid', $uid)
       ->condition('payer_id', $profileid)
       ->execute();

      $node = Node::load($nid);
      if ($node) {
        $node->set('field_show_broker_information', FALSE);
        $node->save();
      }
    }
    else {
      $response['status'] = 'error';
      $response['message'] = $card_response['RESPMSG'];
    }

    return $response;
  }

}
