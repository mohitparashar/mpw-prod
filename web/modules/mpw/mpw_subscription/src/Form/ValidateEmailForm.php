<?php
/**
 * @file
 * Contains \Drupal\mpw_subscription\Form\SubscriptionForm.
 */

namespace Drupal\mpw_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;

/**
 * Contribute form.
 */
class ValidateEmailForm extends FormBase {
   /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mpw_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $subscription_type = \Drupal::request()->query->get('s');

    $form['#prefix'] = '<div id="emailvalidate_form">';
    $form['#suffix'] = '</div>';

    $form['subscription_type'] = array(
      '#type' => 'hidden',
      '#value' => $subscription_type,
      '#weight' => -1,
    );

    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => 'Email',
      '#required' => TRUE,
      '#prefix' => '<div id="user-email-result"></div>',
      // '#ajax' => array(
      //    'callback' => '::checkUserEmailValidation',
      //    'effect' => 'fade',
      //    'event' => 'change',
      //     'progress' => array(
      //        'type' => 'throbber',
      //        'message' => NULL,
      //     ),
      // ),
      '#weight' => -2,
    );

    $form['terms_condition'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('By clicking the checkbox, you agree to My Perfect Workplace <a id="terms-condition-lb" style="color:#be1722" href="javascript:;">Terms and Conditions.</a>'),
    );

    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pay Securely'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'pay-secure-button'
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
      '#weight' => -3,
      '#states' => array(
        'disabled' => array(
          ':input[name="terms_condition"]' => array('checked' => FALSE),
        ),
      ),
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'mpw_subscription/mpw-subscription';

    return $form;

   }

  public function checkUserEmailValidation(array $form, FormStateInterface $form_state) {
   $ajax_response = new AjaxResponse();
   $text = "";
    if ($form_state->getValue('email') == "") {
      $text = 'Email address field is required.';
    }
   // Check if User or email exists or not
   elseif (user_load_by_name($form_state->getValue('email')) || user_load_by_mail($form_state->getValue('email'))) {
     $text = 'Email address already exists, please <a href="/user/login">login</a> using same email address and password already created. If you have forgot your account information, contact us at (888) 518-9168 Monday-Friday from 9AM to 5PM Pacific Time (PST).';
   }

   $ajax_response->addCommand(new ReplaceCommand('#emailvalidate_form', $form));
   $ajax_response->addCommand(new HtmlCommand('#user-email-result', t($text)));

   return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('email') != ""
      && (user_load_by_name($form_state->getValue('email')) || user_load_by_mail($form_state->getValue('email')))) {
      $text = 'Email address already exists, please <a href="/user/login">login</a> using same email address and password already created. If you have forgot your account information, contact us at (888) 518-9168 Monday-Friday from 9AM to 5PM Pacific Time (PST).';
      $form_state->setErrorByName('name="email"', t($text));
    }
    elseif (!\Drupal::service('email.validator')
    ->isValid($form_state->getValue('email'))) {
      $text = 'Email is not valid, please enter valid email address.';
      $form_state->setErrorByName('name="email"', t($text));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('subscription_type') == 'bl') {
        $url = Url::fromRoute('mpw_subscription.get_rc_plan');
    }
    elseif ($form_state->getValue('subscription_type') == 'cl') {
      $url = Url::fromRoute('mpw_subscription.get_rc_concierge');
    }
     elseif ($form_state->getValue('subscription_type') == 'el') {
      $url = Url::fromRoute('mpw_subscription.get_rc_elite');
    }
    else {
      $url = Url::fromRoute('contact.site_page');
    }
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new ReplaceCommand('#emailvalidate_form', $form));
      //$response->addCommand(new HtmlCommand('#user-email-result', $text));
    }
    else {

      if ($form_state->getValue('subscription_type') == 'bl') {
        $url = Url::fromRoute('mpw_subscription.get_rc_plan');
      }
      elseif ($form_state->getValue('subscription_type') == 'cl') {
        $url = Url::fromRoute('mpw_subscription.get_rc_concierge');
      }
      elseif ($form_state->getValue('subscription_type') == 'el') {
        $url = Url::fromRoute('mpw_subscription.get_rc_elite');
      }
      else {
        $url = Url::fromRoute('contact.site_page');
      }

      $command = new RedirectCommand($url->toString());
      $response->addCommand($command);
    }

    return $response;
  }

}
