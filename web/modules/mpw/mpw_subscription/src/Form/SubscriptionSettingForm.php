<?php

/**
 * @file
 * Contains Drupal\mpw_subscription\Form\SettingsForm.
 */
namespace Drupal\mpw_subscription\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Class SettingsForm.
 *
 * @package Drupal\mpw_subscription\Form
 */
class SubscriptionSettingForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mpw_subscription.settings',
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mpw_subscription_setting_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mpw_subscription.settings');


    $form['basic_listing'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Basic listing price'),
      '#default_value' => $config->get('basic_listing'),
    );

    $form['standard_listing'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Standard listing price'),
      '#default_value' => $config->get('standard_listing'),
    );

    $form['elite_listing'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Elite listing price'),
      '#default_value' => $config->get('elite_listing'),
    );

    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('mpw_subscription.settings')
      // Flicker settings
      ->set('basic_listing', $form_state->getValue('basic_listing'))
      ->set('standard_listing', $form_state->getValue('standard_listing'))
      ->set('elite_listing', $form_state->getValue('elite_listing'))
      // Save
      ->save();
    }
  }
