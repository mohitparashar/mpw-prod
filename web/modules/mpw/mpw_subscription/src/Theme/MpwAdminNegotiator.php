<?php

namespace Drupal\mpw_subscription\Theme;

use Drupal\user\Theme\AdminNegotiator;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

class MpwAdminNegotiator implements ThemeNegotiatorInterface {

  public function applies(RouteMatchInterface $route_match) {
    $roles = \Drupal::currentUser()->getRoles();
    $route_name = \Drupal::routeMatch()->getRouteName();

    $mpw_route = [
      'mpw_subscription.subscription',
      'mpw_subscription.leads',
      'simplenews.newsletter_subscriptions_user',
      'entity.user.edit_form',
      'entity.user.canonical',
      'view.user_property.page_1'
    ];

    if (in_array('authenticated', $roles) && in_array($route_name, $mpw_route)) {
      return 'seven';
    }
  }

  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $route_name = \Drupal::routeMatch()->getRouteName();

    $mpw_route = [
      'mpw_subscription.subscription',
      'mpw_subscription.leads',
      'simplenews.newsletter_subscriptions_user',
      'entity.user.edit_form',
      'entity.user.canonical',
      'view.user_property.page_1'
    ];
    $roles = \Drupal::currentUser()->getRoles();
    if (in_array('authenticated', $roles) && in_array($route_name, $mpw_route)) {
      return 'seven';
    }
  }
}
