<?php

namespace Drupal\mpw_subscription\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class MpwNodeInsertEvent extends Event {

  const MPW_NODE_INSERT = 'mpw_event_subscriber.node.insert';

  /**
   * Node entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a node insertion event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get the inserted entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity() {
    if ($node = Node::load($this->entity->id())) {
      return $node;
    }
    return $this->entity;
  }
}
