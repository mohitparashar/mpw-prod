<?php

namespace Drupal\mpw_subscription\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\mpw_subscription\Event\MpwNodeInsertEvent;

/**
 * Logs the creation of a new node.
 */
class MpwNodeInsertSubscriber implements EventSubscriberInterface {

  /**
   * Log the creation of a new node.
   *
   * @param \Drupal\event_subscriber_demo\Event\MpwNodeInsertEvent $event
   */
  public function onPropertyNodeInsert(MpwNodeInsertEvent $event) {
    $entity = $event->getEntity();
    $array_value = $entity->toArray();

    $address = $array_value['field_address'][0]['address_line1'] . ', ' .
      $array_value['field_address'][0]['locality'] . ', '.
      $array_value['field_address'][0]['administrative_area']. ', '.
      $array_value['field_address'][0]['country_code'];

    $geocoder = \Drupal::service('geocoder');
    $geocode = $geocoder->geocode($address, ['googlemaps']);

    $latitude = $geocode->first()->getCoordinates()->getLatitude();
    $longitude = $geocode->first()->getCoordinates()->getLongitude();

    $point = [
      'lat' => $latitude,
      'lon' => $longitude,
    ];

    $value = \Drupal::service('geofield.wkt_generator')->WktBuildPoint($point);
    $entity->field_geolocation->setValue($value);
    // $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MpwNodeInsertEvent::MPW_NODE_INSERT][] = ['onPropertyNodeInsert'];
    return $events;
  }
}
