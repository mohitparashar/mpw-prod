<?php

namespace Drupal\mpw_subscription;

use Drupal\Component\Utility\Html;
use Drupal\taxonomy\Entity\Term;

/**
 * Generate URL aliases for property.
 */
class PropertyPathAlias {

  // All parent terms.
  protected $parent_terms = [
    'industrial' => 'industrial',
    'retail' => 'retail',
  ];

  // Child terms associate with there parent.
  protected $child_terms = [
    'industrial' => [
      'warehouse' => 'warehouse'
    ]
  ];

  // Replace pattern.
  protected $pattern = 'office-space';

  /**
   * Generate new alias if needed.
   */
  public function generate($alias, $context) {
    if ($context['bundle'] === 'property' && ($context['op'] == 'insert' || $context['op'] == 'update')) {
      return $this->assembleAlias($alias, $context['data']['node']);
    }
  }

  /**
   * Perpare alias.
   */
  protected function assembleAlias($alias, $entity) {

    $date = new \DateTime(date('c', $entity->getCreatedTime()));
    $parameters = [
      'office-space' => $this->findTermAlias($entity),
    ];

    if (!empty($parameters['office-space'])) {
      return str_replace(array_keys($parameters), array_values($parameters), $alias);
    }
  }

  /**
   * Get correct terms to add in URL.
   */
  protected function findTermAlias($entity) {
    // Make sure to change `field_property_type` to the field you would like to check.
    if ($keywords = $entity->get('field_property_type')->getValue()) {
      foreach ($keywords as $data) {
        $term = Term::load($data['target_id']);
        $name = strtolower($term->getName());

        $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($data['target_id']);
        if ($parent) {
          $parent = reset($parent);
          $parent_name =  strtolower($parent->getName());
        }
        else {
          $parent_name = $name;
        }

        if (isset($this->child_terms[$parent_name]) && in_array($name, array_keys($this->child_terms[$parent_name]))) {
          $type = $this->findPropertyType($entity);
          return $this->parent_terms[$parent_name] . '/' . $this->child_terms[$parent_name][$name] . '-for-' . $type;
        }
        else if (isset($this->parent_terms[$parent_name]) && in_array($parent_name, array_keys($this->parent_terms))) {
          $type = $this->findPropertyType($entity);
          return $this->parent_terms[$parent_name]  . '-space-for-' . $type;
        }
      }
    }
  }

  protected function findPropertyType ($entity) {
    $lease_type = $entity->get('field_listing_type')->getValue();
    if ($lease_type[0]['value'] == 'lease') {
      return 'rent';
    }
    return $lease_type[0]['value'];
  }

}