<?php
/**
 * @file
 * Contains \Drupal\mpw_subscription\Controller\MpwSubscription.
 */
namespace Drupal\mpw_subscription\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\mpw_subscription\PaypalExpress;
use Drupal\mpw_subscription\Constants\Constants;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;

use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;


class MpwSubscription extends ControllerBase {
  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;
  private $connection;
  /**
   * The ModalFormExampleController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   */
  public function __construct(FormBuilder $formBuilder) {
    $this->formBuilder = $formBuilder;
    $this->connection = \Drupal::service('database');
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * Cresinfo
   */
  public function cresinfo() {

    $config = \Drupal::config('mpw_subscription.settings');
    $standard_listing = $config->get('basic_listing');
    $concierge_listing = $config->get('standard_listing');
    $elite_listing = $config->get('elite_listing');
    // $" . $standard_listing ."/m
    return array(
      '#type' => 'markup',
      '#markup' => "
        <div class='pricing-container'>
          <div class='pricing-title'><h1>How much exposure does your commercial space need? Free trial for 60 days!</h1></div>
          <div class='silver'>
            <h1>Basic Listing</h1>
            <h2>Basic No-Frills Package</h2>
            <div class='price'>Free</div>
            <p>You list a single vacancy on our website</p>
            <span></span>
            <p>We ensure your vacancy is up to date</p>
            <span></span>
            <p>Free for 365 days than $" . $standard_listing ."/m</p>
            <span></span>
            <p class='border'><strong>Try us before committing! 365 day free trial!</strong></p>
            <span></span>
            <a class='button use-ajax' data-dialog-options='{&quot;width&quot;:450}' data-dialog-type='modal' href='/validate-email-form?s=bl'>Select Plan</a>
          </div>
          <div class='gold'>
            <h1>Concierge Listing</h1>
            <h2>The Standard Package</h2>
            <div class='price'>$" . $concierge_listing . "/m</div>
            <p>You list a single vacancy on our website</p>
            <span></span>
            <p>We market your vacancy on Google & Bing</p>
            <span></span>
            <p>We market your vacancy on social media</p>
            <span></span>
            <p>We ensure your vacancy is up to date</p>
            <span></span>
            <p class='border'><strong>Try us before committing! 60 day free trial!</strong></p>
            <span></span>
            <a class='button use-ajax' data-dialog-options='{&quot;width&quot;:450}' data-dialog-type='modal' href='/validate-email-form?s=cl'>Select Plan</a>
          </div>
          <div class='plat'>
            <h1>Enterprise Level</h1>
            <h2>Elite! Enough Said!</h2>
            <div class='price'>$" . $elite_listing . "/m</div>
            <p>You must have 10 or more active monthly vacancies</p>
            <span></span>
            <p>You have all the benefits of the concierge vacancy</p>
            <span></span>
            <p>You have a dedicated account manager</p>
            <span></span>
            <a class='button use-ajax' data-dialog-options='{&quot;width&quot;:450}' data-dialog-type='modal' href='/validate-email-form?s=el'>Select Plan</a>
          </div>
        </div>",);
  }

  /**
   * Get subscription plan.
   */
  public function getplan() {
    $response = new AjaxResponse();
    // Get the modal form using the form builder.
    $modal_form = $this->formBuilder->getForm('Drupal\mpw_subscription\Form\SubscriptionForm');
    // Add an AJAX command to open a modal dialog with the form as the content.
    $response->addCommand(new OpenModalDialogCommand('Personal information', $modal_form, ['width' => '800']));
    return $response;
  }

  /**
   * Pay for basic listing.
   */
  public function payProperty() {
    $config = $this->config('mpw_subscription.settings');
    $basic_listing = $config->get('basic_listing');
    $authorizeNetPayment = new PaypalExpress($basic_listing);
    $authorizeNetPayment->pay();
    exit();
  }

  /**
   * Pay for concierge listing.
   */
  public function payConcierge() {
    $config = $this->config('mpw_subscription.settings');
    $standard_listing = $config->get('standard_listing');
    $authorizeNetPayment = new PaypalExpress($standard_listing);
    $authorizeNetPayment->pay();
    exit();
  }

  /**
   * Paypal return url.
   */
  public function payReturn() {
    $paymentId = \Drupal::request()->query->get('paymentId');
    $PayerID = \Drupal::request()->query->get('PayerID');
    if (empty($paymentId) || empty($PayerID)) {
      throw new Exception('The response is missing the paymentId and PayerID');
    }

    $apiContext = $this->mpwApiContext();
    $payment = Payment::get($paymentId, $apiContext);
    $execution = new PaymentExecution();
    $execution->setPayerId($PayerID);

    try {
      // Take the payment
      $payment->execute($execution, $apiContext);
      try {
          $payment = Payment::get($paymentId, $apiContext);

          $data = [
            'product_id' => ($payment->transactions[0]->amount->total == "49") ? 1 : 2,
            'pay_id' => $payment->getId(),
            'pay_state'=> $payment->getState(),
            'invoice_number'=> $payment->transactions[0]->invoice_number,
            'payment_amount'=> $payment->transactions[0]->amount->total,
            'payer_id'=> $payment->payer->payer_info->getPayerId(),
            'payer_frist_name'=> $payment->payer->payer_info->getFirstName(),
            'payer_last_name'=> $payment->payer->payer_info->getLastName(),
            'payer_email'=> $payment->payer->payer_info->getEmail(),
            'payer_phone'=> $payment->payer->payer_info->getPhone(),
            'payer_address'=> $payment->payer->payer_info->getShippingAddress()->line1,
            'payer_city'=> $payment->payer->payer_info->getShippingAddress()->city,
            'payer_state'=> $payment->payer->payer_info->getShippingAddress()->state,
            'payer_postal_code'=> $payment->payer->payer_info->getShippingAddress()->postal_code,
            'subscription_start' => Drupal::time()->getRequestTime(),
            'subscription_end' => strtotime("+30 days"),
          ];
          if ($this->addPayment($data) !== false && $data['pay_state'] === 'approved') {
              // Payment successfully added, redirect to the payment complete page.
              // This is where you set the destination.
              $url = Url::fromRoute('entity.node.canonical', ['node' => 358]);
              $response = new RedirectResponse($url->toString());
              $response->send();
          } else {
            Drupal\Core\Messenger\MessengerInterface::addMessage("Payment failed");
          }
      } catch (Exception $e) {
        Drupal\Core\Messenger\MessengerInterface::addMessage("Failed to retrieve payment from PayPal");
      }
    }
    catch (Exception $e) {
      Drupal\Core\Messenger\MessengerInterface::addMessage("Failed to take payment");
    }
    $url = Url::fromRoute('entity.node.canonical', ['node' => 358]);
    $response = new RedirectResponse($url->toString());
    $response->send();
  }

  /**
   * Pay cancel url.
   */
  public function payCancel() {
    $ip = \Drupal::request()->getClientIp();
    $token = \Drupal::request()->query->get('token');
    $this->connection->insert('mpw_cancel_subscription')
      ->fields([
        'ip',
        'token',
        'created',
      ])
      ->values([
        'ip' => $ip,
        'token' => $token,
        'created' => Drupal::time()->getRequestTime(),
      ])
    ->execute();
    $url = Url::fromRoute('entity.node.canonical', ['node' => 359]);
    $response = new RedirectResponse($url->toString());
    $response->send();
  }

  /**
   * User subscription.
   */
  public function userSubscription(AccountInterface $user, Request $request) {
    $uid = $user->id();
    $query = \Drupal::database()->select('mpw_subscription', 'u');
    $query->fields('u',
      [
        'uid',
        'product_id',
        'pay_id',
        'invoice_number',
        'payer_id',
        'payer_frist_name',
        'payer_last_name',
        'payer_email',
        'payer_phone',
        'payer_address',
        'payer_city',
        'payer_state',
        'payer_postal_code',
        'subscription_start',
        'subscription_end'
      ]
    );
    $query->condition('uid', $uid);
    //For the pagination we need to extend the pagerselectextender and
    //limit in the query
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $results = $pager->execute()->fetchAll();
    // Initialize an empty array
    $output = array();
    $i = 1;
    // Next, loop through the $results array
    foreach ($results as $result) {
      if ($result->uid != 0 && $result->uid != 1) {
        $output[] = [
          'sno' => $i,
          'subscription' => ($result->product_id == 1) ? 'Property Listing' : 'Concierge Listing',
          //'pay_id' => $result->pay_id,
          'invoice_number' => $result->invoice_number,
          //'payer_id' => $result->payer_id,
          'payer_frist_name' => $result->payer_frist_name,
          'payer_last_name' => $result->payer_last_name,
          'payer_phone' => $result->payer_phone,
          'payer_address' => $result->payer_address,
          'payer_city' => $result->payer_city,
          'payer_state' => $result->payer_state,
          'payer_postal_code' => $result->payer_postal_code,
          'email' => $result->payer_email,
          'start' => date("F j, Y", $result->subscription_start),
          'end' => date("F j, Y", $result->subscription_end),
        ];
      }
      $i++;
    }

    $header = [
      'sno' => 'S.No',
      'subscription' => 'Subscription',
      //'pay_id' => 'Pay ID',
      'invoice_number' => 'Invoice Number',
      //'payer_id' =>'Payer ID',
      'payer_frist_name' => 'First Name',
      'payer_last_name' => 'Last Name',
      'payer_phone' => 'Phone',
      'payer_address' => 'Address',
      'payer_city' => 'City',
      'payer_state' => 'State',
      'payer_postal_code' => 'Postal Code',
      'email' => 'Email',
      'start' => 'Subscription Start',
      'end' => 'Subscription End'
    ];

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#empty' => t('No subscription found'),
    ];
    // Finally add the pager.
    $form['pager'] = array(
      '#type' => 'pager'
    );

    $property_subscription = Link::fromTextAndUrl(t('Renew Property Subscription'), Url::fromRoute('mpw_subscription.pay_property'))->toString();
    $form['property_subscription'] = array(
      '#type' => 'markup',
      '#markup' => $property_subscription . '</br>',
    );

    $concierge_subscription = Link::fromTextAndUrl(t('Renew Concierge Subscription'), Url::fromRoute('mpw_subscription.pay_concierge'))->toString();
    $form['concierge_subscription'] = array(
      '#type' => 'markup',
      '#markup' => $concierge_subscription . '</br>',
    );

    return $form;
 }

 /**
   * Record user leads.
   */
 public function userLeads(AccountInterface $user, Request $request) {
  $uid = $user->id();
  $query = \Drupal::database()->select('mpw_subscription_leads', 'u');
  $query->fields('u', ['payer_id','nid','name', 'phone', 'email', 'message', 'created']);
  $query->condition('payer_id', $uid);
  $query->orderBy("id", 'DESC');
  //For the pagination we need to extend the pagerselectextender and
  //limit in the query
  $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
  $results = $pager->execute()->fetchAll();
  // Initialize an empty array
  $output = array();
  // Next, loop through the $results array
  foreach ($results as $result) {
    if ($result->payer_id != 0 && $result->payer_id != 1) {
      $node_details = Node::load($result->nid);
      $link = Link::createFromRoute($node_details->getTitle(), 'entity.node.canonical', ['node' => $result->nid]);
      $output[] = [
        'nid' => $link,
        'name' => $result->name,
        'phone' => $result->phone,
        'email' => $result->email,
        'message' => \Drupal\Core\Render\Markup::create($result->message),
        'date' => date("F j, Y, H:i:s", $result->created),
      ];
    }
  }
  $header = ['nid' => 'Property', 'name' => 'Name', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message', 'date' => 'Date'];
  $form['table'] = [
    '#type' => 'table',
    '#header' => $header,
    '#rows' => $output,
    '#empty' => t('No Leads found'),
  ];
  // Finally add the pager.
  $form['pager'] = array(
    '#type' => 'pager'
  );

  return $form;
 }

  /**
   * Api context.
   */
  private function mpwApiContext() {
    $apiContext = new ApiContext(
        new OAuthTokenCredential(Constants::CLIENT_ID, Constants::CLIENT_SECRET)
    );

    $apiContext->setConfig([
        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
    ]);
    return $apiContext;
  }

  /**
   * Add payment to database.
   */
  private function addPayment($data) {
    try {
      $uid = $this->insertUser($data['payer_email']);
      $data['uid'] = $uid;
      if ($uid) {
        $subscription_dates = $this->getSubscriptionDate($uid);
         $id = $this->connection->insert('mpw_subscription')
          ->fields([
            'product_id',
            'pay_id',
            'pay_state',
            'invoice_number',
            'payment_amount',
            'uid',
            'payer_id',
            'payer_frist_name',
            'payer_last_name',
            'payer_email',
            'payer_phone',
            'payer_address',
            'payer_city',
            'payer_state',
            'payer_postal_code',
            'subscription_start',
            'subscription_end'
          ])
          ->values([
            'product_id' => $data['product_id'],
            'pay_id' => $data['pay_id'],
            'pay_state' => $data['pay_state'],
            'invoice_number' => $data['invoice_number'],
            'payment_amount' => $data['payment_amount'],
            'uid' => $data['uid'],
            'payer_id' => $data['payer_id'],
            'payer_frist_name' => $data['payer_frist_name'],
            'payer_last_name' => $data['payer_last_name'],
            'payer_email' => $data['payer_email'],
            'payer_phone' => $data['payer_phone'],
            'payer_address' => $data['payer_address'],
            'payer_city' => $data['payer_city'],
            'payer_state' => $data['payer_state'],
            'payer_postal_code' => $data['payer_postal_code'],
            'subscription_start' => $subscription_dates['start_date'],
            'subscription_end' => $subscription_dates['end_date'],
          ])
        ->execute();
        return $id;
      }

    }
    catch (Exception $e) {
      // Failed to insert payment from PayPal
      Drupal\Core\Messenger\MessengerInterface::addMessage("Some thing wrong went please try later.");
    }

  }

  /**
   * Create user.
   */
  public function insertUser($email) {

    $users = \Drupal::entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $email]);
    $user = reset($users);
    if ($user) {
      $uid = $user->id();
      $user->addRole('broker', 'authenticate');
      $user->activate();
      //Save user
      $user->save();
    }
    else {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $user = \Drupal\user\Entity\User::create();

      //Mandatory settings
      $user->setPassword('abc456');
      $user->enforceIsNew();
      $user->setEmail($email);
      $name = self::registrationCleanupUsername($email);
      $user->setUsername($name); //This username must be unique and accept only a-Z,0-9, - _ @ .

      //Optional settings
      $user->set("init", 'email');
      $user->set("langcode", $language);
      $user->set("preferred_langcode", $language);
      $user->set("preferred_admin_langcode", $language);
      $user->addRole('broker', 'authenticate');
      $user->activate();

      //Save user
      $user->save();
      _user_mail_notify('register_no_approval_required', $user);
      $uid = $user->id();
    }
    return $uid;
  }

  /**
   * Clean user for registration.
   */
  static function registrationCleanupUsername($name) {
    // Strip illegal characters.
    $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);
    $split_name = explode('@', $name);

    $name = $split_name[0];

    // Strip leading and trailing spaces.
    $name = trim($name);

    // Convert any other series of spaces to a single underscore.
    $name = preg_replace('/\s+/', '_', $name);

    return $name;
  }

  /**
   * get subscription dates.
   */
  private function getSubscriptionDate($uid) {
    $query = \Drupal::database()->select('mpw_subscription', 'u');
    $query->fields('u', ['subscription_start', 'subscription_end']);
    $query->condition('uid', $uid);
    $query->condition('subscription_end', time(), '>');
    $query->orderBy('id');
    $results = $query->execute()->fetchAll();

    $subscription_date['start_date'] = Drupal::time()->getRequestTime();
    $subscription_date['end_date'] = strtotime("+30 days");
    // Initialize an empty array
    if ($results) {
      foreach ($results as $result) {
        $end_date = $result->subscription_end;
      }
      $subscription_date['start_date'] = strtotime('+1 day' , $end_date);
      $subscription_date['end_date'] = strtotime('+31 day' , $end_date);
    }

    return $subscription_date;
  }

  /**
   * Helper function to redirect user to account.
   */
  function goUaccount($node) {
    $response = new RedirectResponse("/user");
    $response->send();
    return;
  }

}
