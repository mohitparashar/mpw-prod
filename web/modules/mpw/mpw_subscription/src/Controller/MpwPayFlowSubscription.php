<?php

/**
 * @file
 * Contains \Drupal\mpw_subscription\Controller\MpwPayFlowSubscription.
 */

namespace Drupal\mpw_subscription\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\mpw_subscription\PayFlowRC;

/**
 * Controller routines for pay flow routes.
 */
class MpwPayFlowSubscription extends ControllerBase {
	private $process_payflow;
	private $price;

  /**
   * get database conection and api auth.
   */
  public function __construct() {
    $this->connection = \Drupal::service('database');
  }

  /**
   * Callback for 'payflow-user-register' API method.
   */
  public function payflowRegistration(Request $request) {

  	$response['status'] = 'error';
		$response['message'] = 'Unknow error has been occurred!';
  	// This condition checks the `Content-type` and makes sure to
    // decode JSON string from the request body into array.
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
      $data = json_decode($request->getContent(), TRUE);
      $request->request->replace(is_array($data) ? $data : []);
	    $email = $request->request->get('email');

      $users = \Drupal::entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $email]);
      $user = reset($users);
      //exit('dd');
  	  if ($user) {
  		  $response['status'] = 'error';
  		  $response['message'] = 'User account already exists!';
  	  }
  	  else {
  	  	$user_data = $request->request->all();

  	  	$card_response = $this->chargeCreditCard($user_data);
  	  	if ($card_response['RESULT'] == '1') {
  	  		$response['status'] = 'error';
  			  $response['message'] = $card_response['RESPMSG'];
  	  	}
        elseif ($card_response['RESULT'] == '0') {
          // [RESULT] => 0
          // [RPREF] => R8350D1DCAE5
          // [PROFILEID] => RT0000000002
          // [RESPMSG] => Approved
          $plan_id = $this->_getProductId($user_data['plan']);
          $uid = $this->insertUser($user_data, $plan_id);
          // Create subscription plan.
          $this->recordSubscription($user_data, $card_response, $uid);
          // Return success.
          $response['status'] = 'success';
          $response['message'] = 'Account has been created successfully!';

        }
  	  	else {
          $response['status'] = 'error';
          $response['message'] = end(explode(":", $card_response['RESPMSG']));
  	  	}
	  }

    }
    else {
      $response['status'] = 'error';
      $response['message'] = 'Content Type should be application json.';
    }

	  //   print_r($request->request);
	  //   print_r($data);exit;
    //   $response['data'] = 'Some test data to return';
    //   $response['method'] = 'POST';
    return new JsonResponse($response);
  }


  public function payflowCancelSubscription(Request $request) {
    $response = [];
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
      $data = json_decode($request->getContent(), TRUE);
      $request->request->replace(is_array($data) ? $data : []);
      $profileid = $request->request->get('profileid');
      $card_response = $this->cancelSubscription($profileid);

      if ($card_response['RESULT'] == '0') {
        $response['status'] = 'success';
        $response['message'] = 'Subscription has been canceled successfully!';
        // update status on mpw rc recurring.
      }
      else {
        $response['status'] = 'error';
        $response['message'] = $card_response['RESPMSG'];
      }
    }
    return new JsonResponse($response);
  }


  public function cancelSubscription($profileid) {
    // Recurring Transaction.
    $PayFlow = new PayFlowRC('MPWINFO', 'PayPal', 'MohitMPW', 'MohitMPW123', 'recurring');
    // test or live.
    $PayFlow->setEnvironment('live');
    // S = Sale transaction, R = Recurring.
    $PayFlow->setTransactionType('R');
    // A = Automated clearinghouse, C = Credit card.
    $PayFlow->setPaymentMethod('C');
    // Only used for recurring transactions.
    $PayFlow->setProfileAction('C');
    // Set profile id.
    $PayFlow->setProfileId($profileid);

    $PayFlow->processTransaction();

    \Drupal::service('database')->update('mpw_rc_subscription')
      ->fields([
        'product_id' => 1,
        'next_billing_date' => 'N/A',
        'final_payment_date' => 'N/A',
      ])
      ->condition('payer_id', $profileid)
      ->execute();

    return $PayFlow->getResponse();
  }

  public function chargeCreditCard(array $user_data) {
  	foreach ($user_data as $key => $value) {
  	  $$key = $value;
  	}

	  $amount = $this->_getPrice($plan);
  	// Recurring Transaction
    $PayFlow = new PayFlowRC('MPWINFO', 'PayPal', 'MohitMPW', 'MohitMPW123', 'recurring');
	  // test or live
    $PayFlow->setEnvironment('live');
    // S = Sale transaction, R = Recurring
    $PayFlow->setTransactionType('R');
    // A = Automated clearinghouse, C = Credit card.
    $PayFlow->setPaymentMethod('C');
     // 'USD', 'EUR', 'GBP', 'CAD', 'JPY', 'AUD'.
    $PayFlow->setPaymentCurrency('USD');

    // Only used for recurring transactions
    $PayFlow->setProfileAction('A');
    $PayFlow->setProfileName($firstname . '_' . $lastname);
    $PayFlow->setProfileStartDate(date('mdY', strtotime("+1 day")));
    $PayFlow->setProfilePayPeriod('MONT');
    $PayFlow->setProfileTerm(0);

    $PayFlow->setAmount($amount, FALSE);
    $PayFlow->setCCNumber(trim($creditCard));
    $PayFlow->setCVV($cvc);
    $PayFlow->setExpiration(substr(str_replace("/", "", trim($expirationDate)), 0, 4));
    $PayFlow->setCreditCardName($firstname . ' ' . $lastname);

    $PayFlow->setCustomerFirstName($firstname);
    $PayFlow->setCustomerLastName($lastname);
    $PayFlow->setCustomerAddress($address);
    $PayFlow->setCustomerCity($city);
    $PayFlow->setCustomerState($state);
    $PayFlow->setCustomerZip($zip);
    $PayFlow->setCustomerCountry('US');
    $PayFlow->setCustomerPhone($phone);
    $PayFlow->setCustomerEmail($email);
    $PayFlow->setPaymentComment('Monthly Subscription Transaction');

    if($PayFlow->processTransaction()){
    	$this->process_payflow = TRUE;
    }
    else {
      $this->process_payflow = FALSE;
    }

    return $PayFlow->getResponse();
  }

  /**
   * Helper function to get prie
   */
  private function _getPrice($plan) {
  	$config = $this->config('mpw_subscription.settings');
  	$basic_listing = $config->get('basic_listing');
  	$standard_listing = $config->get('standard_listing');
  	$elite_listing = $config->get('elite_listing');
  	switch ($plan) {
  	  case 'bl':
  		  $price = $config->get('basic_listing');
  		break;
  	  case 'cl':
  		  $price = $config->get('standard_listing');
  		break;
  	  case 'el':
  		  $price = $config->get('elite_listing');
  		break;

  	  default:
  		  $price = $config->get('basic_listing');
  		break;
  	}

  	return $price;
  }


  /**
   * Helper function to get product plan.
   */
  private function _getProductId($plan) {
    switch ($plan) {
      case 'bl':
        $plan = 1;
      break;
      case 'cl':
        $plan = 2;
      break;
      case 'el':
        $plan = 3;
      break;

      default:
        $plan = 1;
      break;
    }

    return $plan;
  }


  /**
   * Helper function for registration clean up.
   */
  static function registrationCleanupUsername($name) {
    // Strip illegal characters.
    $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);
    $split_name = explode('@', $name);
 	  $name = $split_name[0];
    // Strip leading and trailing spaces.
    $name = trim($name);
    // Convert any other series of spaces to a single underscore.
    $name = preg_replace('/\s+/', '_', $name);

    return $name;
  }

   /**
   * Insert user into database.
   */
  public function insertUser($user_data, $product_id) {

    $users = \Drupal::entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $user_data['email']]);
    $user = reset($users);
    if ($user) {
      $uid = $user->id();
      $user->addRole('broker', 'authenticate');
      $user->activate();
      //Save user
      $user->save();
    }
    else {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $user = \Drupal\user\Entity\User::create();
      $elite_user = $product_id == 3 ? 1 : 0;
      //Mandatory settings
      $user->setPassword($user_data['pass']);
      $user->enforceIsNew();
      $user->setEmail($user_data['email']);
      $name = self::registrationCleanupUsername($user_data['email']);
      $user->setUsername($name); //This username must be unique and accept only a-Z,0-9, - _ @ .

      //Optional settings
      $user->set("init", 'email');
      $user->set("langcode", $language);
      $user->set("preferred_langcode", $language);
      $user->set("preferred_admin_langcode", $language);
      $user->set("field_elite_member", $elite_user);
      $user->set("field_profile_name", $user_data['firstname'] . ' ' . $user_data['lastname']);
      $user->addRole('broker', 'authenticate');
      $user->activate();

      //Save user
      $user->save();
      _user_mail_notify('register_no_approval_required', $user);
      $uid = $user->id();
    }
    return $uid;
  }


  /**
   * Record subscription.
   */
  public function recordSubscription($user_data, $server_response, $uid) {

    try {
      if ($uid) {
        //$subscription_dates = $this->getSubscriptionDate($uid);
         $id = $this->connection->insert('mpw_rc_subscription')
          ->fields([
            'product_id',
            'pay_id',
            'subscription_state',
            'payment_amount',
            'start_date',
            'uid',
            'nid',
            'payer_id',
            'pay_status',
            'payer_frist_name',
            'payer_last_name',
            'payer_email',
            'payer_address',
            'payer_city',
            'payer_state',
            'payer_postal_code',
            'next_billing_date',
            'last_payment_date',
            'final_payment_date',
            'subscription_start',
            'subscription_end',
            'pay_type'
          ])
          ->values([
            'product_id' => $this->_getProductId($user_data['plan']),
            'pay_id' => $server_response['RPREF'],
            'subscription_state' => $server_response['RESPMSG'],
            'payment_amount' => $this->_getPrice($user_data['plan']),
            'start_date' => date("Y-m-d H:i:s", time()),
            'uid' => $uid,
            'nid' => isset($user_data['nid']) ? $user_data['nid'] : 0,
            'payer_id' => $server_response['PROFILEID'],
            'pay_status'=> $server_response['RESPMSG'],
            'payer_frist_name' => $user_data['firstname'],
            'payer_last_name' => $user_data['lastname'],
            'payer_email' => $user_data['email'],
            'payer_address' => $user_data['address'],
            'payer_city' => $user_data['city'],
            'payer_state' => $user_data['state'],
            'next_billing_date' => date("Y-m-d H:i:s", strtotime("+60 days")),
            'last_payment_date' => date("Y-m-d H:i:s", strtotime("+90 days")),
            'final_payment_date' => date("Y-m-d H:i:s", strtotime("+425 days")),
            'payer_postal_code' => $user_data['zip'],
            'subscription_start' => Drupal::time()->getRequestTime(),
            'subscription_end' => strtotime("+365 days"),
            'pay_type' => 'payflow',
          ])
        ->execute();
        return $id;
      }

    }
    catch (Exception $e) {
      // Failed to insert payment from PayPal
      return FALSE;
    }
    return TRUE;
  }

}
