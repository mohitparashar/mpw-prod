<?php

namespace Drupal\mpw_subscription\Controller;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use \PayPal\Api\VerifyWebhookSignature;
use \PayPal\Api\WebhookEvent;
use Drupal\mpw_subscription\Constants\Constants;

use Drupal\Core\Controller\ControllerBase;

class MpwWebhook extends ControllerBase {

	private $apiContext;
	private $connection;

	public function __construct() {
		$this->apiContext = $this->_getRcApiContext(Constants::CLIENT_ID, Constants::CLIENT_SECRET);
		$this->connection = \Drupal::service('database');
	}

  	// Get API credentails.
 	private function _getRcApiContext ($clientId, $clientSecret) {
	    $apiContext = new ApiContext(
	        new OAuthTokenCredential($clientId, $clientSecret)
	    );

	    $apiContext->setConfig([
	        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
	    ]);
	    return $apiContext;
	}

	function webhook() {

		/**
		* Receive the entire body that you received from PayPal webhook.
		*/
		$bodyReceived = file_get_contents('php://input');

		/**
		* Receive HTTP headers that you received from PayPal webhook.
		*/
		$headers = getallheaders();

		// $bodyReceived = '{"id":"WH-UY687577TY25889J9-2R6T55435R66168Y6","create_time":"2018-19-12T22:20:32.000Z","resource_type":"subscription","event_type":"BILLING.SUBSCRIPTION.CANCELLED","summary":"A billing subscription was cancelled.","resource":{"quantity":"20","subscriber":{"name":{"given_name":"John","surname":"Doe"},"email_address":"customer@example.com","shipping_address":{"name":{"full_name":"John Doe"},"address":{"address_line_1":"2211 N First Street","address_line_2":"Building 17","admin_area_2":"San Jose","admin_area_1":"CA","postal_code":"95131","country_code":"US"}}},"create_time":"2018-12-10T21:20:49Z","shipping_amount":{"currency_code":"USD","value":"10.00"},"start_time":"2018-11-01T00:00:00Z","update_time":"2018-12-10T21:20:49Z","billing_info":{"outstanding_balance":{"currency_code":"USD","value":"10.00"},"cycle_executions":[{"tenure_type":"TRIAL","sequence":1,"cycles_completed":1,"cycles_remaining":0,"current_pricing_scheme_version":1},{"tenure_type":"REGULAR","sequence":2,"cycles_completed":1,"cycles_remaining":0,"current_pricing_scheme_version":1}],"last_payment":{"amount":{"currency_code":"USD","value":"500.00"},"time":"2018-12-01T01:20:49Z"},"next_billing_time":"2019-01-01T00:20:49Z","final_payment_time":"2020-01-01T00:20:49Z","failed_payments_count":2},"links":[{"href":"https://api.paypal.com/v1/billing/subscriptions/I-BW452GLLEP1G","rel":"self","method":"GET"},{"href":"https://api.paypal.com/v1/billing/subscriptions/I-BW452GLLEP1G","rel":"edit","method":"PATCH"}],"id":"I-BW452GLLEP1G","plan_id":"P-5ML4271244454362WXNWU5NQ","auto_renewal":true,"status":"CANCELLED","status_update_time":"2018-12-10T21:20:49Z"},"links":[{"href":"https://api.paypal.com/v1/notifications/webhooks-events/WH-UY687577TY25889J9-2R6T55435R66168Y6","rel":"self","method":"GET","encType":"application/json"},{"href":"https://api.paypal.com/v1/notifications/webhooks-events/WH-UY687577TY25889J9-2R6T55435R66168Y6/resend","rel":"resend","method":"POST","encType":"application/json"}],"event_version":"1.0","resource_version":"2.0"}';
		/**
		 * Receive the entire body that you received from PayPal webhook.
		 * Just uncomment the below line to read the data from actual request.
		 */
		/** @var String $bodyReceived */
		// $bodyReceived = file_get_contents('php://input');

		//  $headers = array (
		//   'Client-Pid' => '14910',
		//   'Cal-Poolstack' => 'amqunphttpdeliveryd:UNPHTTPDELIVERY*CalThreadId=0*TopLevelTxnStartTime=1579e71daf8*Host=slcsbamqunphttpdeliveryd3001',
		//   'Correlation-Id' => '958be65120106',
		//   'Host' => 'shiparound-dev.de',
		//   'User-Agent' => 'PayPal/AUHD-208.0-25552773',
		//   'Paypal-Auth-Algo' => 'SHA256withRSA',
		//   'Paypal-Cert-Url' => 'https://api.sandbox.paypal.com/v1/notifications/certs/CERT-360caa42-fca2a594-1d93a270',
		//   'Paypal-Auth-Version' => 'v2',
		//   'Paypal-Transmission-Sig' => 'K7DF5+hOyGS6v0xTZ9PkEO+BFNQmpryxw2hFlXbHgmxpu+883ldEC9HcL6ccEmB8sfy80y/Q3qcZZL1MR8Qi0e1ysylVj/yROgKK6zBsQPoNmgXC8yTiNoAjULuhk5Aoau0Itk3nrkgAfji1zvADlErMFM9VZe2nDvhM3nDMLjCi/K5fRjWp3eGo6Nx17GX/WIEibeKDyS+YdJMex3i+KO4aWmm9zznC/FJ5Y/ntd0MxOJNrM0r0f3y7c3e3sO5pB3oPgN6KzKVS5P6AWHqtS8acD3DjgCKZ1uqKz7q954G1aBz2/m0wRGOFFsv0/D3FE2uIheRRmoQtVSOAX7Aupg==',
		//   'Paypal-Transmission-Time' => '2019-08-29T10:49:33Z',
		//   'Paypal-Transmission-Id' => 'afa30d40-ca4a-11e9-ab68-7d4e2605c70c',
		//   'Accept' => '*/*',
		// );


		/**
		* Uppercase all the headers for consistency
		*/
		$headers = array_change_key_case($headers, CASE_UPPER);

		$signatureVerification = new VerifyWebhookSignature();
		$signatureVerification->setWebhookId("9NL49197B4633164L");
		$signatureVerification->setAuthAlgo($headers['PAYPAL-AUTH-ALGO']);
		$signatureVerification->setTransmissionId($headers['PAYPAL-TRANSMISSION-ID']);
		$signatureVerification->setCertUrl($headers['PAYPAL-CERT-URL']);
		$signatureVerification->setTransmissionSig($headers['PAYPAL-TRANSMISSION-SIG']);
		$signatureVerification->setTransmissionTime($headers['PAYPAL-TRANSMISSION-TIME']);

		$webhookEvent = new WebhookEvent();
		$webhookEvent->fromJson($requestBody);
		$signatureVerification->setRequestBody($webhookEvent);
		$request = clone $signatureVerification;

		try {
		    /** @var \PayPal\Api\VerifyWebhookSignatureResponse $output */
		    $output = $signatureVerification->post($this->apiContext);
		} catch (Exception $ex) {
			$message = 'An error accoured while on webhook';
		    \Drupal::logger('mpw_rc_subscription')->debug($message);
		}

		$verificationStatus = $output->getVerificationStatus();
		$responseArray = json_decode($request->toJSON(), true);

		$event = $responseArray['webhook_event']['resource']['event_type'];
		$outputArray = json_decode($output->toJSON(), true);
		// Log event
		\Drupal::logger('mpw_rc_subscription')->debug($event);

		if ($verificationStatus == 'SUCCESS') {
		  switch($event) {
		    case 'BILLING.SUBSCRIPTION.CANCELLED':
		    case 'BILLING.SUBSCRIPTION.SUSPENDED':
		      	 $this->connection->update('mpw_rc_subscription')
			        ->fields([
			          'subscription_state' => 'Suspended',
			        ])
			        ->condition('pay_id', $responseArray['webhook_event']['resource']['id'])
			        ->execute();
		      break;
		    case 'PAYMENT.SALE.COMPLETED':
		      //subscription payment recieved: agreement id = $responseArray['webhook_event']['resource']['billing_agreement_id']
		      break;
		  }
		}
	}
}
