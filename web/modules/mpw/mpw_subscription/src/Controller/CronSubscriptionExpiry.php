<?php
namespace Drupal\mpw_subscription\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\darwin_common\Utils\DateTimeUtils;
use \Drupal\user\Entity\User;

use Drupal\mpw_subscription\Constants\Constants;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class CronSubscriptionExpiry extends ControllerBase {

  /**
   * Construct database and auth api object.
   */
  public function __construct() {
    $this->apiContext = $this->_getRcApiContext(Constants::CLIENT_ID, Constants::CLIENT_SECRET);
    $this->connection = \Drupal::service('database');
  }

  /**
   * Cron for subscription expiry.
   */
  public function subscriptionExpiry() {

   	$query = \Drupal::database()->select('mpw_subscription', 'u');
	  $query->fields('u', ['uid', 'subscription_start', 'subscription_end']);
	  $results = $query->execute()->fetchAll();

    foreach ($results as $result) {
    	$today_date = time();
      $end = $result->subscription_end;
      if ($today_date > $end) {
      	$user = User::load($result->uid);
      	$user->removeRole('broker');
        $user->save();
			}
    }
    return [
    	'#type' => 'markup',
      '#markup' => 'Cron run successfully',
    ];
  }

  /**
   * Cron to check agreement status.
   */
  public function checkAgreementStatus() {
    $query = \Drupal::database()->select('mpw_rc_subscription', 'u');
    $query->fields('u', ['pay_id']);
    $query->condition('subscription_state', 'Pending');
    $results = $query->execute()->fetchAll();

    foreach ($results as $result) {
      $agreement = \PayPal\Api\Agreement::get($result->pay_id, $this->apiContext);
      $this->connection->update('mpw_rc_subscription')
        ->fields([
          'subscription_state' => $agreement->getState(),
          'next_billing_date' => $agreement->agreement_details->next_billing_date,
          'last_payment_date' => $agreement->agreement_details->last_payment_date,
          'final_payment_date' => $agreement->agreement_details->final_payment_date,
        ])
        ->condition('pay_id', $result->pay_id)
        ->execute();

      \Drupal::logger('mpw_subscription')->notice("Update subscription status for $result->pay_id to " . $agreement->getState());
    }
    exit('ok');
    return [
      '#type' => 'markup',
      '#markup' => 'Cron run successfully',
    ];
  }

  // Get API credentails.
  /**
   * Get API auth token.
   */
  private function _getRcApiContext ($clientId, $clientSecret) {
    $apiContext = new ApiContext(
        new OAuthTokenCredential($clientId, $clientSecret)
    );

    $apiContext->setConfig([
        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
    ]);
    return $apiContext;
  }

}
