<?php
/**
 * @file
 * Contains \Drupal\mpw_recurring_subscription\Controller\MpwRecurringSubscription.
 */

namespace Drupal\mpw_subscription\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\mpw_subscription\PaypalRcExpress;
use Drupal\mpw_subscription\Constants\Constants;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

use PayPal\Api\Payer;
use PayPal\Api\Agreement;

use Drupal\mpw_subscription\PayFlowRC;

class MpwRecurringSubscription extends ControllerBase {

	private $apiContext;

  /**
   * get database connection and api auth.
   */
	public function __construct() {
		$this->apiContext = $this->_getRcApiContext(Constants::CLIENT_ID, Constants::CLIENT_SECRET);
		$this->connection = \Drupal::service('database');
	}

  // Get API credentails.
  /**
   * Helper function to get connection.
   */
 	private function _getRcApiContext ($clientId, $clientSecret) {
    $apiContext = new ApiContext(
        new OAuthTokenCredential($clientId, $clientSecret)
    );

    $apiContext->setConfig([
        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
    ]);
    return $apiContext;
	}

  /**
   * Pay function for basic plan.
   */
	public function payRcProperty($uid = 0, $nid = 0) {
		$config = $this->config('mpw_subscription.settings');
		$basic_listing = $config->get('basic_listing');
		$paypalRcPayment = new PaypalRcExpress($basic_listing);
	    // current user id.
	    $uid = ($uid) ? $uid : \Drupal::currentUser()->id();
		$paypalRcPayment->payRc($uid, $nid, Constants::PLAN_CODE_BASIC);
		exit();
	}

  /**
   * Pay for concierge listing.
   */
	public function payRcConcierge($uid = 0, $nid = 0) {
		$config = $this->config('mpw_subscription.settings');
		$standard_listing = $config->get('standard_listing');
		$paypalRcPayment = new PaypalRcExpress($standard_listing);
		// current user id.
	  $uid = ($uid) ? $uid : \Drupal::currentUser()->id();
		$paypalRcPayment->payRc($uid, $nid, Constants::PLAN_CODE_CONCIERGE);
		exit();
	}

  /**
   * Pay for elite listing.
   */
  public function payRcElite($uid = 0, $nid = 0) {
    $config = $this->config('mpw_subscription.settings');
    $elite_listing = $config->get('elite_listing');
    $paypalRcPayment = new PaypalRcExpress($elite_listing);
    // current user id.
    $uid = ($uid) ? $uid : \Drupal::currentUser()->id();
    $paypalRcPayment->payRc($uid, $nid, Constants::PLAN_CODE_ELITE);
    exit();
  }

  /**
   * Return handler from paypal url.
   */
  public function payRcReturn() {
  	$token = $paymentId = \Drupal::request()->query->get('token');
    $agreement = new \PayPal\Api\Agreement();

    try {
        // Execute agreement
        $agreement->execute($token, $this->apiContext);
        $agreement = \PayPal\Api\Agreement::get($agreement->getId(), $this->apiContext);

        $description = $agreement->getDescription();
        $uidnid = explode("~", $description);
        $uid = $uidnid[1];
        $nid = $uidnid[2];
        $plan_type = isset($uidnid[3]) ? $uidnid[3] : 1;
		    $data = [
          'product_id' => $plan_type,
          'pay_id' => $agreement->getId(),
          'subscription_state' => $agreement->getState(),
          'payment_amount'=> $agreement->plan->payment_definitions[0]->amount->value,
    		  'start_date' => $agreement->start_date,
    		  'uid' => $uid,
    		  'nid' => isset($nid) ? $nid : 0,
          'payer_id'=> $agreement->payer->payer_info->payer_id,
		      'pay_status'=> $agreement->payer->status,
          'payer_frist_name'=> $agreement->payer->payer_info->first_name,
          'payer_last_name'=> $agreement->payer->payer_info->last_name,
          'payer_email'=> $agreement->payer->payer_info->email,
          'payer_address'=> $agreement->shipping_address->line1,
          'payer_city'=> $agreement->shipping_address->city,
          'payer_state'=> $agreement->shipping_address->state,
          'payer_postal_code'=> $agreement->shipping_address->postal_code,
    		  'next_billing_date' => $agreement->agreement_details->next_billing_date,
    		  'last_payment_date' => $agreement->agreement_details->last_payment_date,
    		  'final_payment_date' => $agreement->agreement_details->final_payment_date,
          'subscription_start' => Drupal::time()->getRequestTime(),
          'subscription_end' => strtotime("+30 days"),
        ];

    		// Update database.
    		if ($this->recordSubscription($data) !== false && $data['pay_status'] === 'verified') {
    			// Payment successfully added, redirect to the payment complete page.
    			// This is where you set the destination.
    			$url = Url::fromRoute('entity.node.canonical', ['node' => 358]);
    			$response = new RedirectResponse($url->toString());
    			$response->send();
    		} else {
          Drupal\Core\Messenger\MessengerInterface::addMessage("You are successfully subscribe for property listing, Payment status is pending");
    		}
      }
      catch (PayPal\Exception\PayPalConnectionException $ex) {
        Drupal\Core\Messenger\MessengerInterface::addMessage("Error accoured pleaes try again.");
      }
      catch (Exception $ex) {
        Drupal\Core\Messenger\MessengerInterface::addMessage("Error accoured pleaes try again.");
      }

      $url = Url::fromRoute('entity.node.canonical', ['node' => 358]);
      $response = new RedirectResponse($url->toString());
      $response->send();
  }

  /**
   * Cancel handler for paypal.
   */
  public function payRcCancel() {
    $ip = \Drupal::request()->getClientIp();
    $token = \Drupal::request()->query->get('token');
    $this->connection->insert('mpw_cancel_subscription')
      ->fields([
        'ip',
        'token',
        'created',
      ])
      ->values([
        'ip' => $ip,
        'token' => $token,
        'created' => Drupal::time()->getRequestTime(),
      ])
    ->execute();
    $url = Url::fromRoute('entity.node.canonical', ['node' => 359]);
    $response = new RedirectResponse($url->toString());
    $response->send();
  }

  /**
   * Record subscription.
   */
  private function recordSubscription($data) {

    try {
      $uid = $this->insertUser($data['payer_email'], $data['product_id']);
      $data['uid'] = $uid;
      if ($uid) {
        //$subscription_dates = $this->getSubscriptionDate($uid);
         $id = $this->connection->insert('mpw_rc_subscription')
          ->fields([
            'product_id',
            'pay_id',
            'subscription_state',
            'payment_amount',
            'start_date',
            'uid',
            'nid',
            'payer_id',
            'pay_status',
            'payer_frist_name',
            'payer_last_name',
            'payer_email',
            'payer_address',
            'payer_city',
            'payer_state',
            'payer_postal_code',
    		    'next_billing_date',
    		    'last_payment_date',
    		    'final_payment_date',
            'subscription_start',
            'subscription_end'
          ])
          ->values([
            'product_id' => $data['product_id'],
            'pay_id' => $data['pay_id'],
            'subscription_state' => $data['subscription_state'],
            'payment_amount' => $data['payment_amount'],
			       'start_date' => $data['start_date'],
            'uid' => $data['uid'],
            'nid' => $data['nid'],
            'payer_id' => $data['payer_id'],
            'pay_status'=> $data['pay_status'],
            'payer_frist_name' => $data['payer_frist_name'],
            'payer_last_name' => $data['payer_last_name'],
            'payer_email' => $data['payer_email'],
            'payer_address' => $data['payer_address'],
            'payer_city' => $data['payer_city'],
            'payer_state' => $data['payer_state'],
      			'next_billing_date' => (isset($data['next_billing_date'])) ? $data['next_billing_date'] : '',
      			'last_payment_date' => (isset($data['last_payment_date'])) ? $data['last_payment_date'] : '',
      			'final_payment_date' => $data['final_payment_date'],
            'payer_postal_code' => $data['payer_postal_code'],
            'subscription_start' => $data['subscription_start'],
            'subscription_end' => $data['subscription_end'],
          ])
        ->execute();
        return $id;
      }

    }
    catch (Exception $e) {
      // Failed to insert payment from PayPal
      Drupal\Core\Messenger\MessengerInterface::addMessage("Some thing wrong went please try later.");
    }

  }

/**
 * Record user subscription.
 */
public function userSubscription(AccountInterface $user, Request $request) {
  //$uid = \Drupal::currentUser()->id();
  $uid = $user->id();
  $query = \Drupal::database()->select('mpw_rc_subscription', 'u');
  $query->fields('u',
    [
      'uid',
      'nid',
      'pay_id',
      'subscription_state',
      'start_date',
      'pay_status',
      'product_id',
      'payment_amount',
      'next_billing_date',
      'last_payment_date',
      'final_payment_date',
      'pay_id',
      'payer_id',
      'payer_frist_name',
      'payer_last_name',
      'payer_email',
      'payer_address',
      'payer_city',
      'payer_state',
      'payer_postal_code',
      'subscription_start',
      'subscription_end',
      'pay_type'
    ]
  );
  $query->condition('uid', $uid);
  //For the pagination we need to extend the pagerselectextender and
  //limit in the query
  $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
  $results = $pager->execute()->fetchAll();
  // Initialize an empty array
  $output = array();
  $i = 1;
  // Next, loop through the $results array
  foreach ($results as $result) {
    if ($result->uid != 0 && $result->uid != 1) {

      switch ($result->product_id) {
        case '1':
          $product_id = 'Property Listing';
          break;
        case '2':
          $product_id = 'Concierge Listing';
          break;
        case '3':
          $product_id = 'Elite Listing';
          break;
        default:
          $product_id = 'Property Listing';
          break;
      }

      $node = Node::load($result->nid);
      if ($result->product_id == 1 && $result->pay_type == 'payflow') {
        $link = Link::createFromRoute($this->t('Upgrade Subscription'), 'mpw_subscription.upgrade_subscription',
          ['nid' => $result->nid, 'uid' => $result->uid],
          ['absolute' => TRUE]
        );
      }
      else {
        if ($result->pay_type == 'payflow') {
          $link = Link::createFromRoute($this->t('Deactivate Subscription'), 'mpw_subscription.cancel_payflow_subscription',
            ['id'=> $result->payer_id, 'nid' => $result->nid, 'uid' => $result->uid],
            ['absolute' => TRUE]
          );
        }
        else {
          $link = Link::createFromRoute($this->t('Deactivate Subscription'), 'mpw_subscription.suspend_subscription',
            ['id'=> $result->pay_id, 'nid' => $result->nid, 'uid' => $result->uid],
            ['absolute' => TRUE]
          );
        }
      }

      $output[] = [
        'sno' => $i,
        'subscription' => $product_id,
        //'nid' => "<a href='/node/$result->nid'> $node->getTitle(); </a>",
        'nid' => ($node) ? Link::createFromRoute($this->t($node->getTitle()), 'entity.node.canonical',
    			['node' => $result->nid],
    			['absolute' => TRUE]
    		) : '',
        'subscription_state' => $result->subscription_state,
        'payer_name' => $result->payer_frist_name . ' '. $result->payer_last_name,
        'payer_address' => $result->payer_address . ', ' . $result->payer_postal_code . ', ' . $result->payer_city . ', ' . $result->payer_state,
        'email' => $result->payer_email,
        'next_billing_date' => ($result->product_id == 1) ? 'N/A' : $result->next_billing_date,
        'last_payment_date' => $result->last_payment_date,
        'final_payment_date' => ($result->product_id == 1) ? 'N/A' : $result->final_payment_date,
        //Actions.
    		'action' => $link,
      ];
    }
    $i++;
  }

  $header = [
    'sno' => 'S.No',
    'subscription' => 'Plan',
    'nid' => 'Property',
    'subscription_state' =>'Status',
    'payer_name' => 'Payer',
    'payer_address' => 'Address',
    'email' => 'Email',
    'next_billing_date' => 'Next Billing',
    'last_payment_date' => 'Last Payment',
    'final_payment_date' => 'Final Payment',
    'Suspend_subscription' => 'Subscription',
  ];

  $form['table'] = [
    '#type' => 'table',
    '#header' => $header,
    '#rows' => $output,
    '#empty' => t('No subscription found'),
  ];
  // Finally add the pager.
  $form['pager'] = array(
    '#type' => 'pager'
  );

  return $form;
 }

  /**
   * Insert user into database.
   */
  public function insertUser($email, $product_id) {

    $users = \Drupal::entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $email]);
    $user = reset($users);
    if ($user) {
      $uid = $user->id();
      $user->addRole('broker', 'authenticate');
      $user->activate();
      //Save user
      $user->save();
    }
    else {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $user = \Drupal\user\Entity\User::create();
      $elite_user = $product_id == 3 ? 1 : 0;
      //Mandatory settings
      $user->setPassword('abc456');
      $user->enforceIsNew();
      $user->setEmail($email);
      $name = self::registrationCleanupUsername($email);
      $user->setUsername($name); //This username must be unique and accept only a-Z,0-9, - _ @ .

      //Optional settings
      $user->set("init", 'email');
      $user->set("langcode", $language);
      $user->set("preferred_langcode", $language);
      $user->set("preferred_admin_langcode", $language);
      $user->set("field_elite_member", $elite_user);
      $user->addRole('broker', 'authenticate');
      $user->activate();

      //Save user
      $user->save();
      _user_mail_notify('register_no_approval_required', $user);
      $uid = $user->id();
    }
    return $uid;
  }

  /**
   * Helper function for registration clean up.
   */
  static function registrationCleanupUsername($name) {
    // Strip illegal characters.
    $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);
    $split_name = explode('@', $name);
 	  $name = $split_name[0];
    // Strip leading and trailing spaces.
    $name = trim($name);
    // Convert any other series of spaces to a single underscore.
    $name = preg_replace('/\s+/', '_', $name);

    return $name;
  }

}
