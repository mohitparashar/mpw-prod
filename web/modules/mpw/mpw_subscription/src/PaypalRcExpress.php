<?php

/**
 * Recurring subscription plan.
 */
namespace Drupal\mpw_subscription;

use Drupal\mpw_subscription\Constants\Constants;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;

use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

use PayPal\Api\Payer;
use PayPal\Api\Agreement;


class PaypalRcExpress {
	// Paypal api context.
	private $apiContext;
	// Amount Payable
	protected $amountPayable;
	public $uid;
	public $nid;
    public $plan_type;

	// Constructor to assign amount.
	public function __construct($amountPayable) {
		$this->apiContext = $this->_getRcApiContext(Constants::CLIENT_ID, Constants::CLIENT_SECRET);
		$this->amountPayable = $amountPayable;
	}

  	// Get API credentails.
 	private function _getRcApiContext ($clientId, $clientSecret) {
	    $apiContext = new ApiContext(
	        new OAuthTokenCredential($clientId, $clientSecret)
	    );

	    $apiContext->setConfig([
	        'mode' => Constants::SANDBOX_MODE ? 'sandbox' : 'live'
	    ]);
	    return $apiContext;
	}

	// Get plan & Intialize payment.
    public function payRc($uid = 0, $nid = 0, $plan_type = 1) {
		$this->uid = $uid;
		$this->nid = $nid;
        $this->plan_type = $plan_type;
		// Create a new billing plan
		$plan = new Plan();
	    $plan->setName("Property Listing")
	        ->setDescription("Property listing services")
	        ->setType('fixed');
	    // Set payment definitions.
	    $plan->setPaymentDefinitions(
	    	$this->getPlanDefinition()
	    );
	    // Set merchant prederences.
	    $plan->setMerchantPreferences($this->getMerchantPreferences());

	    try {
	    	// Create plan.
	    	$createdPlan = $plan->create($this->apiContext);
	    	// Patched paln.
	    	$patchRequest = $this->getPatchedPlan();
		    // Update fetch plan.
		    $createdPlan->update($patchRequest, $this->apiContext);
		    $patchedPlan = Plan::get($createdPlan->getId(), $this->apiContext);
		    // Get agreement.
		    $agreement = $this->getAgreement();

		    // Set plan id
			$plan = new Plan();
			$plan->setId($patchedPlan->getId());
			$agreement->setPlan($plan);

			// Add payer type
			$payer = new Payer();
			$payer->setPaymentMethod('paypal');
			$agreement->setPayer($payer);

			try {
			    // Create agreement
			    $agreement = $agreement->create($this->apiContext);

			    // Extract approval URL to redirect user
			    $approvalUrl = $agreement->getApprovalLink();
			    // Move header location.
			    header("Location: " . $approvalUrl);
			    exit();
			} catch (PayPal\Exception\PayPalConnectionException $ex) {
			    echo $ex->getCode();
			    echo $ex->getData();
			    die($ex);
			} catch (Exception $ex) {
			    die($ex);
			}

	    }
	    catch (PayPal\Exception\PayPalConnectionException $ex) {
		    echo $ex->getCode();
		    echo $ex->getData();
		    die($ex);
		} catch (Exception $ex) {
		    die($ex);
		}
	    return $plan;
	}

	// Create plan definition.
	private function getPlanDefinition () {
		// Set billing plan definitions
		$paymentDefinition = new PaymentDefinition();
		$paymentDefinition->setName('Regular Payments')
		    ->setType('REGULAR')
		    ->setFrequency('Month')
		    ->setFrequencyInterval('1')
		    ->setCycles('12')
		    ->setAmount(new Currency(array(
			    'value' => $this->amountPayable,
			    'currency' => 'USD'
			))
		);

		// Set Trail billing plan definitions
		/*if (!$this->nid && $this->plan_type != '3') {
			$paymentTrailDefinition = new PaymentDefinition();
			// if plan is Basic then trail period will be for 12 month
			// if plan is Concierge than trail period will be for 2 month.
			$cycle = ($this->plan_type == 1) ? 12 : 2;
			$paymentTrailDefinition->setName('Trail Payments')
			    ->setType('TRIAL')
			    ->setFrequency('Month')
			    ->setFrequencyInterval("1")
			    ->setCycles($cycle)
			    ->setAmount(new Currency(array(
			    	'value' => 0,
			    	'currency' => 'USD'
			    ))
			);

			return [$paymentTrailDefinition, $paymentDefinition];
		}*/

		return [$paymentDefinition];
	}

	// Get merchant preferences.
	private function getMerchantPreferences() {
		// Set merchant preferences
	    $merchantPreferences = new MerchantPreferences();
	    $merchantPreferences->setReturnUrl(Constants::RETURN_URL)
	        ->setCancelUrl(Constants::CANCEL_URL)
	        ->setAutoBillAmount('yes')
	        ->setInitialFailAmountAction('CONTINUE')
	        ->setMaxFailAttempts('0')
	        ->setSetupFee(new Currency(array(
	        // 'value' => $this->amountPayable,
	        'value' => 0,
	        'currency' => 'USD'
	    )));

	    return $merchantPreferences;
	}

	// Get patched plan
	private function getPatchedPlan() {
		$patch = new Patch();
		$value = new PayPalModel('{"state":"ACTIVE"}');
		$patch->setOp('replace')
			->setPath('/')
			->setValue($value);
		$patchRequest = new PatchRequest();
		$patchRequest->addPatch($patch);

		return $patchRequest;
	}

	// Get agreement.
	private function getAgreement() {
		$startDate = date('c', time() + 3600);
		$agreement = new Agreement();
		$agreement->setName('Property Listing Agreement')
            ->setDescription('Property Billing Agreement for property ~' . $this->uid . '~' . $this->nid . '~' . $this->plan_type)
			->setStartDate($startDate);

		return $agreement;
	}
}
