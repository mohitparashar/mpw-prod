<?php

namespace Drupal\mpw_property_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides the frontpage lawyer search block.
 *
 * @Block(
 *   id = "mpw_property_search_frontpage_lawyer_search",
 *   admin_label = @Translation("MPW Property Search Frontpage Property Search")
 * )
 */
class MPWPropertySEarchFront extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'mpw_search_frontpage_property_search',
      '#property_search_form_front' => \Drupal::formBuilder()->getForm('Drupal\mpw_property_search\Form\PropertyStartpageSearchFrom'),
    ];
  }

}
