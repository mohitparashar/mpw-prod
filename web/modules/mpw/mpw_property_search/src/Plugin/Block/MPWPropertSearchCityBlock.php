<?php

namespace Drupal\mpw_property_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\views\Views;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a block with views attachment.
 *
 * @Block(
 *     id = "mpw_citypage_banner",
 *     admin_label = @Translation("mpw citypage banner"),
 * )
 */
class MPWPropertSearchCityBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $location_page_no_result_block = \Drupal\block\Entity\Block::load('officespacepageblock');
    $location_page_no_result_block = \Drupal::entityTypeManager()
      ->getViewBuilder('block')
      ->view($location_page_no_result_block);

    return [
      '#theme' => 'mpw_property_search_cityblock',
      '#office_space_views_city' => [
        '#type' => 'view',
        '#name' => 'office_space',
        '#display_id' => 'city_page_banner',
        '#embed' => TRUE,
      ],
      '#office_space_views_state' => [
        '#type' => 'view',
        '#name' => 'office_space',
        '#display_id' => 'state_page_banner',
        '#embed' => TRUE,
      ],
      '#location_page_no_result_block' => $location_page_no_result_block,
    ];
  }
}