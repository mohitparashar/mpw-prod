<?php

namespace Drupal\mpw_property_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\views\Views;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a block with views attachment.
 *
 * @Block(
 *     id = "mpw_citypage_content_map",
 *     admin_label = @Translation("mpw citypage content map"),
 * )
 */
class MPWPropertSearchCityContentMap extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'mpw_property_search_cityblock_content_map',
      '#office_space_views_city_content_map' => [
        '#type' => 'view',
        '#name' => 'office_space',
        '#display_id' => 'city_banner_map',
        '#embed' => TRUE,
      ],
    ];
  }
}
