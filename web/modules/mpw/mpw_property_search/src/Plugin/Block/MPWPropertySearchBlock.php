<?php

namespace Drupal\mpw_property_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\views\Views;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a block with views attachment.
 *
 * @Block(
 *     id = "mpw_citypage_property",
 *     admin_label = @Translation("mpw citypage property"),
 * )
 */
class MPWPropertySearchBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

        return [
            '#theme' => 'mpw_property_search_block',
            '#office_space_views_property' => [
                '#type' => 'view',
                '#name' => 'office_space',
                '#display_id' => 'search_property',
                '#embed' => TRUE,
            ],

        ];
    }
}