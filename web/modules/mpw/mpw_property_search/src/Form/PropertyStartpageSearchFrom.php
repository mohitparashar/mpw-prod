<?php

namespace Drupal\mpw_property_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;
use Drupal\geolocation\GoogleMapsDisplayTrait;

/**
 * Two-Slot-Search for frontpage.
 */
class PropertyStartpageSearchFrom extends FormBase {

  use GoogleMapsDisplayTrait;

  /**
   * ExposedPropertySearchForm.
   *
   * @var array
   *  exposedPropertySearchForm
   */
  public $exposedPropertySearchForm;

  /**
   * PropertStartpageSearchForm constructor.
   */
  public function __construct() {
    $property_search_view = Views::getView('office_space');
    $property_search_view->setDisplay('search_page');
    $property_search_display = $property_search_view->getDisplay();
    $property_search_view->initHandlers();

    /** @var \Drupal\views\Plugin\views\exposed_form\ExposedFormPluginBase $exposed_form_plugin */
    $exposed_form_plugin = $property_search_display->getPlugin('exposed_form');
    $this->exposedPropertySearchForm = $exposed_form_plugin->renderExposedForm(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->exposedPropertySearchForm['#id'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $exposed_form = $this->exposedPropertySearchForm;

    $form['#attributes']['class'][] = 'mpw--startpage-search-form';

//    var_dump($exposed_form);
    $form['location'] = $exposed_form['main_search_row']['location'];
    $form['location']['geolocation_geocoder_google_places_api']['#placeholder'] = $form['location']['geolocation_geocoder_google_places_api']['#title'];
    unset($form['location']['geolocation_geocoder_google_places_api']['#title']);
    unset($form['location']['#prefix']);
    unset($form['location']['#suffix']);

    $form['#attached']['library'][] = 'geolocation/geolocation.views.filter.geocoder';
    $form['#attached']['drupalSettings'] = $exposed_form['#attached']['drupalSettings'];

    $form['#action'] = $exposed_form['#action'];
    $form['#method'] = 'get';

    $form['submit'] = [
      '#type' => 'submit',
      '#weight' => 10,
      '#value' => $this->t('Go'),
    ];

    $form['#after_build'][] = [__CLASS__, 'removeFormTokens'];

    return $form;
  }

  /**
   * Remove unnecessary get parameters.
   *
   * @param array $form
   *   PropertySearchForm.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array
   *   Altered form.
   *
   * @see https://www.drupal.org/node/1191278
   */
  public static function removeFormTokens(array $form, FormStateInterface $form_state) {
    $form['form_id']['#access'] = FALSE;
    $form['form_token']['#access'] = FALSE;
    $form['form_build_id']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // We're redirecting to views, so nothing to do.
  }

}
