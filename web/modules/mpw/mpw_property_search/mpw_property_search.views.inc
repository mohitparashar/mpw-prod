<?php

/**
 * @file
 * AW24 lawyer search views handling.
 */

use Drupal\views\ViewExecutable;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\cache\CachePluginBase;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function mpw_property_search_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (in_array($form['#id'], [
    'views-exposed-form-office-space-search-page',
  ])) {
    // Translate exposed filter labels.
    // Add as placeholders.
    // @todo: Find a better way!
    foreach ($form['#info'] as &$info) {
      if (
        !empty($info['label'])
        && in_array($info['value'], [
          'location',
        ])
      ) {
        $form[$info['value']]['#placeholder'] = $info['label'];
      }
    }

    $form['location']['#weight'] = 1;
    $form['location']['#prefix'] = '<div class="col-xs-12 col-sm-6">';
    $form['location']['#suffix'] = '</div>';
    if (!empty($form['location']['#placeholder'])) {
    $form['location']['geolocation_geocoder_google_places_api']['#placeholder'] = $form['location']['#placeholder'];
  //    $form['location']['geolocation_geocoder_google_geocoding_api']['#placeholder'] = $form['location']['#placeholder'];
    }
  unset($form['location']['geolocation_geocoder_google_places_api']['#description']);
   // unset($form['location']['geolocation_geocoder_google_geocoding_api']['#description']);

    $form['main_search_row'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'row',
      ],
    ];

    $form['main_search_row']['location'] = $form['location'];
    unset($form['location']);

    $form['actions']['submit']['#value'] = t('Go');

    if (!empty($form['actions']['reset'])) {
      $form['actions']['reset'] += [
        '#attributes' => [
          'class' => [
            'form-reset',
          ],
        ],
      ];
    }
  }
}
