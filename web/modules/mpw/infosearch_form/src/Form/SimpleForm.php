<?php

namespace Drupal\infosearch_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Our simple form class.
 */
class SimpleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupalup_simple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['number_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First number'),
    ];

    $form['number_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second number'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //drupal_set_message($form_state->getValue('number_1') + $form_state->getValue('number_2'));
    $form_var = $form_state->getValue('number_1');

   $option = [
      'query' => ['testvar' => $form_var],
    ];

    $url = Url::fromUri('/info/tool', $option);

    //$form_state->setRedirect('infotool_controller.article_list');
   // $form_state->setRedirect($url);
  }

}
