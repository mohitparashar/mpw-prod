<?php

/**
 * @file
 * Contains \Drupal\paragraphs_validator\Plugin\Validation\Constraint\ParagraphChangedConstraint.
 */

namespace Drupal\paragraphs_validator\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for the paragraph entity changed timestamp.
 *
 * @Constraint(
 *   id = "ParagraphChanged",
 *   label = @Translation("Paragraph changed", context = "Validation"),
 *   type = {"entity"}
 * )
 */
class ParagraphChangedConstraint extends Constraint {
    public $message = 'The paragraph content has either been modified by another user, or you have already submitted modifications. As a result, your changes cannot be saved.';
}