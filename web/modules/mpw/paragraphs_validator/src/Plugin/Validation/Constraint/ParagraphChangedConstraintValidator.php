<?php

/**
 * @file
 * Contains \Drupal\paragraphs_validator\Plugin\Validation\Constraint\ParagraphChangedConstraintValidator.
 */

namespace Drupal\paragraphs_validator\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ParagraphChanged constraint.
 */
class ParagraphChangedConstraintValidator extends ConstraintValidator {
    /**
     * {@inheritdoc}
     */
    public function validate($entity, Constraint $constraint) {
        if(isset($entity)) {
            /** @var \Drupal\Core\Entity\EntityInterface $entity */
            if(!$entity->isNew()) {
                $saved_entity = \Drupal::entityManager()->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
                // A change to any other translation must add a violation to the current
                // translation because there might be untranslatable shared fields.

                /**
                 * Hotfix for paragraph entity type
                 *
                 * We should remove this check, because it was causing page saving issues sometimes.
                 */
                /*
                if($saved_entity && $saved_entity->getChangedTimeAcrossTranslations() > $entity->getChangedTimeAcrossTranslations()) {
                    $this->context->addViolation($constraint->message);
                }*/
            }
        }
    }
}