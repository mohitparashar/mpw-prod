(function($, undefined) {
	var scrollTop = $(window).scrollTop();
    var big_carousel_options = {
            items: 1,
			loop: true,//was not working with fade
            nav: true,
            navText: false,
            dots: true,
			autoplay:true,
			autoplayTimeout:15000,
            lazyLoad: true
        };
    
    $('#content').find('.paragraph.testimonial').each(function(idx, el) {
        el = $(el);
		if (!$('.paragraph.testimonial').hasClass('items-1')) {el.find('.field--name-field-testimonial-item').owlCarousel(big_carousel_options);};
        
    });
})(jQuery);