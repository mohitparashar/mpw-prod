(function($, undefined) {
    var big_carousel_options = {
            items: 5,
			loop: true,//was not working on DEV
            nav: true,
			//center:true,
            navText: false,
            dots: false,
            lazyLoad: true
        },
        items_to_responsive_mapping = {
            1: {
                0: {items: 1}
            },
            2: {
                0: {items: 1},
                620: {items: 2, margin: 20}
            },
            3: {
                0: {items: 1},
                620: {items: 2, margin: 20},
                768: {items: 3, margin: 20}
            },
            5: {
                0: {items: 1},
                480: {items: 2},
                620: {items: 3},
                768: {items: 4},
                960: {items: 5}
            }
        };
    
    $('#content').find('.paragraph.client-section').each(function(idx, el) {
        el = $(el);
        var autoplay = el.attr('data-autoplay'),
            items = 5;
        if(autoplay) $.extend(big_carousel_options, { autoplay: true, autoplayTimeout: autoplay, autoplayHoverPause: true });
        if(items) $.extend(big_carousel_options, { items: items, responsive: items_to_responsive_mapping[items] });
       el.find('.field--name-field-logo').owlCarousel(big_carousel_options);
    });
})(jQuery);