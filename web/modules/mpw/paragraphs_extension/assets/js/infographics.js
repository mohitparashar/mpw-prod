(function($, undefined) {
    var infographics_conts = $('.field--name-field-infographic'),
        infographics_items = $('.paragraph.infographics-item'),
        text_lines = infographics_items.find('.field--name-field-text-line').children('.field__item'),
        carousel_options = {
            items: 1,
           // loop: true,//was not working on DEV
            nav: true,
            navText: false,
            dots: false,
            lazyLoad: true,
            responsive: {
                0: {items: 1},
                620: {items: 2, margin: 20},
                768: {items: 3, margin: 20}
            },
            onInitialized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
            },
            onResized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                else arr_prev.removeClass('disabled');
                
                if(last.hasClass('active')) arr_next.addClass('disabled');
                else arr_next.removeClass('disabled');
            },
            onTranslated: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev').removeClass('disabled'),
                    arr_next = carousel.find('.owl-next').removeClass('disabled');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
            }
        };
    
    // attach number counter logic to selected line
    function initNumberCounter(line) {
        var counters = line.children('.number-counter');
        counters.each(function(idx, elem) {
            elem = $(elem);
            
            var token = (new Date()).getTime(),
                limit = elem.attr('data-number'),
                interval = parseInt(elem.attr('data-interval')),
                step = parseInt(elem.attr('data-step')),
                tmp;
            
            this['number_counter_' + token] = setInterval(function() {
                if((tmp = parseInt(elem.text())) < limit) elem.text(tmp + step);
                else {
                    clearInterval(this['number_counter_' + token]);
                    elem.text(limit);
                }
            }, interval);
        });
    }
    
    // generate interval and step for a number
    function generateCounterOptions(number) {
        options = {};
        
        /*
        if(number < 100) {
            options.interval = 20;
            options.step = 1;
        }
        else if(number < 1000) {
            options.interval = 20;
            options.step = 20;
        }
        else if(number < 5000) {
            options.interval = 20;
            options.step = 50;
        }
        else if(number < 10000) {
            options.interval = 20;
            options.step = 100;
        }
        else {
            options.interval = 1;
            options.step = 1000;
        }*/
        
        // these settings are temporary solution
        if(number < 100) {
            options.interval = 23;
            options.step = 1;
        }
        else if(number < 500) {
            options.interval = 20;
            options.step = 3;
        }
        else if(number < 1000) {
            options.interval = 20;
            options.step = 7;
        }
        else if(number < 5000) {
            options.interval = 20;
            options.step = 50;
        }
        else if(number < 10000) {
            options.interval = 20;
            options.step = 100;
        }
        else {
            options.interval = 1;
            options.step = 1000;
        }
        
        return options;
    }
    
    // identify lines with numbers and change theirs html structure
    text_lines.each(function(idx, elem) {
        elem = $(elem);
        var text = elem.text(),
            numbers = text.match(/\d+/g),// check if text line has numbers in it
            i = 0,
            regex,
            options;
        
        if(numbers) {
            for(i; i < numbers.length; i++) {
                regex = new RegExp(numbers[i], 'g');
                options = generateCounterOptions(numbers[i]);
                text = text.replace(regex, '<span class="number-counter" data-number="' + numbers[i] + '" data-interval="' + options.interval + '" data-step="' + options.step + '">0</span>');
            }
            elem.html(text).addClass('has-number');
            $(document).ready(function() {initNumberCounter(elem);});
        }
    });
    
    // if(!isMobile.phone) infographics_conts.owlCarousel(carousel_options);
})(jQuery);