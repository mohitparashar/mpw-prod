(function($, undefined) {
    $('#content').find('.paragraph.tabs').each(function(idx, el) {
        el = $(el);
        var tabs_header = el.find('.tabs-header'),
            tabs_body = el.find('.tabs-body'),
            tab_titles = tabs_header.children('.tab-title'),
            tab_bodies = tabs_body.children('.tab-body'),
            tmp;
        
        tab_titles.on('click', function(e) {
            tmp = $(e.currentTarget);
            tab_titles.removeClass('actv');
            tmp.addClass('actv');
            tab_bodies.removeClass('actv').filter('[data-id="' + tmp.attr('data-id') + '"]').addClass('actv');
        });
    });
})(jQuery);