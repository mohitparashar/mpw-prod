(function($, undefined) {
    var big_carousel_options = {
            items: 1,
			//loop: true,//was not working on DEV
            nav: true,
            navText: false,
            dots: false,
            lazyLoad: true,
			onInitialized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
            },
            onResized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
				if(first.hasClass('active')) arr_prev.addClass('disabled');
                else arr_prev.removeClass('disabled');
                
                if(last.hasClass('active')) arr_next.addClass('disabled');
                else arr_next.removeClass('disabled');
            },
            onTranslated: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev').removeClass('disabled'),
                    arr_next = carousel.find('.owl-next').removeClass('disabled');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
            }
        };
    $('#content').find('.paragraph.events').each(function(idx, el) {
        el = $(el);
        var autoplay = el.attr('data-autoplay'),
            items = el.attr('data-grid_items');
        if(autoplay) $.extend(big_carousel_options, { autoplay: true, autoplayTimeout: autoplay, autoplayHoverPause: true });
        if(items) $.extend(big_carousel_options, { items: items, responsive: items_to_responsive_mapping[items] });
        el.children('.holder').find('.field--name-field-slides').owlCarousel(big_carousel_options);
    });
})(jQuery);