(function($, undefined) {
	var scrollTop = $(window).scrollTop();
    var big_carousel_options = {
			animateOut: 'fadeOut',
            items: 1,
			//loop: true,//was not working with fade
			//video: true,
            nav: true,
            navText: false,
            dots: true,
			autoplay:true,
			autoplayTimeout: 6000,
            lazyLoad: true,
			onInitialized: function(e) {
				var video_cont = $('.field--name-field-paragraphs'),
					owl_item = video_cont.find('.owl-item');
					owl_item_actv = video_cont.find('.owl-item.active');
					if (owl_item_actv.find('.video-cont').length) {owl_item.find('video').get(0).play();} 
					owl_item.each(function(idx, el) {
					var el = $(el);
					if (el.hasClass('active')) {
						if (el.find('.video-cont').length) {el.find('video').get(0).play();} 
						el.find('.message').addClass('start').removeClass('end');
					} else {
						if (el.find('.message').hasClass('start')) {
							el.find('.message').removeClass('start').addClass('end');
						}
					}
				});
			},
			onResized: function() {
				var video_cont = $('.field--name-field-paragraphs'),
					owl_item = video_cont.find('.owl-item');
					owl_item_actv = video_cont.find('.owl-item.active');
					if (owl_item_actv.find('.video-cont').length) {owl_item.find('video').get(0).play();} 
					owl_item.each(function(idx, el) {
					var el = $(el);
					if (el.hasClass('active')) {
						if (el.find('.video-cont').length) {el.find('video').get(0).play();} 
						el.find('.message').addClass('start').removeClass('end');
					} else {
						if (el.find('.message').hasClass('start')) {
							el.find('.message').removeClass('start').addClass('end');
						}
					}
				});
			},
			onTranslated: function() {
				var video_cont = $('.field--name-field-paragraphs'),
					owl_item = video_cont.find('.owl-item');
					owl_item_actv = video_cont.find('.owl-item.active');
					if (owl_item_actv.find('.video-cont').length) {owl_item.find('video').get(0).play();} 
					owl_item.each(function(idx, el) {
					var el = $(el);
					if (el.hasClass('active')) {
						if (el.find('.video-cont').length) {el.find('video').get(0).play();} 
						el.find('.message').addClass('start').removeClass('end');
					} else {
						if (el.find('.message').hasClass('start')) {
							el.find('.message').removeClass('start').addClass('end');
						}
					}
				});
			}
        };
    
    $('#video-block').find('.node--type-front-page-video').each(function(idx, el) {
        el = $(el);
		el.find('.field--name-field-paragraphs').owlCarousel(big_carousel_options);
        
    });
})(jQuery);