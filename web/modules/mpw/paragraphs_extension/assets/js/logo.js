(function($, undefined) {
    var big_carousel_options = {
            items: 6,
			//loop: true,//was not working on DEV
            nav: true,
			//center:true,
            navText: false,
            dots: false,
            lazyLoad: true,
            onInitialized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
            },
            onResized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
               if(first.hasClass('active')) arr_prev.addClass('disabled');
                else arr_prev.removeClass('disabled');
                
                if(last.hasClass('active')) arr_next.addClass('disabled');
                else arr_next.removeClass('disabled');
            },
            onTranslated: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev').removeClass('disabled'),
                    arr_next = carousel.find('.owl-next').removeClass('disabled');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
            }
        },
        items_to_responsive_mapping = {
			4: {
                0: {items: 1},
                620: {items: 2, margin: 10},
                768: {items: 3, margin: 10},
                1200: {items: 4, margin: 10}
            },
			5: {
                0: {items: 1},
                480: {items: 2, margin: 10},
                620: {items: 3, margin: 10},
                768: {items: 4, margin: 10},
                1200: {items: 5, margin: 10}
            },
            6: {
                0: {items: 1},
                480: {items: 2, margin: 10},
                620: {items: 3, margin: 10},
                768: {items: 4, margin: 10},
                960: {items: 5, margin: 10},
                1200: {items: 6, margin: 10},
            }
        };
    
    $('#content').find('.paragraph.logo').each(function(idx, el) {
        el = $(el);
        var autoplay = el.attr('data-autoplay'),
            items = el.attr('data-grid_items');
        if(autoplay) $.extend(big_carousel_options, { autoplay: true, autoplayTimeout: autoplay, autoplayHoverPause: true });
        if(items) $.extend(big_carousel_options, { items: items, responsive: items_to_responsive_mapping[items] });
        el.find('.field--name-field-infographic').owlCarousel(big_carousel_options);
    });
})(jQuery);