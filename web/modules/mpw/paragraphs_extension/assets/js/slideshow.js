(function($, undefined) {
    var big_carousel_options = {
            //animateOut: 'fadeOut',
            items: 1,
			loop: true,//was not working on DEV
            nav: true,
			center:true,
            navText: false,
            dots: false,
            lazyLoad: true,
            onInitialized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
                //if(first.hasClass('active')) arr_prev.addClass('disabled');
                //if(last.hasClass('active')) arr_next.addClass('disabled');
            },
            onResized: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
               // if(first.hasClass('active')) arr_prev.addClass('disabled');
                //else arr_prev.removeClass('disabled');
                
                //if(last.hasClass('active')) arr_next.addClass('disabled');
                //else arr_next.removeClass('disabled');
            },
            onTranslated: function() {
                var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev').removeClass('disabled'),
                    arr_next = carousel.find('.owl-next').removeClass('disabled');
                
                //if(first.hasClass('active')) arr_prev.addClass('disabled');
                //if(last.hasClass('active')) arr_next.addClass('disabled');
            }
        },
        items_to_responsive_mapping = {
            1: {
                0: {items: 1}
            },
            2: {
                0: {items: 1},
                620: {items: 2, margin: 20}
            },
            3: {
                0: {items: 1},
                620: {items: 2, margin: 20},
                768: {items: 3, margin: 20}
            },
            4: {
                0: {items: 1},
                620: {items: 2, margin: 20},
                768: {items: 3, margin: 20},
                1200: {items: 4, margin: 20},
                1500: {items: 5, margin: 20},
                1700: {items: 6, margin: 20},
                2000: {items: 7, margin: 20},
                2300: {items: 8, margin: 20}
            }
        };
    
    $('#content').find('.paragraph.slideshow').each(function(idx, el) {
        el = $(el);
        var autoplay = el.attr('data-autoplay'),
            items;
            if (el.hasClass('slideshow-mobile-background') || el.hasClass('slideshow-desktop-background')) {
                items = 1
            } else {
                items = el.attr('data-grid_items');
            }
        if(autoplay) $.extend(big_carousel_options, { autoplay: true, autoplayTimeout: autoplay, autoplayHoverPause: true });
        if(items) $.extend(big_carousel_options, { items: items, responsive: items_to_responsive_mapping[items] });
        el.children('.holder').children('.field--name-field-slides').owlCarousel(big_carousel_options);
    });
})(jQuery);