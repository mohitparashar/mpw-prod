(function($, undefined) {
    // mobile table fix
    // adds "fixed-first-column" class to tables on screens with width lower than 769px
    if($(window).width() < 769) {
        var tables = $('#content').find('.field--name-field-paragraphs').find('table'),
            hidden_tabs = [];
        
        tables.each(function(idx, el) {
            el = $(el);
            var header = el.children('thead');
            if(header.length) {
                el.addClass('fixed-first-column');
                
                var first_row = header.children('tr').eq(0),
                    first_row_height = first_row.height(),
                    first_row_first_cell = first_row.children('th').eq(0);
                
                if(first_row_height) first_row_first_cell.innerHeight(first_row_height);
                else {
                    // If table's row height is equal to 0, we can't set the proper height for the first cell.
                    // This case is actual when the table or one of it's parent containers is hidden.
                    // So we check to see if current table is situated in a "tab" paragraph type, and if so,
                    // we mark that tab with class "tables-not-fixed" and add in collection to fix it's tables later.
                    var parent_tab = el.closest('.tab-body');
                    if(!parent_tab.hasClass('tables-not-fixed')) {
                        hidden_tabs.push({
                            tab: parent_tab.addClass('tables-not-fixed'),
                            tables: [el]
                        });
                    }
                    else hidden_tabs[hidden_tabs.length-1].tables.push(el);
                }
            }
            else {
                var first_row = el.children('tbody').children('tr').eq(0),
                    first_row_first_th = first_row.children('th').eq(0);
                
                if(first_row_first_th.length) {
                    el.addClass('fixed-first-column');
                    /* // this code is not actual anymore, because we fixed the height of cell and set "white-space: nowrap" on cells
                    first_row_first_th.innerHeight(first_row.height() + 1);*/
                }
            }
        });
        
        // We loop through hidden tabs array and add a click event custom listener to every tab's selection button to fix
        // table's header's first row's first cell's height immediately when tab is being active(shown) for the first time.
        hidden_tabs.forEach(function(el, idx, arr) {
            function fixTabTables(e) {
                // loops through all tables in the tab and tries to fix the height of the first cell
                e.data.tables.forEach(function(el, idx, arr) {
                    var header = el.children('thead'),
                        first_row = header.children('tr').eq(0),
                        first_row_height = first_row.height(),
                        first_row_first_cell = first_row.children('th').eq(0);

                    if(first_row_height) first_row_first_cell.innerHeight(first_row_height);
                    else {
                        // if row height is 0, we should check it later with setInterval function
                        var interval_fix_cell_height = setInterval(function() {
                            var first_row_height = first_row.height();
                            if(first_row_height) {
                                first_row_first_cell.innerHeight(first_row_height);
                                // remove interval after successful height fix operation
                                clearInterval(interval_fix_cell_height);
                            }
                        }, 200);
                    }
                });
                
                // Here we unbind "fixTabTables" handler from tab button click event listeners list
                // We just need to provide the custom namespace's name
                $(e.data.tab_btn).off('.tables_fix');
            }
            
            // we add event listener only for hidden tabs
            if(!el.tab.hasClass('actv')) {
                // find corresponding tab button
                var tab_btn = el.tab.parent().siblings('.tabs-header').children('.tab-title[data-id="' + el.tab.attr('data-id') + '"]');
                // We attach click event custom handler within a custom namespace
                // called "tables_fix" to be able then remove it at the right time
                tab_btn.on('click.tables_fix', { tab_btn: tab_btn, tab: el.tab, tables: el.tables }, fixTabTables);
            }
        });
    }
})(jQuery);