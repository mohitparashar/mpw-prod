(function($, undefined) {
	var scrollTop = $(window).scrollTop();
	var text_lines = $('.field--name-field-paragraphs').find('.owl-item.active').find('.field--name-field-slogan');
    var big_carousel_options = {
			//animateOut: 'fadeOut',
            items: 1,
			//loop: true,//was not working with fade
            nav: true,
            navText: false,
            dots: true,
			//autoplay:true,
			//autoplayTimeout:5000,
            lazyLoad: true,
			onInitialized: function() {
				var carousel = this.$element,
                    first = this._items[0],
					last = this._items[this._items.length-1],
					arr_prev = carousel.find('.owl-prev'),
					arr_next = carousel.find('.owl-next');
				if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
				$('.field--name-field-paragraphs .owl-dot.active').prevAll('.owl-dot').addClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').addClass('past');
                text_lines.each(function(idx, elem) {
					elem = $(elem);
					var text = elem.text(),
						numbers = text.match(/\d+/g),// check if text line has numbers in it
						i = 0,
						regex,
						options;
					
					if(numbers) {
						for(i; i < numbers.length; i++) {
							regex = new RegExp(numbers[i], 'g');
							options = generateCounterOptions(numbers[i]);
							text = text.replace(regex, '<span class="number-counter" data-number="' + numbers[i] + '" data-interval="' + options.interval + '" data-step="' + options.step + '">0</span>');
						}
						elem.html(text).addClass('has-number');
						$(document).ready(function() {initNumberCounter(elem);});
					}
				});
				$('.field--name-field-paragraphs .owl-dot.active').prevAll('.owl-dot').addClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').nextAll('.owl-dot').removeClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').addClass('past');
            },
            onResized: function() {
				var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev'),
                    arr_next = carousel.find('.owl-next');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                else arr_prev.removeClass('disabled');
                
                if(last.hasClass('active')) arr_next.addClass('disabled');
                else arr_next.removeClass('disabled');
				/*var text_lines = $('.paragraph.workflow-infographics').find('.field--name-field-slogan');
                text_lines.each(function(idx, elem) {
					elem = $(elem);
					var text = elem.text(),
						numbers = text.match(/\d+/g),// check if text line has numbers in it
						i = 0,
						regex,
						options;
					
					if(numbers) {
						for(i; i < numbers.length; i++) {
							regex = new RegExp(numbers[i], 'g');
							options = generateCounterOptions(numbers[i]);
							text = text.replace(regex, '<span class="number-counter" data-number="' + numbers[i] + '" data-interval="' + options.interval + '" data-step="' + options.step + '">0</span>');
						}
						elem.html(text).addClass('has-number');
						$(document).ready(function() {initNumberCounter(elem);});
					}
				});*/
				$('.field--name-field-paragraphs .owl-dot.active').prevAll('.owl-dot').addClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').nextAll('.owl-dot').removeClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').addClass('past');
            },
            onTranslated: function() {
				var carousel = this.$element,
                    first = this._items[0],
                    last = this._items[this._items.length-1],
                    arr_prev = carousel.find('.owl-prev').removeClass('disabled'),
                    arr_next = carousel.find('.owl-next').removeClass('disabled');
                
                if(first.hasClass('active')) arr_prev.addClass('disabled');
                if(last.hasClass('active')) arr_next.addClass('disabled');
				var text_lines = $('.field--name-field-paragraphs').find('.owl-item.active').find('.field--name-field-slogan');
                text_lines.each(function(idx, elem) {
					elem = $(elem);
					var text = elem.text(),
						numbers = text.match(/\d+/g),// check if text line has numbers in it
						i = 0,
						regex,
						options;
					
					if(numbers) {
						for(i; i < numbers.length; i++) {
							regex = new RegExp(numbers[i], 'g');
							options = generateCounterOptions(numbers[i]);
							text = text.replace(regex, '<span class="number-counter" data-number="' + numbers[i] + '" data-interval="' + options.interval + '" data-step="' + options.step + '">0</span>');
						}
						elem.html(text).addClass('has-number');
						$(document).ready(function() {initNumberCounter(elem);});
					}
				});
				$('.field--name-field-paragraphs .owl-dot.active').prevAll('.owl-dot').addClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').nextAll('.owl-dot').removeClass('past');
				$('.field--name-field-paragraphs .owl-dot.active').addClass('past');
            }
        };
    
    $('#content').find('.field--name-field-paragraphs').each(function(idx, el) {
        el = $(el);
		el.owlCarousel(big_carousel_options);
        
    });
	/*$('#content').find('.paragraph.big-banner').children('.down-arrow').click(function(el){
		el = $(this).closest('.field__item').next();
		$('html, body').animate({
			scrollTop: el.offset().top-80}, 800);
	})*/
	var i = 1;
	$('.field--name-field-paragraphs .owl-dot').each(function(){
	  $(this).text(i);
	  i++;
	});
	$('#content').find('.paragraph.workflow').children('.down-arr').click(function(el){
		el = $('#content').find('.field--name-field-paragrap');
		var header = $("#header").height();
		$('html, body').animate({
			scrollTop: el.offset().top-header}, 800);
	})
	    // attach number counter logic to selected line
    function initNumberCounter(line) {
        var counters = line.children('.number-counter');
        counters.each(function(idx, elem) {
            elem = $(elem);
            
            var token = (new Date()).getTime(),
                limit = elem.attr('data-number'),
                interval = parseInt(elem.attr('data-interval')),
                step = parseInt(elem.attr('data-step')),
                tmp;
            
            this['number_counter_' + token] = setInterval(function() {
                if((tmp = parseInt(elem.text())) < limit) elem.text(tmp + step);
                else {
                    clearInterval(this['number_counter_' + token]);
                    elem.text(limit);
                }
            }, interval);
        });
    }
    
    // generate interval and step for a number
    function generateCounterOptions(number) {
        options = {};
        
        // these settings are temporary solution
        if(number < 100) {
            options.interval = 23;
            options.step = 1;
        }
        else if(number < 500) {
            options.interval = 20;
            options.step = 3;
        }
        else if(number < 1000) {
            options.interval = 20;
            options.step = 7;
        }
        else if(number < 5000) {
            options.interval = 20;
            options.step = 50;
        }
        else if(number < 10000) {
            options.interval = 20;
            options.step = 100;
        }
        else {
            options.interval = 1;
            options.step = 1000;
        }
        
        return options;
    }
    
    // identify lines with numbers and change theirs html structure
    /*text_lines.each(function(idx, elem) {
        elem = $(elem);
        var text = elem.text(),
            numbers = text.match(/\d+/g),// check if text line has numbers in it
            i = 0,
            regex,
            options;
        
        if(numbers) {
            for(i; i < numbers.length; i++) {
                regex = new RegExp(numbers[i], 'g');
                options = generateCounterOptions(numbers[i]);
                text = text.replace(regex, '<span class="number-counter" data-number="' + numbers[i] + '" data-interval="' + options.interval + '" data-step="' + options.step + '">0</span>');
            }
            elem.html(text).addClass('has-number');
            $(document).ready(function() {initNumberCounter(elem);});
        }
    });*/
	
})(jQuery);