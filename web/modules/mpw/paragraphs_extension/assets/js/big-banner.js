(function($, undefined) {
	var scrollTop = $(window).scrollTop();
    var big_carousel_options = {
			animateOut: 'fadeOut',
            items: 1,
			loop: true,//was not working with fade
            nav: true,
            navText: false,
            dots: true,
			autoplay:true,
			autoplayTimeout:5000,
            lazyLoad: true
        };
    
    $('#content').find('.paragraph.big-banner').each(function(idx, el) {
        el = $(el);
		if (!$('.paragraph.big-banner').hasClass('items-1')) {el.children('.field--name-field-slides').owlCarousel(big_carousel_options);};
        
    });
})(jQuery);