(function($, Drupal, undefined) {
    if(isMobile.phone) document.body.classList.add('device-mobile', 'device-phone');
    else if(isMobile.tablet) document.body.classList.add('device-mobile', 'device-tablet');
    else document.body.classList.add('device-desktop');
	$(".paragraph.files").find('.file').children('a').attr('target','_blank');
})(jQuery, Drupal);