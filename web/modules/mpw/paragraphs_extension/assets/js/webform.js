(function($, undefined) {
    var formItem = $(".paragraph.webform").find(".form-item");
    formItem.children("input").each(function() {
        if($(this).val()) $(this).closest(".form-item").addClass("clicked");
    })
    formItem.find("textarea").each(function() {
        if($(this).val()) $(this).closest(".form-item").addClass("clicked");
    })
    formItem.find("select").each(function() {
        if($(this).val()) $(this).closest(".form-item").addClass("clicked");
    })
	formItem.children("input").on('focus', function(e) {
		
        $(this).closest(".form-item").addClass("clicked");
                $(this).closest(".form-item").siblings(".form-item").each(function(idx, el){
                	//debugger;
             el = $(el);
            if (el.children("input").val() == "") el.removeClass("clicked");
            if (el.find("textarea").val() == "") el.removeClass("clicked");
        });
    });
   formItem.find("textarea").on('focus', function(e) {
		
        $(this).closest(".form-item").addClass("clicked");
                $(this).closest(".form-item").siblings(".form-item").each(function(idx, el){
                	//debugger;
             el = $(el);
            if (el.children("input").val() == "") el.removeClass("clicked");
            if (el.find("textarea").val() == "") el.removeClass("clicked");
        });
    });
   formItem.find("select").on('focus', function(e) {
		
        $(this).closest(".form-item").addClass("clicked");
                $(this).closest(".form-item").siblings(".form-item").each(function(idx, el){
                	//debugger;
             el = $(el);
            if (el.children("input").val() == "") el.removeClass("clicked");
            if (el.find("textarea").val() == "") el.removeClass("clicked");
        });
    });
    $("body").on('click', function(e) {
         if (!$(e.target).closest('.paragraph.webform .form-item').length) {
             formItem.each(function(idx, el){
                            //debugger;
                     el = $(el);
                    if (el.children("input").val() == "") el.removeClass("clicked");
                    if (el.find("textarea").val() == "") el.removeClass("clicked");
                });
          }
           
    });

})(jQuery);