<?php

namespace Drupal\mpw_rest_api\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "mpw_serializer",
 *   title = @Translation("Mpw serializer"),
 *   help = @Translation("Serializes views row data and pager using the Serializer component."),
 *   display_types = {"data"}
 * )
 */
class SerializerMpw extends Serializer {

  /**
   * {@inheritdoc}
   */
 public function render() {
   $rows = array();
   $count = $this->view->pager->getTotalItems();
   
   foreach ($this->view->result as $row_index => $row) {
     $this->view->row_index = $row_index;
     $rows[] = $this->view->rowPlugin->render($row);
   }
   unset($this->view->row_index);

   // Get the content type configured in the display or fallback to the// default.
   if ((empty($this->view->live_preview))) {
     $content_type = $this->displayHandler->getContentType();
   }
   else {
     $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
   }
   return $this->serializer->serialize(['count' => (int) $count, 'results' => $rows], $content_type);
 }

}

