<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to post User update data.
 *
 * @RestResource(
 *   id = "user_profile_resource",
 *   label = @Translation("User Profile"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/profile",
 *   },
 * )
 */
class UpdateUserResource extends ResourceBase {


     /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('my_custom_log'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database')
    );
  }

    /**
     * Responds to PATCH requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function get(Request $request) {
        
        $uid = $this->currentUser->id();
        $user = \Drupal\user\Entity\User::load($uid);
        $output = [];
        if ($user) {
        	$user_image = '';
            $fid = $user->user_picture->target_id;
            if ($fid) {
	            $file_storage = \Drupal::entityTypeManager()->getStorage('file');
	            $file = $file_storage->load($fid);
	            $user_image = file_url_transform_relative(file_create_url($file->getFileUri()));
    		}
            $output = [
                'profile_name' => $user->field_profile_name->value,
                'email' => $user->mail->value,
                'user_picture' => $user_image,
                'created' => $user->created->value,
                'last_login' => $user->login->value,
                'user_timezone' => $user->timezone->value
            ];
        }
        $cache = [
            "max-age" => 0
        ];

        $response = new ResourceResponse($output);
        $response->addCacheableDependency($cache);

        return  $response;
    }
	/**
     * Responds to PATCH requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function patch(Request $request) {
    	$data = json_decode($request->getContent(), TRUE);
        // $uid = $this->currentUser->id();
        //print_r(\Drupal::currentUser()->id());
    	//var_dump($this->currentUser);
        //print_r($data);
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        // later decode and save image

        if ($data['avatar'] != "") {
            $avatar = str_replace('data:image/png;base64,', '', $data['avatar']);
    
            $image = base64_decode($avatar);    
            $finfo = finfo_open();
            $mime_type = finfo_buffer($finfo, $image, FILEINFO_MIME_TYPE);
            finfo_close($finfo);
            // get file type png. jpg
            $ext = $mime_type ? str_replace('image/', '', $mime_type) : 'png';
            // unique
            $file_name = time();
            // Save image file
            $file = file_save_data($image, 'public://pictures/'.$file_name.'.'.$ext, FileSystemInterface::EXISTS_REPLACE);
    
            // $file = file_save_data($data, $target, FileSystemInterface::EXISTS_REPLACE);
            $file->save();
        }

        if ($this->hasValue($data['profile_name'])) {            
            $user->set('field_profile_name', $data['profile_name']);
        }

        if ($this->hasValue($data['timezone'])) {
            $user->set('timezone', $data['timezone']);
        }

        if ($this->hasValue($data['password'])) {
            $user->setPassword($data['password']);
        }

        if ($file){
            // And set the user_picture
            $user->set('user_picture', $file);
        }
        $user->save();

    	exit;
    	// $user = User::load($userId);
    	// if ($this->hasValue($data['name'])) {
        //        $dirty = true;
        //        $user->set('field_name', $data['name']);
        //    }

        //    if ($user->save()) {
        //            if (isset($file_url)) {
        //                $response = ["url" => $file_url];
        //            } else {
        //                $response = ["status" => "OK"];
        //            }
        //            static::invalidateProfileCache($user->id());
        //        } else {
        //            throw new \UnexpectedValueException(ResponseMessages::INVALID_REQUEST);
        //        }
    }


    protected function hasValue($field) {
        return isset($field) && $field !== "";
    }

    protected function hasKey($field) {
        return isset($field);
    }
}