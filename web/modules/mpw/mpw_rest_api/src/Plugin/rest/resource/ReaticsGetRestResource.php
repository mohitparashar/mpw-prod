<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Drupal\node\Entity\Node;

use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Cache\CacheableResponse;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "reatics_get_rest_resource",
 *   label = @Translation("Reatics get rest resource"),
 *   uri_paths = {
 *     "canonical" = "/reatics_rest"
 *   }
 * )
 */
class ReaticsGetRestResource extends ResourceBase {
    
    
    /**
     *  A curent user instance.
     *
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    protected $currentUser;
    
    /**
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $currentRequest;
    
    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Drupal\Core\Session\AccountProxyInterface $current_user
     *   The current user instance.
     * @param Symfony\Component\HttpFoundation\Request $current_request
     *   The current request
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user, Request $current_request) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->currentUser = $current_user;
        $this->currentRequest = $current_request;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('mpw_rest_api'),
            $container->get('current_user'),
            $container->get('request_stack')->getCurrentRequest()
        );
    }
    
    /**
     * Responds to GET requests.
     *
     * Returns details of a course node.
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function get() {

        $keyword = $this->currentRequest->query->get('keyword') ? $this->currentRequest->query->get('keyword'): NULL;
        
        // $city = $this->currentRequest->query->get('city') ? $this->currentRequest->query->get('city'): NULL;
        // $address = $this->currentRequest->query->get('address') ? $this->currentRequest->query->get('address'): NULL;
        // $county = $this->currentRequest->query->get('county') ? $this->currentRequest->query->get('county'): NULL;
        // $zipcode = $this->currentRequest->query->get('zipcode') ? $this->currentRequest->query->get('zipcode'): NULL;

        // $result = get_result_by_drupal_entity($zipcode, $state);
        // $result = get_result_by_solr($keyword, $zipcode, $state, $city, $county, $address);

        $result = get_result_by_solr($keyword);
        $response = new ResourceResponse($result);
        $response->addCacheableDependency($result);
        return $response;
    }

}

function get_result_by_drupal_entity($target_zipcode, $target_state) {
    
    $nids = \Drupal::entityQuery('node')->condition('type','property')->execute();
    $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
    
    $result = array();
    foreach($nodes as $node){
        //get address field
        $addr = $node->get('field_address');
        //skip non-query items
        $current_zipcode = $addr->postal_code;
        if( isset($target_zipcode) && $current_zipcode != $target_zipcode) continue;
        
        $current_state = $addr->administrative_area;
        if( isset($target_state) && $current_state != $target_state) continue;
        //save result
        $tmp = array(
            'title' => $node->title->value,
            //'name_line' => $addr->name_line,
            //'first_name' => $addr->first_name,
            //'last_name' => $addr->last_name,
            //'organisation_name' => $addr->organisation_name,
            'state' => $addr->administrative_area,
            //'locality' => $addr->locality,
            'zipcode' => $addr->postal_code,
            //'thoroughfare' => $addr->thoroughfare,
            //'premise' => $addr->premise,
        );
        
        $result[] = $tmp;
    }
    
    return $result;
}

/**
 * Helper function to get result from solr.
 */
function get_result_by_solr($keyword){
    //Flow adjust from:
    // https://www.drupal.org/docs/8/modules/search-api/developer-documentation/executing-a-search-in-code

    //set search ID for solr server
    $index = \Drupal\search_api\Entity\Index::load('autocomplete_search');
    $query = $index->query();
    $query->range(0, 9999999);  //unlimit?

    // Change the parse mode for the search.
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')
      ->createInstance('terms');
    // $parse_mode->setConjunction('OR');
    $query->setParseMode($parse_mode);
    
    // Set fulltext search keywords and fields.
    if(isset($keyword)) {
        // $words = str_word_count($keyword, 1);
        // $keys = array_merge($words, ['#conjunction' => 'AND']);
        $query->keys($keyword);
        // $query->setFulltextFields(['title', 'postal_code', 'administrative_area', 'locality', 'field_property_type', 'field_listing_type']);
        $query->setFulltextFields(['title', 'postal_code', 'locality', 'dependent_locality', 'address_line1']);
    }

    // Set additional conditions.
    // $query->addCondition('status', 1);

    // Add more complex conditions.
    $conditions = $query->createConditionGroup('AND');
    
    // if( isset($target_zipcode) )     $conditions->addCondition('postal_code', "$target_zipcode", '=');
    // if( isset($target_state) )       $conditions->addCondition('administrative_area', "$target_state", '=');
    // if( isset($target_city) )       $conditions->addCondition('locality', "$target_city", '=');
    // if( isset($county) )       $conditions->addCondition('dependent_locality', "$county", '=');
    // if( isset($address) )       $conditions->addCondition('address_line1', "$address", '=');
    
    $query->addConditionGroup($conditions);
    // Sort result by relevance.
    $query->sort('search_api_relevance', 'DESC');
    // Execute the search.
    $results = $query->execute();
    $return_arr = array();

    foreach ($results->getResultItems() as $item) {
        $tmp = array();
        $tmp['title'] = end($item->getField('title')->getValues())->toText();
        $tmp['city'] = end($item->getField('locality')->getValues())->toText();
        $tmp['zipcode'] = end($item->getField('postal_code')->getValues())->toText();
        $tmp['state'] = end($item->getField('administrative_area')->getValues())->toText();
        $tmp['county'] = end($item->getField('dependent_locality')->getValues())->toText();
        $tmp['address'] = end($item->getField('address_line1')->getValues())->toText();

        if (isset($keyword)) {
            if (!stristr(serialize($tmp), $keyword)) {
                continue;
            }
        }

        $return_arr[] = $tmp;
    }


    $result = array_values(_process_record($return_arr));

    return $result;
}

/**
 * Helper function to remove empty array from multidimensional array.
 */
function _array_unique_multidimensional($input) {
    $serialized = array_map('serialize', $input);
    $unique = array_unique($serialized);
    return array_intersect_key($input, $unique);
}

/**
 * Helper function to process record.
 */
function _process_record($return_arr) {
    $main_array = [];
    $field = ['city', 'address', 'county', 'zipcode'];
    foreach ($field as $field_value) { 
        foreach ($return_arr as $key => $value) {
            if ($value[$field_value] != '') {
                $tmp['title'] = $value[$field_value] . ' - In ' . ucfirst($field_value);
                $tmp['code'] = $value[$field_value];
                $main_array[] = $tmp;
            }
            
        }
    }
    
    return  _array_unique_multidimensional($main_array);
}