<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mpw_subscription\PayFlowRC;
/**
 * Provides a resource to post User update data.
 *
 * @RestResource(
 *   id = "subscription_add_resource",
 *   label = @Translation("Subscription Add Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/add-subscription",
 *       "https://www.drupal.org/link-relations/create" = "/api/v1/add-subscription"
 *   },
 * )
 */
class SubscriptionAddResource extends ResourceBase {
   /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;
    // Error object.
    private $error;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

	/**
	* {@inheritdoc}
	*/
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
	  return new static(
		$configuration,
		$plugin_id,
		$plugin_definition,
		$container->getParameter('serializer.formats'),
		$container->get('logger.factory')->get('mpw_rest_api'),
		$container->get('current_user'),
		$container->get('request_stack')->getCurrentRequest(),
		$container->get('database')
	  );
	}


	/**
     * Responds to POST requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function post(Request $request) {

      if (!$this->currentUser->hasPermission('access content')) {
        throw new AccessDeniedHttpException();
      }

      $uid = $this->currentUser->id();
      $data = json_decode($request->getContent());
      $response = $this->addSubscription($data);

      $response = new ResourceResponse($response, 200);
      return $response;
    }

	private function addSubscription($data) {
		$user_data = $this->getUserProfile($data->uid);
		$card_response = $this->chargeCreditCard($user_data, $data);
		if ($card_response['RESULT'] == '1') {
  	  		$response['status'] = 'error';
  			  $response['message'] = $card_response['RESPMSG'];
  	  	}
        elseif ($card_response['RESULT'] == '0') {
          // [RESULT] => 0
          // [RPREF] => R8350D1DCAE5
          // [PROFILEID] => RT0000000002
          // [RESPMSG] => Approved
          $plan_id = $this->_getProductId($user_data['plan']);
          // Create subscription plan.
          $this->recordSubscription($user_data, $data, $card_response);
          // Return success.
          $response['status'] = 'success';
          $response['message'] = 'Property has been subscribe successfuly!';

        }
  	  	else {
          $response['status'] = 'error';
          $response['message'] = end(explode(":", $card_response['RESPMSG']));
  	  	}

		return $response;
	}

	private function getUserProfile($uid) {
	  $query =$this->database->select('mpw_rc_subscription', 'mpw_rc')
	    ->fields('mpw_rc', ['payer_frist_name', 'payer_last_name', 'payer_email', 'payer_address', 'payer_city','payer_state', 'payer_postal_code'])
	    ->condition('uid', $uid);
	  $results = $query->execute();
	  while ($content = $results->fetchAssoc()) {
	    // Operations using $content.
	    $user = $content;
	    break;
	  }
	  return $user;
	}




    private function chargeCreditCard(array $user_data, $data) {

		$amount = $this->_getPrice($data->plan);
	  	// Recurring Transaction
	    $PayFlow = new PayFlowRC('MPWINFO', 'PayPal', 'MohitMPW', 'MohitMPW123', 'recurring');
		  // test or live
	    $PayFlow->setEnvironment('test');
	    // S = Sale transaction, R = Recurring
	    $PayFlow->setTransactionType('R');
	    // A = Automated clearinghouse, C = Credit card.
	    $PayFlow->setPaymentMethod('C');
	     // 'USD', 'EUR', 'GBP', 'CAD', 'JPY', 'AUD'.
	    $PayFlow->setPaymentCurrency('USD');

	    // Only used for recurring transactions
	    $PayFlow->setProfileAction('A');
	    $PayFlow->setProfileName($user_data['payer_frist_name'] + '_' + $user_data['payer_last_name']);
	    $PayFlow->setProfileStartDate(date('mdY', strtotime("+60 day")));
	    $PayFlow->setProfilePayPeriod('MONT');
	    $PayFlow->setProfileTerm(0);

	    $PayFlow->setAmount($amount, FALSE);
	    $PayFlow->setCCNumber(trim($data->creditCard));
	    $PayFlow->setCVV($data->cvc);
	    $PayFlow->setExpiration(substr(str_replace("/", "", trim($data->expirationDate))), 0, 4);
	    $PayFlow->setCreditCardName($user_data['payer_frist_name'] + ' ' + $user_data['payer_last_name']);

	    $PayFlow->setCustomerFirstName($user_data['payer_frist_name']);
	    $PayFlow->setCustomerLastName($user_data['payer_last_name']);
	    $PayFlow->setCustomerAddress($user_data['payer_address']);
	    $PayFlow->setCustomerCity($user_data['payer_city']);
	    $PayFlow->setCustomerState($user_data['payer_state']);
	    $PayFlow->setCustomerZip($user_data['payer_postal_code']);
	    $PayFlow->setCustomerCountry('US');
	    $PayFlow->setCustomerEmail($user_data['payer_email']);
	    $PayFlow->setPaymentComment('New Monthly Transaction');

	    if($PayFlow->processTransaction()){
	    	$this->process_payflow = TRUE;
	    }
	    else {
	      $this->process_payflow = FALSE;
	    }

	    return $PayFlow->getResponse();
  	}

	/**
	 * Helper function to get prie
	 */
	private function _getPrice($plan) {
	  	$config = \Drupal::config('mpw_subscription.settings');
	  	$basic_listing = $config->get('basic_listing');
	  	$standard_listing = $config->get('standard_listing');
	  	$elite_listing = $config->get('elite_listing');
	  	switch ($plan) {
	  	  case 'bl':
	  		  $price = $config->get('basic_listing');
	  		break;
	  	  case 'cl':
	  		  $price = $config->get('standard_listing');
	  		break;
	  	  case 'el':
	  		  $price = $config->get('elite_listing');
	  		break;

	  	  default:
	  		  $price = $config->get('basic_listing');
	  		break;
	  	}

	  	return $price;
	}


   /**
    * Helper function to get product plan.
    */
  	private function _getProductId($plan) {
	  switch ($plan) {
	    case 'bl':
	      $plan = 1;
	      break;
	    case 'cl':
	      $plan = 2;
	      break;
	    case 'el':
	      $plan = 3;
	      break;
	    default:
	      $plan = 1;
	      break;
	    }

	    return $plan;
	}

	/**
   * Record subscription.
   */
  private function recordSubscription($user_data, $data, $server_response) {

    try {
        //$subscription_dates = $this->getSubscriptionDate($uid);
         $id = $this->database->insert('mpw_rc_subscription')
          ->fields([
            'product_id',
            'pay_id',
            'subscription_state',
            'payment_amount',
            'start_date',
            'uid',
            'nid',
            'payer_id',
            'pay_status',
            'payer_frist_name',
            'payer_last_name',
            'payer_email',
            'payer_address',
            'payer_city',
            'payer_state',
            'payer_postal_code',
            'next_billing_date',
            'last_payment_date',
            'final_payment_date',
            'subscription_start',
            'subscription_end',
            'pay_type'
          ])
          ->values([
            'product_id' => $this->_getProductId($data->plan),
            'pay_id' => $server_response['RPREF'],
            'subscription_state' => $server_response['RESPMSG'],
            'payment_amount' => $this->_getPrice($data->plan),
            'start_date' => date("Y-m-d H:i:s", time()),
            'uid' => $data->uid,
            'nid' => $data->nid,
            'payer_id' => $server_response['PROFILEID'],
            'pay_status'=> $server_response['RESPMSG'],
            'payer_frist_name' => $user_data['payer_frist_name'],
            'payer_last_name' => $user_data['payer_last_name'],
            'payer_email' => $user_data['payer_email'],
            'payer_address' => $user_data['payer_address'],
            'payer_city' => $user_data['payer_city'],
            'payer_state' => $user_data['payer_state'],
            'next_billing_date' => date("Y-m-d H:i:s", strtotime("+60 days")),
            'last_payment_date' => date("Y-m-d H:i:s", strtotime("+90 days")),
            'final_payment_date' => date("Y-m-d H:i:s", strtotime("+425 days")),
            'payer_postal_code' => $user_data['payer_postal_code'],
            'subscription_start' => Drupal::time()->getRequestTime(),
            'subscription_end' => strtotime("+365 days"),
            'pay_type' => 'payflow',
          ])
        ->execute();
        return $id;
      }
      catch (Exception $e) {
      // Failed to insert payment from PayPal
      return FALSE;
    }
    return TRUE;
  }
}
