<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use \Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\rest\ResourceResponse;
use Drupal\image\Entity\ImageStyle;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\File\FileSystem;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Property Detail Resource
 *
 * @RestResource(
 *   id = "property_detail_resource",
 *   label = @Translation("Property Detail Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/property-detail/{id}"
 *   }
 * )
 */
class PropertyDetailResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request, $id = 0) {

    $allowed_base64 = false;
    // Check condition
  	if ($id) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($id);
      $allowed_base64 = true;
    }
    else {
      $header_alias = $request->headers->get('Alias');
      $alias = \Drupal::service('path_alias.manager')->getPathByAlias($header_alias);
      $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
      $entity_type = key($params);
      if($entity_type) {
        $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
      }
      else {
        $response = new ResourceResponse(['error' => 'Missing required parameters']);
        return $response;
      }
    }
    //echo $alias;exit;
    $response = \Drupal::service('renderer')->executeInRenderContext(new RenderContext(), function () use ($node, $allowed_base64) {

      // Lease type
      $lease_type = $node->get('field_listing_type')->value;

      // Property type
      $property_type = $node->get('field_property_type')->entity->getName();
      $property_id = $node->get('field_property_type')->entity->id();

      // Banner images
      $fid = $node->get('field_banner_image')->target_id;
      $file_storage = \Drupal::entityTypeManager()->getStorage('file');
      $file = $file_storage->load($fid);
      $banner_uri = $file->getFileUri();
      $banner_image = file_url_transform_relative(file_create_url($banner_uri));
      $banner_image_type = $file->getMimeType();
      // Create image derivatives if they not already exists.
      $filesystem = \Drupal::service('stream_wrapper_manager')->getViaUri($banner_uri);
      $absolute_path = $filesystem->realpath();
      // Check if base64 is allowed
      if ($allowed_base64) {
        // Encode image in base64 format.
        $image_file = file_get_contents($absolute_path);
        $base_64_image = base64_encode($image_file);
        $banner_image_base_64 = "data:$banner_image_type;base64,$base_64_image";
      }

      $show_broler_information = $node->get('field_show_broker_information')->value;

      if ($show_broler_information) {
        $broker_info = [
          'name' => $node->get('field_brokers_owners')->entity->field_contact_name->value,
          'phone' => $node->get('field_brokers_owners')->entity->field_phone_number->value,
          'email' => $node->get('field_brokers_owners')->entity->field_email->value,
          'company' => $node->get('field_brokers_owners')->entity->field_company->value,
          'license' => $node->get('field_brokers_owners')->entity->field_license->value,
          'secondary_name' => $node->get('field_brokers_owners')->entity->field_contact_name_second->value,
          'secondary_phone' => $node->get('field_brokers_owners')->entity->field_phone_number_second->value,
          'secondary_email' => $node->get('field_brokers_owners')->entity->field_email_second->value,
          'secondary_company' => $node->get('field_brokers_owners')->entity->field_company_second->value,
          'secondary_license' => $node->get('field_brokers_owners')->entity->field_secondary_license->value,
        ];
      }
      else{
        $broker_info = [
          'name' => '',
          'phone' => '1-888-518-9168',
          'email' => 'info@myperfectworkplace.com',
          'company' => 'Main Office',
          'secondary_name' => '',
          'secondary_phone' => '',
          'secondary_email' => '',
          'secondary_company' => '',
        ];
      }

      $style = ImageStyle::load('property_listing_front');
      foreach ($node->get('field_image') as $field_images) {
        $fid = $field_images->target_id;
        $file_storage = \Drupal::entityTypeManager()->getStorage('file');
        $file = $file_storage->load($fid);
        $field_image[] = $style->buildUrl($file->getFileUri());

        $filesystem = \Drupal::service('stream_wrapper_manager')->getViaUri($file->getFileUri());
        $absolute_path = $filesystem->realpath();

        $field_image_type = $file->getMimeType();
        if ($allowed_base64) {
          // Encode image in base64 format.
          $image_file = file_get_contents($absolute_path);
          $base_64_image = base64_encode($image_file);
          $field_image_base64[] = "data:$banner_image_type;base64,$base_64_image";
        }
      }

      $map = [
        'lat' => (float) $node->get('field_geolocation')->lat,
        'lng' => (float) $node->get('field_geolocation')->lng,
        'lat_sin' => (float) $node->get('field_geolocation')->lat_sin,
        'lat_cos' => (float) $node->get('field_geolocation')->lat_cos,
        'lng_rad' => (float) $node->get('field_geolocation')->lng_rad,
      ];

      $address = [
        'country_code' => $node->get('field_address')->country_code,
        'administrative_area' => $node->get('field_address')->administrative_area,
        'locality' => $node->get('field_address')->locality,
        'dependent_locality' => $node->get('field_address')->dependent_locality,
        'postal_code' => $node->get('field_address')->postal_code,
        'address_line1' => $node->get('field_address')->address_line1,
        'address_line2' => $node->get('field_address')->address_line2,
        'organization' => $node->get('field_address')->organization
      ];

      $meta_tags = unserialize($node->field_meta_tags->value);
      $avaibility = $node->get('field_avaibility')->value;
      unset($avaibility['caption']);

      return [
        'nid' => $node->id(),
        'title' => $node->get('title')->value,
        'lease_type' => $lease_type,
        'property_type' => $property_type,
        'property_id' => $property_id,
        'banner_image' => $banner_image,
        'banner_image_base64' => $allowed_base64 ? $banner_image_base_64 : '',
        'video' => $node->get('field_video')->value,
        'video_id' => $this->getIdFromInput($node->get('field_video')->value),
        'contact' => $broker_info,
        'property_images' => $field_image,
        'property_images_base64' => $allowed_base64 ? $field_image_base64 : '',
        'availability' => $avaibility,
        'property_terms' => $node->get('field_property_terms')->value,
        'address' => $address,
        'map' => $map,
        'description' => $node->get('body')->value,
        'published' => $node->isPublished(),
        'created' => $node->get('created')->value,
        'updated' => $node->get('changed')->value,
        'featured' => $node->get('field_featured')->value,
        'meta_tags' => $meta_tags,
        'previous' => $this->getNextPrevious($node->getCreatedTime(), '<', 'DESC'),
        'next' => $this->getNextPrevious($node->getCreatedTime(), '>', 'ASC'),
      ];
    });

    $response = new ResourceResponse($response);
    $cacheMeta = (
      new CacheableMetadata())->addCacheContexts(
        [
          'headers:Alias'
        ]
    );

    // $maxAge = new CacheableMetadata();
    // $maxAge->setCacheMaxAge(0);

    // $response->addCacheableDependency($disable_cache);
    // return $response;

    $response->addCacheableDependency($node);
    $response->addCacheableDependency($cacheMeta);

    return  $response;
  }


  /**
   * {@inheritdoc}
   */
  public function getIdFromInput($input) {
    preg_match('/^https?:\/\/(www\.)?((?!.*list=)youtube\.com\/watch\?.*v=|youtu\.be\/)(?<id>[0-9A-Za-z_-]*)/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }


  function getNextPrevious($date, $date_comparator, $sort_order) {
    $prev_or_next = \Drupal::entityQuery('node')
      ->condition('type', 'property')
      ->condition('status', 1)
      ->condition('created', $date, $date_comparator)
      ->sort('created', $sort_order)
      ->range(0, 1)
      ->execute();

    if(!$prev_or_next) return false;

    // Get the node itself
    $prev_or_next = Node::load(array_values($prev_or_next)[0]);

    // Return the information you need, can be w/e you want.
    return [
      'title' => $prev_or_next->getTitle(),
      'id' => $prev_or_next->id(),
      'path' => $prev_or_next->toUrl()->toString()
    ];
  }

}
