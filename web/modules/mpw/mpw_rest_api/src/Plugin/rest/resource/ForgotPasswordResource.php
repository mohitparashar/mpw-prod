<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\mpw_rest_api\Utils\StringUtils;
use Drupal\mpw_rest_api\Constants\ResponseMessages;
use Drupal\mpw_rest_api\Constants\ResponseCodes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "forgotpassword_resource",
 *   label = @Translation("Forgot Password resource"),
 *   uri_paths = {
        "canonical" = "/api/v1/forgotpassword",
 *      "https://www.drupal.org/link-relations/create" = "/api/v1/forgotpassword"
 *   }
 * )
 */

class ForgotPasswordResource extends ResourceBase {

    /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;

    /**
    * {@inheritdoc}
    */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxy $current_user, Connection $connection) {
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

    /**
    * {@inheritdoc}
    */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('current_user'),
            $container->get('database')
        );
    }
    
    public function post(Request $request) {
        $this->stringUtils = new StringUtils();
        
        $cache = [
            "max-age" => 0
        ];
        
        $payload = json_decode($request->getContent(), TRUE);
        
        $verfication_code_sent = $this->sendVerificationCode($payload['email'], $this->currentUser);
        
        if($verfication_code_sent){
            $response = new ResourceResponse(["status"=>ResponseMessages::SUCCESS], ResponseCodes::HTTP_SUCCESS_CODE);
        }else{
            $response = new ResourceResponse(ResponseMessages::ERROR_SENDING_EMAIL_2003, ResponseCodes::ERROR_SENDING_EMAIL);
        }
        
        $response->setResponseCache($cache);
        return $response;
    }
    
    private function sendVerificationCode($emailId, $user) {
        global $base_url;
            
        $user = user_load_by_mail($emailId);
        // Validating mail id
        if(empty($user)){
            throw new \UnexpectedValueException(ResponseMessages::EMAIL_NOT_REGISTERED);
        }
            
            
        $mailManager = \Drupal::service('plugin.manager.mail');
        $module = "mpw_rest_api";
        $key = 'forgot_password';
        $to = $emailId;
        $code = $this->stringUtils->generateRandomString(5);

            
        $params['message'] = '
            <!DOCTYPE html>
            <html>
            <head>
                <title>MyPerfectWrokplace</title>
                <meta content="width=device-width, initial-scale=1.0" name="viewport">    
                <style type="text/css">
                        @media only screen and (max-width: 480px){table{width:100% !important;} } 
                </style>
            </head>

            <body marginheight=0 marginwidth=0 topmargin=0 leftmargin=0 style="height: 100% !important; margin: 0; padding: 0; width: 100% !important;min-width: 100%;"> 
                    <table width="600" cellspacing="0" cellpadding="0" border="0" name="bmeMainBody" style="background-color: rgb(255, 255, 255);" bgcolor="#ffffff" align="center">
                            <tbody>
                                    <tr>
                                        <td width="100%" valign="top" align="center" style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;"> 
                                                <table cellspacing="0" cellpadding="0" border="0" name="bmeMainColumnParentTable" width="100%">
                                                        <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                    <img src="http://dev.myperfectworkplace.com/themes/mpw/logo.png">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><hr></td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" valign="top" align="center" style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;"> 
                                                <table cellspacing="0" cellpadding="0" border="0" name="bmeMainColumnParentTable" width="100%">
                                                        <tbody>
                                                                <tr>
                                                                    <td><p color="#000" style="font-size: 16px;">Dear User,</p>
                                                                        <p color="#000" style="font-size: 16px;"><strong> Please find code below'.$code.'</strong> </p>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" align="left"><p color="#000" style="font-size: 16px;">MyPerfectWrokplace Team</p></td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </td>
                                    </tr>
                            </tbody>
                        </table> 
                    </table>
                </body>
            </html>';

        $langcode = $user->getPreferredLangcode();
        $send = true;
        $this->saveEmailVerficationCodes($emailId,$code);
        
        ob_start();
        $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
        ob_end_clean();
        if ($result['result'] !== true) {
          return false;
        }
        else {
          return true;
        }
    }
    
    private function saveEmailVerficationCodes($emailId, $code){
        
        // $fields = array(
        //   'email_id' => $emailId,
        //   'code' => $code
        // );
        // $id = $this->database->insert('mpw_email_verification_codes')
        // ->fields($fields)
        // ->execute();


        $this->database->merge('mpw_email_verification_codes')
          ->key('email', $emailId)
          ->fields([
              'code' => $code
          ])
          ->execute();
       
        return true;
        
    }
    
    
}
