<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\Core\Link;
use \Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\mpw_rest_api\Utils\StringUtils;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\mpw_rest_api\Constants\ResponseCodes;
use Drupal\mpw_rest_api\Constants\ResponseMessages;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 *
 * @RestResource(
 *   id = "property_leads_resource",
 *   label = @Translation("Property Leads Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/property-leads",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/property-leads"
 *   }
 * )
 */
class LeadsResource extends ResourceBase {

  /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('my_custom_log'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param array $request
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request) {

    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $uid = $this->currentUser->id();
    $data = json_decode($request->getContent());

    $alias = \Drupal::service('path_alias.manager')->getPathByAlias($data->uri);
    $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
    $entity_type = key($params);
    $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);

    if ($node) {
      $uid = $node->getOwnerId();
      $nid = $node->id();

      $message = "Company Name : $data->company\nCompany Size : $data->companysize\nLocation of interest : $data->location\nMessage : $data->notes";

      if (!empty($data->email)) {
        $connection = \Drupal::service('database');

        $connection->insert('mpw_subscription_leads')
          ->fields([
            'payer_id',
            'nid',
            'name',
            'phone',
            'email',
            'message',
            'created',
          ])
          ->values([
            'payer_id' => $uid,
            'nid' => $nid,
            'name' => $data->name,
            'phone' => $data->phone,
            'email' => $data->email,
            'message' => $message,
            'created' => Drupal::time()->getRequestTime(),
          ])
          ->execute();
      }
      $response = new ResourceResponse(['result' => 'ok']);
      return  $response;
    }
    else {
      $response = new ResourceResponse(['result' => 'error']);
      return  $response;
    }

  }

 /**
   * Responds to GET requests.
   *
   * @param array $request
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request) {

    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

  	$uid = $this->currentUser->id();
    $alias_manager = \Drupal::service('path_alias.manager');

	  $query = \Drupal::database()->select('mpw_subscription_leads', 'u');
    $query->fields('u', ['payer_id','nid','name', 'phone', 'email', 'message', 'created']);
    $query->condition('payer_id', $uid);
    $results = $query->execute()->fetchAll();
    // Initialize an empty array
    $output = array();
    // Next, loop through the $results array
    foreach ($results as $result) {
        if ($result->payer_id != 0 && $result->payer_id != 1) {
          $node_details = Node::load($result->nid);
          $output[] = [
            'nid' => $result->nid,
            'title' => $node_details->get('title')->value,
            'node_view' => $alias_manager->getAliasByPath('/node/'. $node_details->id()),
            'name' => $result->name,
            'phone' => $result->phone,
            'email' => $result->email,
            'message' => $result->message,
            'date' => date("F j, Y", $result->created),
          ];
        }
      }

    $cache = [
      "max-age" => 0
    ];

		$response = new ResourceResponse($output);
		$response->addCacheableDependency($cache);

		return  $response;
  }

}
