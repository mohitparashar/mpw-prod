<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use \Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\rest\ResourceResponse;
use Drupal\image\Entity\ImageStyle;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Property Detail Resource
 *
 * @RestResource(
 *   id = "city_property_resource",
 *   label = @Translation("Cities Property Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/cities-property/{id}"
 *   }
 * )
 */
class CityPropertyResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request, $id = 0) {
    $response = [];
    if ($id) {
      $city_node = \Drupal::entityTypeManager()->getStorage('node')->load($id);
    }
    else {

      $alias = \Drupal::service('path_alias.manager')->getPathByAlias($request->headers->get('Alias'));
      if ($alias != '') {
        $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
        $entity_type = key($params);
        $city_node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
      }
    }

    if ($city_node) {
      // Get city field.
      $response['city']['description'] = $city_node->get('body')->value;
      $response['city']['city'] = $city_node->get('field_city')->locality;
      $response['city']['administrative_area'] = $city_node->get('field_city')->administrative_area;
      $response['city']['map'] = [
        'lat' => (float) $city_node->get('field_geolocation')->lat,
        'lng' => (float) $city_node->get('field_geolocation')->lng,
        'lat_sin' => (float) $city_node->get('field_geolocation')->lat_sin,
        'lat_cos' => (float) $city_node->get('field_geolocation')->lat_cos,
        'lng_rad' => (float) $city_node->get('field_geolocation')->lng_rad,
      ];
      $response['meta_tags'] = unserialize($city_node->field_meta_tags->value);

      // Get all related property
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'property')
        ->condition('field_referenced_city_page', $city_node->id());
      $nids = $query->execute();
      // Load all nodes.
      $nodes = Node::loadMultiple($nids);
      // Get all property.
      foreach ($nodes as $node) {

        $lease_type = $node->get('field_listing_type')->value;
        // Property type.
        $property_type = $node->get('field_property_type')->entity->getName();
        // Banner images.
        $fid = $node->get('field_banner_image')->target_id;
        $file_storage = \Drupal::entityTypeManager()->getStorage('file');
        $file = $file_storage->load($fid);
        $banner_image = file_url_transform_relative(file_create_url($file->getFileUri()));
        $banner_thumb_image = ImageStyle::load('max_325x325')->buildUrl($file->getFileUri());

        $map = [
          'lat' => (float) $node->get('field_geolocation')->lat,
          'lng' => (float) $node->get('field_geolocation')->lng,
          'lat_sin' => (float) $node->get('field_geolocation')->lat_sin,
          'lat_cos' => (float) $node->get('field_geolocation')->lat_cos,
          'lng_rad' => (float) $node->get('field_geolocation')->lng_rad,
        ];

        $address = [
          'country_code' => $node->get('field_address')->country_code,
          'administrative_area' => $node->get('field_address')->administrative_area,
          'locality' => $node->get('field_address')->locality,
          'dependent_locality' => $node->get('field_address')->dependent_locality,
          'postal_code' => $node->get('field_address')->postal_code,
          'address_line1' => $node->get('field_address')->address_line1,
          'address_line2' => $node->get('field_address')->address_line2,
          'organization' => $node->get('field_address')->organization
        ];

        $response['property'][] = [
          'title' => $node->get('title')->value,
          'lease_type' => $lease_type,
          'property_type' => $property_type,
          'banner_image' => $banner_image,
          'banner_thumb_image' => $banner_thumb_image,
          'view_node' => $node->toUrl()->toString(TRUE)->getGeneratedUrl(),
          'address' => $address,
          'map' => $map,
        ];
      }
    }

    $response = new ResourceResponse($response);
    $cacheMeta = (
      new CacheableMetadata())->addCacheContexts(
        [
          'headers:Alias'
        ]
    );

    $response->addCacheableDependency($node);
    $response->addCacheableDependency($cacheMeta);

    return  $response;
  }

}
