<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\Core\Url;
use Drupal\rest\ResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Blog Detail Resource
 *
 * @RestResource(
 *   id = "sitemap_list_resource",
 *   label = @Translation("Sitemap List Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/sitemap-list"
 *   }
 * )
 */
class SitemapResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request) {

  	$response = [];
  	$node_storage = \Drupal::entityTypeManager()->getStorage('node');
  	$alias_manager = \Drupal::service('path_alias.manager');

  	$states_nodes = $node_storage->loadByProperties(['type' => 'featuret_locations', 'status' => 1]);

  	$state_arr = [];
	foreach ($states_nodes as $states) {
  		$states_name = $states->getTitle();
  		// City
  		$city_query = \Drupal::entityQuery('node')
  			->condition('type', 'city_page')
  			->condition('status', 1)
  			->condition('field_referenced_state_page', $states->id());
		$city_nids = $city_query->execute();

		$city_nodes = $node_storage->loadMultiple($city_nids);

		$city_arr = [];
		// Property
		foreach ($city_nodes as $city) {

			$city_name = $city->getTitle() . ':' . $alias_manager->getAliasByPath('/node/'. $city->id());

			$property_query = \Drupal::entityQuery('node')
				->condition('type', 'property')
				->condition('status', 1)
				->condition('field_referenced_city_page', $city->id()
			);
			$property_nids = $property_query->execute();
			$property_nodes = $node_storage->loadMultiple($property_nids);

			$prop_arr = [];
			foreach ($property_nodes as $property) {
				// $result_property['state'][$states_name]['city'][$city_name][] = $property->getTitle();
				array_push($prop_arr, $property->getTitle() . ':' . $alias_manager->getAliasByPath('/node/'. $property->id()));
			}

			array_push($city_arr, ['cityname' => $city_name, 'property' => $prop_arr]);
		}

		array_push($state_arr, ['statename' => $states_name . ':' . $alias_manager->getAliasByPath('/node/'. $states->id()),  'cities' => $city_arr]);
  	}

	$response = new ResourceResponse($state_arr);
	return  $response;
  }
}
