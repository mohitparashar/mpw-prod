<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use \Drupal\Core\Url;
use Drupal\rest\ResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Blog Detail Resource
 *
 * @RestResource(
 *   id = "blog_detail_resource",
 *   label = @Translation("Blog Detail Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/blog-detail/{id}"
 *   }
 * )
 */
class BlogDetailResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request, $id = 0) {

  	if ($id) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($id);
    }
    else {
      $alias = \Drupal::service('path_alias.manager')->getPathByAlias($request->headers->get('Alias'));
      $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
      $entity_type = key($params);
      $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
    }

     // Banner images
    $fid = $node->get('field_image')->target_id;
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    $file = $file_storage->load($fid);
    $blog_image = file_url_transform_relative(file_create_url($file->getFileUri()));

    $meta_tags = unserialize($node->field_meta_tags->value);

    $response = [
      'title' => $node->get('title')->value,
      'blog_image' => $blog_image,
      'description' => $node->get('body')->value,
      'description' => $node->get('body')->value,
      'published' => $node->isPublished(),
      'meta_tags' => $meta_tags,
      'created' => date("jS F, Y", $node->get('created')->value),
      'updated' => $node->get('changed')->value,
    ];

    $response = new ResourceResponse($response);
    $cacheMeta = (
      new CacheableMetadata())->addCacheContexts(
        [
          'headers:Alias'
        ]
    );

    $response->addCacheableDependency($node);
    $response->addCacheableDependency($cacheMeta);

    return  $response;
  }

}
