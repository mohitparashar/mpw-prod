<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\Core\Link;
use \Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\mpw_rest_api\Utils\StringUtils;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\mpw_rest_api\Constants\ResponseCodes;
use Drupal\mpw_rest_api\Constants\ResponseMessages;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides a Blog Detail Resource
 *
 * @RestResource(
 *   id = "subscription_listing_resource",
 *   label = @Translation("Subscription Listing Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/subscription-listing",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/subscription-listing"
 *   }
 * )
 */
class SubscriptionListingResource extends ResourceBase {

 /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('my_custom_log'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database')
    );
  }

  /**
     * Responds to GET requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function get(Request $request) {

      $uid = $this->currentUser->id();    
      $query = \Drupal::database()->select('mpw_rc_subscription', 'u');
      $query->fields('u', [
        'uid',
        'nid',
        'pay_id',
        'subscription_state',
        'start_date',
        'pay_status',
        'product_id',
        'payment_amount',
        'next_billing_date',
        'last_payment_date',
        'final_payment_date',
        'pay_id',
        'payer_id',
        'payer_frist_name',
        'payer_last_name',
        'payer_email',
        'payer_address',
        'payer_city',
        'payer_state',
        'payer_postal_code',
        'subscription_start',
        'subscription_end'
      ]
    );
    $query->condition('uid', $uid);
    // For the pagination we need to extend the pagerselectextender and
    // limit in the query
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $results = $pager->execute()->fetchAll();
    // Initialize an empty array
    $output = array();
    $i = 1;
    // Next, loop through the $results array
    foreach ($results as $result) {
      if ($result->uid != 0 && $result->uid != 1) {

        switch ($result->product_id) {
          case '1':
            $product_id = 'Essentials';
            break;
          case '2':
            $product_id = 'Concierge';
            break;
          default:
            $product_id = 'Enterprise';
            break;
        }

        $node = Node::load($result->nid);
        $output[] = [
          'sno' => $i,
          'subscription' => $product_id,
          //'nid' => "<a href='/node/$result->nid'> $node->getTitle(); </a>",
          'nid' => ($node) ? $node->getTitle() : 'N/A',
          'subscription_state' => $result->subscription_state,
          'payer_name' => $result->payer_frist_name . ' '. $result->payer_last_name,
          'payer_address' => $result->payer_address . ', ' . $result->payer_postal_code . ', ' . $result->payer_city . ', ' . $result->payer_state,
          'email' => $result->payer_email,
          'next_billing_date' => $result->next_billing_date,
          'last_payment_date' => $result->last_payment_date,
          'final_payment_date' => $result->final_payment_date,
          'payer_id' => $result->payer_id,
          ];
        }
        $i++;
      }

      $response = new ResourceResponse($output);

      $cacheMeta = array(
        '#cache' => array(
          'max-age' => 0,
        ),
      );
      
      $response->addCacheableDependency($cacheMeta);
      
      return  $response;
    }

}