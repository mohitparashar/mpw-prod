<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\mpw_rest_api\Plugin\rest\resource\User;

use Drupal\mpw_rest_api\Constants\ResponseMessages;
use Drupal\mpw_rest_api\Constants\ResponseCodes;
use Drupal\rest\Plugin\ResourceBase;

use Drupal\mpw_rest_api\Utils\StringUtils;

use Drupal\user\Entity\User;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "resetpassword_resource",
 *   label = @Translation("Reset Password resource"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/resetpassword"
 *   }
 * )
 */
class ResetPasswordResource extends ResourceBase {
   
    protected $database;
    
    public function post(Request $data) {
        
        $this->database = \Drupal::service("database");
        $this->stringUtils = new StringUtils();
        
        $cache = [
                "max-age" => 0
            ];
        
        $payload = $data->getPayload();
        
        $verify_code = $this->verifyCode($payload['emailId'], $payload['code']);
        
        if($verify_code){
            $update_password = $this->updatePassword($payload['emailId'],$payload['password']);
            if($update_password)
                $response = new RESTResponseBuilder(["status"=>ResponseMessages::SUCCESS], ResponseCodes::HTTP_SUCCESS_CODE);
            else
                $response = new RESTResponseBuilder(ResponseMessages::TECHNICAL_ERROR, 400);
        }else{
            $response = new RESTResponseBuilder(ResponseMessages::EMAIL_VERIFICATION_CODE_MISMATCH_2004, ResponseCodes::EMAIL_VERIFICATION_CODE_MISMATCH);
        }
        $response->setResponseCache($cache);
        return $response;
    }
    
    private function verifyCode($emailId, $code){
			
			 $querys = "select code,count(code) as count from darwin_email_verification_codes WHERE email_id='".$emailId."' and code='".$code."'"; 
			 $query = \Drupal::database()->query($querys);
			 $results = $query->fetchAll();
			 $getResultCount = $results[0]->count;
			if($getResultCount > 0){
				if(strcmp($results[0]->code,$code) == 0){
					return $this->checkCode($emailId,$code);
				}else{
					return false;
				}
			}else{
				return false;
			}
			
    }
	
	private function checkCode($emailId,$code){
		$query = $this->database->select('darwin_email_verification_codes', 't');
		$result = $query->condition('t.email_id', $emailId)
					->condition('t.code', $code)
					->countQuery()->execute();
		$count = $result->fetchField();
		if($count > 0) return true; else return false;
	}
    
    private function updatePassword($emailId, $password){
        
                 
        $user_storage = \Drupal::entityTypeManager()->getStorage('user');
        $ids = $user_storage->getQuery()
                ->condition('mail', $emailId)
                ->condition('roles', 'registered_user')
                ->execute();
        if(!empty($ids)){
            
            $user = User::load(current($ids)); 
            $user->setPassword($password);
            $user->save();
            return true;
        }else{
            return false;
        }
        
            
    }
}
