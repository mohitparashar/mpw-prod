<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\mpw_rest_api\Utils\StringUtils;
use Symfony\Component\HttpFoundation\Request;
use Drupal\mpw_rest_api\Constants\ResponseCodes;
use Drupal\mpw_rest_api\Constants\ResponseMessages;
use Symfony\Component\DependencyInjection\ContainerInterface;


use Psr\Log\LoggerInterface;


use Drupal\user\Entity\User;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\rest\ResourceResponse;
use Drupal\Component\Utility\Html;


/**
* Provides an example resource to return the right Arnold movie quote.
*
* @RestResource(
*   id = "rp_resource",
*   label = @Translation("Reset password resource"),
*   uri_paths = {
*     "canonical" = "/api/v1/resetpass",
*     "https://www.drupal.org/link-relations/create" = "/api/v1/resetpass"
*   }
* )
*/


class RPResource extends ResourceBase {

	/**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('my_custom_log'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database')
    );
  }

  public function post(Request $request) {
    $this->stringUtils = new StringUtils();
        
    $cache = [
      "max-age" => 0
    ];

    $payload = json_decode($request->getContent(), TRUE);

    $verify_code = $this->verifyCode($payload['email'], $payload['code']);
        
    if($verify_code){
        $update_password = $this->updatePassword($payload['email'],$payload['password']);
        if($update_password)
            $response = new ResourceResponse(["status"=>ResponseMessages::SUCCESS], ResponseCodes::HTTP_SUCCESS_CODE);
        else
            $response = new ResourceResponse(ResponseMessages::TECHNICAL_ERROR, 400);
    }else{
        $response = new ResourceResponse(ResponseMessages::EMAIL_VERIFICATION_CODE_MISMATCH_2004, ResponseCodes::EMAIL_VERIFICATION_CODE_MISMATCH);
    }
    $response->addCacheableDependency($cache);
    return $response;
  }

  private function verifyCode($emailId, $code) {
    //$querys = "SELECT code,count(*) as count from mpw_resetpassword_codes WHERE email = '" . $emailId ."' and code = '" . $code ."'";
    $results = $this->database
    ->query("SELECT code FROM {mpw_resetpassword_codes} WHERE email = :email AND code = :code", 
    [
      ':email' => $emailId,
      ':code' => $code
    ])
    ->fetchField();

    if($results){
      return $this->checkCode($emailId,$code);
    }
    else{
      return false;
    }
  }
  
  private function checkCode($emailId,$code){
    $query = $this->database->select('mpw_resetpassword_codes', 't');
    $result = $query->condition('t.email', $emailId)
          ->condition('t.code', $code)
          ->countQuery()->execute();
    $count = $result->fetchField();
    if($count > 0) return true; else return false;
  }
    
  private function updatePassword($emailId, $password){
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');
    $ids = $user_storage->getQuery()
      ->condition('mail', $emailId)
      //->condition('roles', 'registered_user')
      ->execute();

    if (!empty($ids)) {
      $user = User::load(current($ids)); 
      $user->setPassword($password);
      $user->save();
      return true;
    }
    else{
      return false;
    }
  }
}