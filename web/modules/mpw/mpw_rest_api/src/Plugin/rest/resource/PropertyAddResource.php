<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;
use Drupal\mpw_rest_api\Base64Image;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RenderContext;
/**
 * Provides a resource to post User update data.
 *
 * @RestResource(
 *   id = "property_add_resource",
 *   label = @Translation("Property Add Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/add-property",
 *       "https://www.drupal.org/link-relations/create" = "/api/v1/add-property"
 *   },
 * )
 */
class PropertyAddResource extends ResourceBase {


   /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;
    private $error;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('my_custom_log'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database')
    );
  }


    /**
     * Responds to POST requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function post(Request $request) {


      if (!$this->currentUser->hasPermission('access content')) {
        throw new AccessDeniedHttpException();
      }

      $uid = $this->currentUser->id();
      $data = json_decode($request->getContent());

      $context = new RenderContext();
      $node = \Drupal::service('renderer')->executeInRenderContext($context, function () use($uid, $data) {

        $avaibility[0] = [
          'Floor',
          'Suit Number',
          'SF Available',
          'Use',
          'Term',
          'SqFt per month'
        ];
        for ($i=1; $i < 6; $i++) {
          $avaibility[$i] =[
            $data->{"floor_$i"},
            $data->{"suit_number_$i"},
            $data->{"sf_available_$i"},
            $data->{"use_$i"},
            $data->{"term_$i"},
            $data->{"sqft_per_month_$i"}
          ];
        }

        //  $avaibility = array_filter(array_map('array_filter', $avaibility));

        if (isset($data->banner_image) && !empty($data->banner_image)) {
          $path = 'property/' . date('Y-m');
          $img = new Base64Image($data->banner_image);
          $img->setFileDirectory($path);
          $file = file_save_data($img->getFileData(), 'public://' . $path . '/' . $img->getFileName() , Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE);
        }

        $field_image = [];
        foreach ($data->property_images as $key => $image) {
          if (isset($image) && !empty($image)) {
            $path = 'property/' . date('Y-m');
            $img = new Base64Image($image);
            $img->setFileDirectory($path);
            $file = file_save_data($img->getFileData(), 'public://' . $path . '/' . $img->getFileName() , Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE);
            $field_image[]['target_id'] = $file->id();
          }
        }

        $paragraph = Paragraph::create([
          'type' => 'brokers_owners',
          'field_company' => isset($data->company) ? $data->company : '',
          'field_contact_name' => isset($data->primary_contact_name) ? $data->primary_contact_name : '',
          'field_email' => isset($data->email) ? $data->email : '',
          'field_phone_number' => isset($data->phone_number) ? $data->phone_number : '',
          'field_company_second' => isset($data->secondary_company) ? $data->secondary_company : '',
          'field_license' => isset($data->license) ? $data->license : '',
          'field_contact_name_second' => isset($data->secondary_contact_name) ? $data->secondary_contact_name : '',
          'field_email_second' => isset($data->secondary_email) ? $data->secondary_email : '',
          'field_phone_number_second' => isset($data->secondary_phone_number) ? $data->secondary_phone_number : '',
          'field_secondary_license'  => isset($data->secondary_license) ? $data->secondary_license : '',
        ]);
        $paragraph->save();

        $node = Node::create(
          array(
            'type' => 'property',
            'title' => $data->property_title,
            'status' => false,
            'field_listing_type' => $data->listing_type,
            'field_property_type' => ['target_id' => $data->property_type],
            'field_banner_image' => ['target_id' => $file->id()],
            'field_image' => $field_image,
            'field_video' => $data->video,
            'field_address' => [
              'langcode' => "",
              'country_code' =>  "US",
              'administrative_area' =>  $data->state,
              'locality' =>  $data->city,
              'dependent_locality' => $data->county,
              'postal_code' =>  $data->zip,
              'sorting_code' =>  "",
              'address_line1' =>  $data->street_address,
              'address_line2' => $data->lsuit_number,
              'organization' =>  "",
              'given_name' =>  "",
              'additional_name' =>  "",
              'family_name' =>  "",
            ],
            'field_brokers_owners' => [
              'target_id' => $paragraph->id(),
              'target_revision_id' => $paragraph->getRevisionId(),
            ],
            'field_avaibility' => [
              [
                "value" =>
               // [
              //   "0" => [
              //       "Suite Name/Number",
              //       "Space Type",
              //       "Rental Rate",
              //       "Space Available"
              //   ],
              //   "1" => [
              //       "Office #1 I",
              //       "Office Space",
              //       "$569",
              //       "1 Person Office Space"
              //   ],
              //   "2" => [
              //       "Office #2 I",
              //       "Office Space",
              //       "$909",
              //       "2-4 Person Office Space"
              //   ],
              //   "3" => [
              //       "Office #3 I",
              //       "Office Space",
              //       "$1,189",
              //       "4-5 Person Office Space"
              //   ],
              //   "4" => [
              //       "Office #4 W",
              //       "Office Space",
              //       "$3,659",
              //       "6-12 Person Office Space"
              //   ],
              // ],
              $avaibility,
              "format" => null,
              "caption" => ""
              ],
            ],
            'field_referenced_city_page' => [
              'target_id' => 11
            ],
            'body' => [
              'value' => $data->property_description,
              'format' => 'basic_html',
            ],
          )
        );

        $violations = $node->validate();
        if ($violations->count() > 0) {
          $violated_fields  = $violations->getFieldNames();
          foreach ($violations as $key => $violation) {
            $this->error[$violation->getPropertyPath()] = $violation->getMessage()->render();
          }

          $this->error = array_combine($violated_fields, $this->error);
          return ['error' => $this->error];
        }
        else {
          $node->save();
        }
        return ['sucess' => 'ok', 'nid' => $node->id()];

      });

      // Update node id in subscription table.
      $this->updateNidForSubscription($node['nid']);

      $response = new ResourceResponse($node, 200);
      // $response->addCacheableDependency($node);
      return $response;

      //print_r($violation->getMessage()->render());
      //print_r($violation->getPropertyPath());
      //print_r($violation->getInvalidValue());
      //print_r($violations->getByField('title'));

      // Validation failed.
      //print_r($violations->getInvalidValue());
      // print_r($violations->__toString());

      //print $violations[0]->getMessage();
      //print_r($violations->toArray());exit;
    }


    /**
     * Responds to PATCH requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function patch(Request $request) {

      if (!$this->currentUser->hasPermission('access content')) {
        throw new AccessDeniedHttpException();
      }

      $uid = $this->currentUser->id();
      $data = json_decode($request->getContent());

// $node = Node::load($data->id);

// // Update avaibility
//         $avaibility[0] = [
//           'Floor',
//           'Suit Number',
//           'SF Available',
//           'Use',
//           'Term',
//           'SqFt per month'
//         ];
//         for ($i=1; $i < 5; $i++) {
//           $avaibility[$i] =[
//             $data->{"floor_$i"},
//             $data->{"suit_number_$i"},
//             $data->{"sf_available_$i"},
//             $data->{"use_$i"},
//             $data->{"term_$i"},
//             $data->{"sqft_per_month_$i"}
//           ];
//         }

//         $node->field_avaibility->value = $avaibility;
//         //print_r($node->get('field_brokers_owners')->entity);exit;

//         $node->save();
//         echo 'ok';
//         exit;
      $context = new RenderContext();
      $node = \Drupal::service('renderer')->executeInRenderContext($context, function () use($uid, $data) {

        $node = Node::load($data->id);
        //print_r($node);exit;
        // Update title.
        $node->title = $data->property_title;
        // Update listing type.
        $node->field_listing_type = $data->listing_type;
        // Update property type.
        $node->field_property_type = $data->property_type;
        // Update Video URL
        $node->field_video = $data->video;

        // Update user contact information data.
        $brokers_owners = $node->get('field_brokers_owners')->entity;
        $brokers_owners->set('field_company', $data->company);
        $brokers_owners->set('field_contact_name', $data->primary_contact_name);
        $brokers_owners->set('field_email', $data->email);
        $brokers_owners->set('field_phone_number', $data->phone_number);
        $brokers_owners->set('field_company_second', $data->secondary_company);
        $brokers_owners->set('field_license', $data->license);
        $brokers_owners->set('field_contact_name_second', $data->secondary_contact_name);
        $brokers_owners->set('field_email_second', $data->secondary_email);
        $brokers_owners->set('field_phone_number_second', $data->secondary_phone_number);
        $brokers_owners->set('field_secondary_license', $data->secondary_license);
        $brokers_owners->save();

        // Update address.
        $node->field_address->locality =  $data->city;
        $node->field_address->dependent_locality = $data->county;
        $node->field_address->postal_code =  $data->zip;
        $node->field_address->address_line1 =  $data->street_address;
        $node->field_address->address_line2 = $data->lsuit_number;
        // Update body.
        $node->body->value = $data->property_description;
        // Update avaibility
        $avaibility[0] = [
          'Floor',
          'Suit Number',
          'SF Available',
          'Use',
          'Term',
          'SqFt per month'
        ];
        for ($i=1; $i < 6; $i++) {
          $avaibility[$i] =[
            $data->{"floor_$i"},
            $data->{"suit_number_$i"},
            $data->{"sf_available_$i"},
            $data->{"use_$i"},
            $data->{"term_$i"},
            $data->{"sqft_per_month_$i"}
          ];
        }

        $node->field_avaibility->value = $avaibility;
        // print_r($data);exit;

        if (isset($data->banner_image) && !empty($data->banner_image)) {
          $path = 'property/' . date('Y-m');
          $img = new Base64Image($data->banner_image);
          $img->setFileDirectory($path);
          $file = file_save_data($img->getFileData(), 'public://' . $path . '/' . $img->getFileName() , Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE);
          // Save image if changes
          $node->field_banner_image->target_id = $file->id();
        }

        $field_image = [];
        if (isset($data->property_images) && !empty($data->property_images)) {
          foreach ($data->property_images as $key => $image) {
            if (isset($image) && !empty($image)) {
              $path = 'property/' . date('Y-m');
              $img = new Base64Image($image);
              $img->setFileDirectory($path);
              $file = file_save_data($img->getFileData(), 'public://' . $path . '/' . $img->getFileName() , Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE);
              $field_image[]['target_id'] = $file->id();
            }
          }
          // Save property image
          $node->field_image = $field_image;
        }

        $violations = $node->validate();
        if ($violations->count() > 0) {
          $violated_fields  = $violations->getFieldNames();
          foreach ($violations as $key => $violation) {
            $this->error[$violation->getPropertyPath()] = $violation->getMessage()->render();
          }

          $this->error = array_combine($violated_fields, $this->error);
          return ['error' => $this->error];
        }
        else {
          $node->save();
        }
        return ['sucess' => 'ok'];
      });

      $response = new ResourceResponse($node, 200);
      // $response->addCacheableDependency($node);
      return $response;
    }

    private function updateNidForSubscription($nid = 0) {
      // Change status of subscription
      $this->database->update('mpw_rc_subscription')
       ->fields(array('nid' => $nid))
       ->condition('nid', 0)
       ->condition('uid', $this->currentUser->id())
       ->execute();
      return TRUE;
    }
  }
