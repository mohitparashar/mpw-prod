<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;
use Drupal\mpw_rest_api\Base64Image;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mpw_subscription\PayFlowRC;
/**
 * Provides a resource to post User update data.
 *
 * @RestResource(
 *   id = "subscription_cancel_resource",
 *   label = @Translation("Subscription cancel Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/cancel-subscription",
 *       "https://www.drupal.org/link-relations/create" = "/api/v1/cancel-subscription"
 *   },
 * )
 */
class SubscriptionCancelResource extends ResourceBase {

  /**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;
    private $error;

  /**
    * Constructs a new object.
    *
    * @param array $configuration
    *   A configuration array containing information about the plugin instance.
    * @param string $plugin_id
    *   The plugin_id for the plugin instance.
    * @param mixed $plugin_definition
    *   The plugin implementation definition.
    * @param array $serializer_formats
    *   The available serialization formats.
    * @param \Psr\Log\LoggerInterface $logger
    *   A logger instance.
    * @param \Symfony\Component\HttpFoundation\Request $request
    *   The request object.
    * @param \Drupal\Core\Session\AccountProxyInterface $current_user
    *   A current user instance.
    */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      Request $request,
      Connection $connection) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->request = $request;
        $this->currentUser = $current_user;
        $this->database = $connection;
    }

	/**
	* {@inheritdoc}
	*/
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
	  return new static(
		$configuration,
		$plugin_id,
		$plugin_definition,
		$container->getParameter('serializer.formats'),
		$container->get('logger.factory')->get('mpw_rest_api'),
		$container->get('current_user'),
		$container->get('request_stack')->getCurrentRequest(),
		$container->get('database')
	  );
	}


    /**
     * Responds to POST requests.
     *
     * @param array $request
     *
     * @return \Drupal\rest\ResourceResponse
     */
    public function post(Request $request) {

      if (!$this->currentUser->hasPermission('access content')) {
        throw new AccessDeniedHttpException();
      }

      $uid = $this->currentUser->id();
      $data = json_decode($request->getContent());
      $profileid = $data->profileid;

      $card_response = $this->cancelSubscription($profileid);

      if ($card_response['RESULT'] == '0') {
        $response['status'] = 'success';
        $response['message'] = 'Subscription has been cancel successfully!';
        // Update user subscription
        $this->updateSubscriptionStatus($profileid);
      }
      else {
        $response['status'] = 'error';
        $response['message'] = $card_response['RESPMSG'];
      }

      $response = new ResourceResponse($response, 200);
      return $response;

    }

    private function cancelSubscription($profileid) {
      // update status on mpw rc recurring.
	  // Recurring Transaction.
	  $PayFlow = new PayFlowRC('MPWINFO', 'PayPal', 'MohitMPW', 'MohitMPW123', 'recurring');
	  // test or live.
	  $PayFlow->setEnvironment('test');
	  // S = Sale transaction, R = Recurring.
	  $PayFlow->setTransactionType('R');
	  // A = Automated clearinghouse, C = Credit card.
	  $PayFlow->setPaymentMethod('C');
	  // Only used for recurring transactions.
	  $PayFlow->setProfileAction('C');
	  // Set profile id.
	  $PayFlow->setProfileId($profileid);

	  $PayFlow->processTransaction();
	  return $PayFlow->getResponse();
	}

	private function updateSubscriptionStatus($profileid) {
  	// Change status of subscription
 	  $this->database->update('mpw_rc_subscription')
	   ->fields(array('subscription_state' => 'Canceled'))
	   ->condition('uid', $this->currentUser->id())
	   ->condition('payer_id', $profileid)
	   ->execute();

      // Update property to show MPW info.
      $query =$this->database->select('mpw_rc_subscription', 'mpw_rc')
	    ->fields('mpw_rc', ['nid'])
	    ->condition('uid', $this->currentUser->id())
	    ->condition('payer_id', $profileid);
	  $results = $query->execute();

	  while ($content = $results->fetchAssoc()) {
	    // Operations using $content.
	    $nid = $content['nid'];
	  }

	  $node = Node::load($node);
	  if ($node) {
  		$node->set('field_show_broker_information', FALSE);
  		$node->save();
	  }

  	return TRUE;
	}
 

}