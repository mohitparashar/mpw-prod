<?php

namespace Drupal\mpw_rest_api\Plugin\rest\resource;

use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Drupal\Component\Utility\Html;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxy;
use Drupal\Component\Serialization\Json;
use Drupal\mpw_rest_api\Utils\StringUtils;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\mpw_rest_api\Constants\ResponseCodes;
use Drupal\mpw_rest_api\Constants\ResponseMessages;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
* Provides an example resource to return the right Arnold movie quote.
*
* @RestResource(
*   id = "fp_resource",
*   label = @Translation("Forgot password resource"),
*   uri_paths = {
*     "canonical" = "/api/v1/forgotpass",
*     "https://www.drupal.org/link-relations/create" = "/api/v1/forgotpass"
*   }
* )
*/


class FPResource extends ResourceBase {

	/**
    * Drupal\Core\Session\AccountProxy definition.
    *
    * @var \Drupal\Core\Session\AccountProxy
    */
    protected $currentUser;
    // Database object.
    protected $database;
    // String utils object.
    private $stringUtils;

/**
  * Constructs a new object.
  *
  * @param array $configuration
  *   A configuration array containing information about the plugin instance.
  * @param string $plugin_id
  *   The plugin_id for the plugin instance.
  * @param mixed $plugin_definition
  *   The plugin implementation definition.
  * @param array $serializer_formats
  *   The available serialization formats.
  * @param \Psr\Log\LoggerInterface $logger
  *   A logger instance.
  * @param \Symfony\Component\HttpFoundation\Request $request
  *   The request object.
  * @param \Drupal\Core\Session\AccountProxyInterface $current_user
  *   A current user instance.
  */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    Request $request,
    Connection $connection) {
      parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
      $this->request = $request;
      $this->currentUser = $current_user;
      $this->database = $connection;
  }

 /**
  * {@inheritdoc}
  */
 public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
   return new static(
     $configuration,
     $plugin_id,
     $plugin_definition,
     $container->getParameter('serializer.formats'),
     $container->get('logger.factory')->get('my_custom_log'),
     $container->get('current_user'),
     $container->get('request_stack')->getCurrentRequest(),
     $container->get('database')
   );
 }

	public function get() {
	   
	}

 	public function post(Request $request) {
    $this->stringUtils = new StringUtils();
    
    $cache = [
      "max-age" => 0
    ];
    
    $payload = json_decode($request->getContent(), TRUE);

    $email = isset($payload['email']) ? $payload['email'] : $payload['emai'];

    $verfication_code_sent = $this->sendVerificationCode($email, $this->currentUser);

    if ($verfication_code_sent) {
      $response = new ResourceResponse(["status"=>ResponseMessages::SUCCESS], ResponseCodes::HTTP_SUCCESS_CODE);
    }
    else {
      $response = new ResourceResponse(ResponseMessages::ERROR_SENDING_EMAIL_2003, ResponseCodes::HTTP_SUCCESS_CODE);
    }

    // $response->addCacheableDependency($cache);
    return $response;
  }
    
  private function sendVerificationCode($emailId, $user) {
      global $base_url;
          
      $user = user_load_by_mail($emailId);

      // Validating mail id
      if(empty($user)){
          return false;
      }
          
      $mailManager = \Drupal::service('plugin.manager.mail');
      $module = "mpw_rest_api";
      $key = 'forgot_password';
      $to = $emailId;
      $code = $this->stringUtils->generateRandomString(5);

          
      $params['message'] = '
          Dear User,

          Please use code mention '. $code .'

          MPW Team';

    $langcode = $user->getPreferredLangcode();
    $send = true;
    $this->saveEmailVerficationCodes($emailId,$code);
    
    ob_start();
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    ob_end_clean();

    if ($result['send']) {
      return true;
    }
    else {
      return false;
    }
  }
    
    private function saveEmailVerficationCodes($emailId, $code){
        
      $this->database->merge('mpw_resetpassword_codes')
        ->key('email', $emailId)
        ->fields([
            'code' => $code
        ])
        ->execute();
     
      return true;
        
    }

}
