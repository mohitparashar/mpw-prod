<?php
namespace Drupal\mpw_rest_api;

use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Base64Image {
  protected $base64Image;
  protected $fileData;
  protected $fileName;
  protected $directory;

  public function __construct($base64Image) {
    $this->base64Image = $base64Image;
    $this->decodeBase64Image();
  }

  protected function decodeBase64Image() {
    $this->fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $this->base64Image));
    if ($this->fileData === FALSE) {
      throw new BadRequestHttpException('Image could not be processed.');
    }
    // Determine image type
    $f = finfo_open();
    $mimeType = finfo_buffer($f, $this->fileData, FILEINFO_MIME_TYPE);
    // Generate fileName
    $ext = $this->getMimeTypeExtension($mimeType);
    $this->fileName = uniqid(rand()) . $ext;
  }

  /**
   * @param string $mimeType
   *
   * @return string
   */
  protected function getMimeTypeExtension($mimeType) {
    $mimeTypes = [
      'image/png' => 'png',
      'image/jpeg' => 'jpg',
      'image/gif' => 'gif',
      'image/bmp' => 'bmp',
      'image/vnd.microsoft.icon' => 'ico',
      'image/tiff' => 'tiff',
      'image/svg+xml' => 'svg',
    ];
    if (isset($mimeTypes[$mimeType])) {
      return '.' . $mimeTypes[$mimeType];
    }
    else {
      $split = explode('/', $mimeType);
      return '.' . $split[1];
    }
  }

  /**
   * @return mixed
   */
  public function getFileData() {
    return $this->fileData;
  }

  /**
   * @return mixed
   */
  public function getFileName() {
    return $this->fileName;
  }

  /**
   * @param string $path
   */
  public function setFileDirectory($path) {
    $this->directory = \Drupal::service('file_system')->realpath(Drupal::config('system.file')->get('default_scheme') . "://");
    $this->directory .= '/' . $path;
    Drupal\Core\File\FileSystemInterface::prepareDirectory($this->directory, Drupal\Core\File\FileSystemInterface::MODIFY_PERMISSIONS | Drupal\Core\File\FileSystemInterface::CREATE_DIRECTORY);
  }

  /**
   * @param $path
   */
  public function setImageStyleImages($path) {
    $styles = ['avatar_large', 'avatar_medium', 'avatar_small'];
    foreach ($styles as $style) {
      $imageStyle = ImageStyle::load($style);
      $uri = $imageStyle->buildUri($path . '/' . $this->fileName);
      $imageStyle->createDerivative($this->directory . '/' . $this->fileName, $uri);
    }
  }
}
