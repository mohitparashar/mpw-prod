<?php

/**
 * @file
 * Contains Drupal\mpw_rest_api\Form\ConsumersForm.
 */

namespace Drupal\mpw_rest_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

class WhiteListApiForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'mpw_whitelistconsumers_form';
  }
  /**
   * {@inheritdoc}
   */
  public function  getEditableConfigNames() {
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->get('mpw_rest_api.mpw_form_config');

    $form['mpw_whitelist_api'] = [
      '#type' => 'textarea',
      '#title' => $this->t('White List Rest'),
      '#description' => $this->t('Place route which needs to White List on separate lines'),
      '#default_value' => $config->get('mpw_whitelist_api'),
    ];

    $form['mpw_whitelist_api_domain'] = [
      '#type' => 'textarea',
      '#title' => $this->t('White List Rest API for domains'),
      '#description' => $this->t('Please add domain which needs to authenticate for rest request.'),
      '#default_value' => $config->get('mpw_whitelist_api_domain'),
    ];

    return parent::buildForm($form, $form_state);
  }

  protected function getSerializerFormats() {
    try {
      $serializedFormats = \Drupal::getContainer()->getParameter('serializer.formats');
      return array_combine($serializedFormats, $serializedFormats);
    } catch (ParameterNotFoundException $e) {
      Drupal\Core\Messenger\MessengerInterface::addMessage(
          sprintf(
            '%s %s',
            $e->getMessage(),
            $this->t('Please, install module "RESTful Web Services"')
          ),
          'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = \Drupal::configFactory()->getEditable('mpw_rest_api.mpw_form_config');
    $config->set('mpw_whitelist_api', $form_state->getValue('mpw_whitelist_api'));
    $config->set('mpw_whitelist_api_domain', $form_state->getValue('mpw_whitelist_api_domain'));
    $config->save();
  }
}
