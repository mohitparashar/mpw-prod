<?php

namespace Drupal\mpw_rest_api\Constants;

/**
 * Description of RESTConstants
 *
 */
class ResponseCodes {
    const HTTP_SUCCESS_CODE = 200;
    const CAPTCH_CODE_MISMATCH = 2000;
    const REGISTERED_EMAILID_EXISTS = 2001;
    const INVALID_CREDENTAILS = 2002;
    const ERROR_SENDING_EMAIL = 2003;
    const EMAIL_VERIFICATION_CODE_MISMATCH = 2004;
    const ACCOUNT_BLOCKED = 403;
    const INVALID_REQUEST = 400;
}
