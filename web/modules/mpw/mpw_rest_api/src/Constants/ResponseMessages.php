<?php

namespace Drupal\mpw_rest_api\Constants;

/**
 * Description of RESTErrorResponseMessages
 *
 */
class ResponseMessages {
    const CAPTCH_CODE_MISMATCH_2000 = "Captch code does not match";
    const OTP_CODE_MISMATCH_2000 = "Otp code does not match";
    const REGISTERED_EMAILID_EXISTS_2001 = "Email Id already exists";
    const REGISTERED_EMAILID_MOBILE_EXISTS_2001 = "Email Id/Mobile No already exists";
    const REGISTERED_MOBILE_EXISTS = "Mobile No already exists";
    const INVALID_CREDENTIALS_2002 = "Invalid username/password";
    const INVALID_REQUEST = "Invalid Request";
    const INVALID_TOKEN = "Invalid Token";
    const INVALID_EMAIL = "Invalid Email Id";
    const VALIDATION_FAILED = "Validation Failed";
    const TECHNICAL_ERROR = "Technical Error";
    const SUCCESS = "Success";
    const ERROR = "Error";
    const INVALID_EXISTING_PWD = "Invalid existing password";
    const ERROR_SENDING_EMAIL_2003 = "Error sending email";
    const EMAIL_VERIFICATION_CODE_MISMATCH_2004 = "Email verification code mismatch";
    const INVALID_DEVICE = "Invalid device type";
    const PLAN_SUBSCRIBED = "Plan has been already subscribed";
    const ACCOUNT_BLOCKED = "Account Blocked";
    const FORBIDDEN = "Forbidden";
    const INVALID_DBID = "Invalid DB-Id";
    const EMAIL_NOT_REGISTERED = "Email id is not registered";
    const INVALID_QR = "QR Not valid";
    const ACCESS_DENIED = "Access denied";
    const NO_TOKEN_DATA = "No Token Present";
}
