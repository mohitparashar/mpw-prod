<?php

namespace Drupal\mpw_rest_api\Controller;

use Drupal\mpw_rest_api\JWTHandler;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class JwtAuthIssuerController.
 *
 * @package Drupal\jwt_auth_issuer\Controller
 */
class MpwAuthController extends ControllerBase {
  /**
   * Generate.
   *
   * @return string
   *   Return token string.
   */
  public function getToken(Request $request) {

    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
      $data = json_decode($request->getContent(), TRUE);
      $request->request->replace(is_array($data) ? $data : []);

      echo $uid = \Drupal::service('user.auth')->authenticate($data['username'], $data['password']);

     

       $jwtHandler = new JWTHandler($uid);
       $token = $jwtHandler->generateTokens();
        var_dump($token);

      exit;

    }

    $response['data'] = 'Some test data to return';
    $response['method'] = 'POST';

    return new JsonResponse($response);
  }
}