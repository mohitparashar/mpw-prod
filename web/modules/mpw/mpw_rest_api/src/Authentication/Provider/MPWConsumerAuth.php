<?php

/**
 * @file
 * Contains \Drupal\mpw_rest_api\Authentication\Provider\MPWConsumerAuth.
 */

namespace Drupal\mpw_rest_api\Authentication\Provider;

//use Drupal\Component\Utility\String;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\user\UserAuthInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Url;

use Symfony\Component\HttpFoundation\RequestStack;
/**
 * HTTP Basic authentication provider.
 */
class MPWConsumerAuth implements AuthenticationProviderInterface {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The user auth service.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request_stack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The user authentication service.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->request_stack = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {

    // $current_path = \Drupal::service('path.current')->getPath();
    // $route_provider = \Drupal::service('router.route_provider');
    // $found_routes = $route_provider->getRoutesByPattern($current_path);
    // $route_iterator = $found_routes->getIterator();
    // $route_name = [];

    // if (count($route_iterator)) {
    //   // Route found.
    //   foreach($route_iterator as $route => $route_obj) {
    //     $route_name[] = $route;
    //   }
    // }

    $current_path = \Drupal::service('path.current')->getPath();

    if (Url::fromUserInput($current_path)->isRouted()) {
      $route_name = Url::fromUserInput($current_path)->getRouteName();

      // Only apply this validation if request has a valid accept value
      $config = $this->configFactory->get('mpw_rest_api.mpw_form_config');
      $api_consumers = $config->get('mpw_whitelist_api');

      $api = array_map('trim', explode( "\n", $api_consumers));

      if (in_array($route_name, $api)) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    $config = $this->configFactory->get('mpw_rest_api.mpw_form_config');
    $domain_consumers = $config->get('mpw_whitelist_api_domain');
    // Determine if list of IP is a white list or black list
    $domain_consumers = array_map('trim', explode( "\n", $domain_consumers));
    // Origin
    $origin = $request->headers->get('origin');
    // Remove the http://, www., and slash(/) from the URL
    $consumer_ip = $this->getCleanIP($origin);
    // White list logic
    if (in_array($consumer_ip, $domain_consumers)) {
      // Return Anonymous user
      return $this->entityTypeManager->getStorage('user')->load(0);
    }
    else{
      throw new AccessDeniedHttpException();
      return null;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cleanup(Request $request) {}

  /**
   * {@inheritdoc}
   */
  public function handleException(ExceptionEvent $event) {
    $exception = $event->getThrowable();
    if ($exception instanceof AccessDeniedHttpException) {
      $event->setThrowable(new UnauthorizedHttpException('Invalid consumer origin.', $exception));
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Helper function to get clean IP.
   */
  public function getCleanIP($input) {
    // If URI is like, eg. www.way2tutorial.com/
    $input = trim($input, '/');
    // If not have http:// or https:// then prepend it
    if (!preg_match('#^http(s)?://#', $input)) {
        $input = 'http://' . $input;
    }
    // Get url in parts.
    $urlParts = parse_url($input);

    // Remove www.
    $domain_name = preg_replace('/^www\./', '', $urlParts['host']);

    return $domain_name;
  }
}
