<?php

namespace Drupal\mpw_rest_api;

use Drupal\jwt\Transcoder\JwtTranscoder;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\Authentication\Event\JwtAuthValidEvent;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\Authentication\Event\JwtAuthValidateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class JWTHandler implements EventSubscriberInterface {

    const TYPE_AUTH = 1;
    const TYPE_REFRESH = 2;
    const AUTH_TOKEN_VERSION = "1.0.0";
    const REFRESH_TOKEN_VERSION = "1.0.0";
    const DISABLE_TOKEN_INVALIDATION = TRUE;

    /**
     * The JWT Transcoder service.
     *
     * @var \Drupal\jwt\Transcoder\JwtTranscoderInterface
     */
    protected $transcoder;

    /**
     * The event dispatcher.
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * User id.
     */
    protected $uid;

    /**
     * Constructor
     * @param type $uid
     */
    public function __construct($uid) {
        $this->eventDispatcher = \Drupal::service('event_dispatcher');
        $this->transcoder = new JwtTranscoder(new \Firebase\JWT\JWT(), \Drupal::configFactory(), \Drupal::service('key.repository'));
        $this->uid = $uid;
    }


    /**
    * {@inheritdoc}
    */
    public static function getSubscribedEvents() {
        $events = [];
        $events[JwtAuthEvents::GENERATE][] = ['setStandardClaims', 98];
        $events[JwtAuthEvents::GENERATE][] = ['setDrupalClaims', 99];
        return $events;
    }


    /**
     * Generates a new JWT.
     */
    public function generateTokens() {
        $authToken = $this->generateAuthToken();
        return [$authToken];
    }

    /**
     * generateAuthToken
     * @return type
     */
    private function generateAuthToken() {
        $event = new JwtAuthGenerateEvent(new JsonWebToken());
        $event->addClaim(['mpw', 'uid'], $this->uid);
        $this->eventDispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
        $jwt = $event->getToken();
        return $this->transcoder->encode($jwt);
    }


   /**
    * Sets the standard claims set for a JWT.
    *
    * @param \Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent $event
    *   The event.
    */
    public function setStandardClaims(JwtAuthGenerateEvent $event) {
        $event->addClaim('iat', time());
        // @todo: make these more configurable.
        $event->addClaim('exp', strtotime('+2 months'));
    }

   /**
    * Sets claims for a Drupal consumer on the JWT.
    *
    * @param \Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent $event
    *   The event.
    */
    public function setDrupalClaims(JwtAuthGenerateEvent $event) {
      $event->addClaim(
        ['drupal', 'uid'],
        $this->currentUser->id()
      );
    }

    /**
     * Load user
     * @param \Drupal\darwin_rest_api\JwtAuthValidEvent $event
     */
    public static function loadUser(JwtAuthValidEvent $event) {
        $token = $event->getToken();
        $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');
        $uid = $token->getClaim(['drupal', 'uid']);
        $user = $user_storage->load($uid);
        $event->setUser($user);
    }

}
