(function($, undefined) {
    var $window = $(window),
        $body = $(document.body),
        header = $('#header');
        mobile_menu_cont = $('#mobile-menu-cont'),
        mobile_menu = mobile_menu_cont.children('.mobile-menu'),
        mobile_menu_trigger = mobile_menu_cont.children('.menu-button');

    var is_mobile_or_tablet = false;
	function isMobileOrTablet(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) is_mobile_or_tablet = true;}
	isMobileOrTablet(navigator.userAgent||navigator.vendor||window.opera);



	// function for setting full height to welcome message
	function BannerHeight() {
		// var el = $('#front-featured-banner');
		// var el_property_image = $('#property_page_banner');
        var el_not_founnd = $("meta[content*='404']").closest('head').siblings('body').find('#front-featured-banner');
		var el_big_banner = $('#content').find('.paragraph.big-banner.full-screan');
		var content = $('#content');
		var header_height = header.height();
		// el.height($window.height() - header_height);
		el_not_founnd.height($window.height() - header_height);
		// el_property_image.height($window.height() - header_height);
		el_big_banner.children('.field--name-field-slides').find('.image-cont').height($window.height() - header_height);
        content.css("padding-top", header_height);
        // return [el, el_property_image], el_big_banner;
        return el_not_founnd, el_big_banner, content;
	}
	//

    $(document).ready(function() {
    	var big_carousel_options = {
			animateOut: 'fadeOut',
            items: 1,
			//loop: true,//was not working with fade
            nav: true,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
			autoplay:false,
			//autoplayTimeout:5000,
            lazyLoad: true
        };
        if ( $( ".view-office-space" ).length ) $('.view-office-space:not(".view-display-id-city_banner_map, .view-display-id-property_city_map")').children('.view-content').find('.views-field-field-image').find('.field-content').owlCarousel(big_carousel_options);
        mobile_menu_trigger.on('click', function(e) {
            mobile_menu.toggleClass('actv');
            mobile_menu_trigger.toggleClass('actv');
			$("body").toggleClass('mobile-menu-oppened');
        });
		$('#overlay').on('click', function(e) {
			 mobile_menu.toggleClass('actv');
            mobile_menu_trigger.toggleClass('actv');
			$("body").toggleClass('mobile-menu-oppened');
        });

		$('#city-page-banner').each(function () {
            if ($(this).children('.city').find('.views-row').length == 1) {
                $(this).children('.state').hide();
                $(this).children('.no-result').hide();
            }
            else if($(this).children('.state').find('.views-row').length == 1) {
                $(this).children('.city').hide();
                $(this).children('.no-result').hide();
            }
            else {
                $(this).children('.state').hide();
                $(this).children('.city').hide();
            }
        });
        var geolocation_input = $('#views-exposed-form-office-space-search-page');
        geolocation_input.find('input.form-autocomplete').each(function() {
                if ($(this).val().length == 0) {
                    geolocation_input.find('input.form-submit').attr('disabled', 'disabled');
                }
            });
         geolocation_input.find('input.form-autocomplete').keyup(function() {

            var empty = false;
            geolocation_input.find('input.form-autocomplete').each(function() {
                if ($(this).val().length == 0) {
                    empty = true;
                }
            });

            if (empty) {
                geolocation_input.find('input.form-submit').attr('disabled', 'disabled');
            } else {
                geolocation_input.find('input.form-submit').removeAttr('disabled');
            }
        });

        geolocation_input.find('input.form-autocomplete').on('autocompleteselect', function (e, ui) {
            setTimeout( function(){
                $('#views-exposed-form-office-space-search-page').submit();
            }  , 500 );
        });

        // Bind enter to select first item from autocomplete list.
        geolocation_input.find('input.form-autocomplete').on('keypress' ,function(e) {
            if(e.which == 13) {
                var list = geolocation_input.find('input.form-autocomplete').autocomplete("widget");
                $(list[0].children[0]).click();
            }
        });

        $('.view-featured-locations.view-display-id-cities').find('.form-autocomplete').on('autocompleteselect', function (e, ui) {
            setTimeout( function(){
                $('#views-exposed-form-featured-locations-cities').submit();
            }  , 500 );
        });

        // var showChar = 100;  // How many characters are shown by default
        // var showP = 100;  // How many characters are shown by default
        // var ellipsestext = "...";
        // var moretext = "Read more";
        // var lesstext = "Read less";
        //
        //
        // $('.view-office-space').find('.text').each(function() {
        //     var content = $(this).html();
        //
        //     if(content.length > showChar) {
        //
        //         var c = content.substr(0, showChar);
        //         var h = content.substr(showChar, content.length - showChar);
        //
        //         var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        //
        //         $(this).html(html);
        //     }
        // });
        //
        // $(".morelink").click(function(){
        //     if($(this).hasClass("less")) {
        //         $(this).removeClass("less");
        //         $(this).html(moretext);
        //     } else {
        //         $(this).addClass("less");
        //         $(this).html(lesstext);
        //     }
        //     $(this).parent().prev().toggle();
        //     $(this).prev().toggle();
        //     return false;
        // });

        $('.view-office-space').find('.text').readmore({
            speed: 75,
            collapsedHeight: 230,
            moreLink: '<a href="#">Read more</a>',
            lessLink: '<a href="#">Read less</a>'
        });

        $('#block-mpw-content').find('.small-description-ciy').readmore({
            speed: 75,
            collapsedHeight: 230,
            moreLink: '<a href="#">Read more</a>',
            lessLink: '<a href="#">Read less</a>'
        });

        $('#city-content-body').find('.small-description-city').readmore({
            speed: 75,
            collapsedHeight: 230,
            moreLink: '<a href="#">Read more</a>',
            lessLink: '<a href="#">Read less</a>'
        });

    });

    Drupal.behaviors.mpw = {
        attach: function(context, settings) {
			$(window).on('load', BannerHeight);
			$(window).on('resize', BannerHeight);
            $(window).on('load', function() {
                $(".se-pre-con").fadeOut("slow");
            });

             var big_carousel_options = {
                animateOut: 'fadeOut',
                items: 1,
                //loop: true,//was not working with fade
                nav: true,
                navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
                dots: false,
                autoplay:false,
                //autoplayTimeout:5000,
                lazyLoad: true
            };
            if ($(".property-row").length) {
                $(".property-row").once().find('.views-row').find('.views-field-field-image').find('.field-content').owlCarousel(big_carousel_options);
            }
        }
    };

    // This drives the interaction with the Geofield Google Map.
    Drupal.behaviors.geofieldGoogleMapInteraction = {
        attach: function (context, settings) {

          // React on geofieldMapInit event.
          $(document).on('geofieldMapInit', function (e, mapid) {
            var map = Drupal.geoFieldMap.map_data[mapid].map;
            var markers = Drupal.geoFieldMap.map_data[mapid].markers;
            $('.property-row .medium-4 .views-field .marker-selector', context).each(function () {
              $(this).hover(
                function() {
                  $( this ).css("text-decoration", "underline");
                  var marker_id = $(this).data('marker-id');
                  setTimeout(function () {
                    if(markers[marker_id]) {
                        // map.setZoom(14);
                        // map.panTo(markers[marker_id].getPosition());
                        setTimeout(function () {
                            google.maps.event.trigger(markers[marker_id], 'click');
                        }, 200);
                    }
                  }, 500);
                }, function() {
                  $( this ).css("text-decoration", "none");
                  setTimeout(function () {
                    map.infowindow.close();
                  }, 200);
                }
              );
            });

            /*
            $('.property-row', context).each(function () {
              var timeoutId;
              $(this).hover(
                function() {
                }, function() {
                  $( this ).css("text-decoration", "none");
                  timeoutId = setTimeout(function () {
                    map.infowindow.close();
                    $('#' + 'geofield-map--' + mapid + '--reset-control').click();
                  }, 8000);
                }
              );
            });
            SSS*/

          });
        }
    };

    Drupal.behaviors.mpw_tablefield_cleanup = {
        attach: function(context, settings) {
          $('table.tablefield > tbody tr').each(function(){
            if($(this).children('td:empty').length === $(this).children('td').length){
              $(this).remove();
            }
          });
        }
    };

})(jQuery);
