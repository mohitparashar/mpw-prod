/*!
 * modernizr v3.1.0
 * Build http://modernizr.com/download?-csstransitions-details-inputtypes-touchevents-addtest-prefixes-teststyles-dontmin
 *
 * Copyright (c)
 *  Faruk Ates
 *  Paul Irish
 *  Alex Sexton
 *  Ryan Seddon
 *  Patrick Kettner
 *  Stu Cox
 *  Richard Herrera

 * MIT License
 */
!function(e,t,n){function o(e,t){return typeof e===t}function i(e){var t=S.className,n=w._config.classPrefix||"";if(T&&(t=t.baseVal),w._config.enableJSClass){var o=new RegExp("(^|\\s)"+n+"no-js(\\s|$)");t=t.replace(o,"$1"+n+"js$2")}w._config.enableClasses&&(t+=" "+n+e.join(" "+n),T?S.className.baseVal=t:S.className=t)}function r(e,t){if("object"==typeof e)for(var n in e)x(e,n)&&r(n,e[n]);else{e=e.toLowerCase();var o=e.split("."),s=w[o[0]];if(2==o.length&&(s=s[o[1]]),void 0!==s)return w;t="function"==typeof t?t():t,1==o.length?w[o[0]]=t:(!w[o[0]]||w[o[0]]instanceof Boolean||(w[o[0]]=new Boolean(w[o[0]])),w[o[0]][o[1]]=t),i([(t&&0!=t?"":"no-")+o.join("-")]),w._trigger(e,t)}return w}function s(){return"function"!=typeof t.createElement?t.createElement(arguments[0]):T?t.createElementNS.call(t,"http://www.w3.org/2000/svg",arguments[0]):t.createElement.apply(t,arguments)}function a(){var e=t.body;return e||(e=s(T?"svg":"body"),e.fake=!0),e}function l(e,n,o,i){var r,l,u,f,c="modernizr",d=s("div"),p=a();if(parseInt(o,10))for(;o--;)u=s("div"),u.id=i?i[o]:c+(o+1),d.appendChild(u);return r=s("style"),r.type="text/css",r.id="s"+c,(p.fake?p:d).appendChild(r),p.appendChild(d),r.styleSheet?r.styleSheet.cssText=e:r.appendChild(t.createTextNode(e)),d.id=c,p.fake&&(p.style.background="",p.style.overflow="hidden",f=S.style.overflow,S.style.overflow="hidden",S.appendChild(p)),l=n(d,e),p.fake?(p.parentNode.removeChild(p),S.style.overflow=f,S.offsetHeight):d.parentNode.removeChild(d),!!l}function u(e,t){return!!~(""+e).indexOf(t)}function f(e){return e.replace(/([a-z])-([a-z])/g,function(e,t,n){return t+n.toUpperCase()}).replace(/^-/,"")}function c(e,t){return function(){return e.apply(t,arguments)}}function d(e,t,n){var i;for(var r in e)if(e[r]in t)return!1===n?e[r]:(i=t[e[r]],o(i,"function")?c(i,n||t):i);return!1}function p(e){return e.replace(/([A-Z])/g,function(e,t){return"-"+t.toLowerCase()}).replace(/^ms-/,"-ms-")}function h(t,o){var i=t.length;if("CSS"in e&&"supports"in e.CSS){for(;i--;)if(e.CSS.supports(p(t[i]),o))return!0;return!1}if("CSSSupportsRule"in e){for(var r=[];i--;)r.push("("+p(t[i])+":"+o+")");return r=r.join(" or "),l("@supports ("+r+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return n}function m(e,t,i,r){function a(){c&&(delete q.style,delete q.modElem)}if(r=!o(r,"undefined")&&r,!o(i,"undefined")){var l=h(e,i);if(!o(l,"undefined"))return l}for(var c,d,p,m,y,v=["modernizr","tspan"];!q.style;)c=!0,q.modElem=s(v.shift()),q.style=q.modElem.style;for(p=e.length,d=0;d<p;d++)if(m=e[d],y=q.style[m],u(m,"-")&&(m=f(m)),q.style[m]!==n){if(r||o(i,"undefined"))return a(),"pfx"!=t||m;try{q.style[m]=i}catch(e){}if(q.style[m]!=y)return a(),"pfx"!=t||m}return a(),!1}function y(e,t,n,i,r){var s=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+E.join(s+" ")+s).split(" ");return o(t,"string")||o(t,"undefined")?m(a,t,i,r):(a=(e+" "+N.join(s+" ")+s).split(" "),d(a,t,n))}function v(e,t,o){return y(e,n,n,t,o)}var g=[],C=[],_={_version:"3.1.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,t){var n=this;setTimeout(function(){t(n[e])},0)},addTest:function(e,t,n){C.push({name:e,fn:t,options:n})},addAsyncTest:function(e){C.push({name:null,fn:e})}},w=function(){};w.prototype=_,w=new w;var b=_._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):[];_._prefixes=b;var x,S=t.documentElement,T="svg"===S.nodeName.toLowerCase();!function(){var e={}.hasOwnProperty;x=o(e,"undefined")||o(e.call,"undefined")?function(e,t){return t in e&&o(e.constructor.prototype[t],"undefined")}:function(t,n){return e.call(t,n)}}(),_._l={},_.on=function(e,t){this._l[e]||(this._l[e]=[]),this._l[e].push(t),w.hasOwnProperty(e)&&setTimeout(function(){w._trigger(e,w[e])},0)},_._trigger=function(e,t){if(this._l[e]){var n=this._l[e];setTimeout(function(){var e;for(e=0;e<n.length;e++)(0,n[e])(t)},0),delete this._l[e]}},w._q.push(function(){_.addTest=r});var k=s("input"),P="search tel url email datetime date month week time datetime-local number range color".split(" "),z={};w.inputtypes=function(e){for(var o,i,r,s=e.length,a=0;a<s;a++)k.setAttribute("type",o=e[a]),r="text"!==k.type&&"style"in k,r&&(k.value=":)",k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(o)&&k.style.WebkitAppearance!==n?(S.appendChild(k),i=t.defaultView,r=i.getComputedStyle&&"textfield"!==i.getComputedStyle(k,null).WebkitAppearance&&0!==k.offsetHeight,S.removeChild(k)):/^(search|tel)$/.test(o)||(r=/^(url|email|number)$/.test(o)?k.checkValidity&&!1===k.checkValidity():":)"!=k.value)),z[e[a]]=!!r;return z}(P);var j=_.testStyles=l;/*!
{
  "name": "Touch Events",
  "property": "touchevents",
  "caniuse" : "touch",
  "tags": ["media", "attribute"],
  "notes": [{
    "name": "Touch Events spec",
    "href": "http://www.w3.org/TR/2013/WD-touch-events-20130124/"
  }],
  "warnings": [
    "Indicates if the browser supports the Touch Events spec, and does not necessarily reflect a touchscreen device"
  ],
  "knownBugs": [
    "False-positive on some configurations of Nokia N900",
    "False-positive on some BlackBerry 6.0 builds – https://github.com/Modernizr/Modernizr/issues/372#issuecomment-3112695"
  ]
}
!*/
w.addTest("touchevents",function(){var n;if("ontouchstart"in e||e.DocumentTouch&&t instanceof DocumentTouch)n=!0;else{var o=["@media (",b.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");j(o,function(e){n=9===e.offsetTop})}return n}),/*!
{
  "name": "details Element",
  "caniuse": "details",
  "property": "details",
  "tags": ["elem"],
  "builderAliases": ["elem_details"],
  "authors": ["@mathias"],
  "notes": [{
    "name": "Mathias' Original",
    "href": "http://mths.be/axh"
  }]
}
!*/
w.addTest("details",function(){var e,t=s("details");return"open"in t&&(j("#modernizr details{display:block}",function(n){n.appendChild(t),t.innerHTML="<summary>a</summary>b",e=t.offsetHeight,t.open=!0,e=e!=t.offsetHeight}),e)});var A="Moz O ms Webkit",E=_._config.usePrefixes?A.split(" "):[];_._cssomPrefixes=E;var N=_._config.usePrefixes?A.toLowerCase().split(" "):[];_._domPrefixes=N;var L={elem:s("modernizr")};w._q.push(function(){delete L.elem});var q={style:L.elem.style};w._q.unshift(function(){delete q.style}),_.testAllProps=y,_.testAllProps=v,/*!
{
  "name": "CSS Transitions",
  "property": "csstransitions",
  "caniuse": "css-transitions",
  "tags": ["css"]
}
!*/
w.addTest("csstransitions",v("transition","all",!0)),function(){var e,t,n,i,r,s,a;for(var l in C)if(C.hasOwnProperty(l)){if(e=[],t=C[l],t.name&&(e.push(t.name.toLowerCase()),t.options&&t.options.aliases&&t.options.aliases.length))for(n=0;n<t.options.aliases.length;n++)e.push(t.options.aliases[n].toLowerCase());for(i=o(t.fn,"function")?t.fn():t.fn,r=0;r<e.length;r++)s=e[r],a=s.split("."),1===a.length?w[a[0]]=i:(!w[a[0]]||w[a[0]]instanceof Boolean||(w[a[0]]=new Boolean(w[a[0]])),w[a[0]][a[1]]=i),g.push((i?"":"no-")+a.join("-"))}}(),i(g),delete _.addTest,delete _.addAsyncTest;for(var $=0;$<w._q.length;$++)w._q[$]();e.Modernizr=w}(window,document);
//# sourceMappingURL=../../maps/vendor/modernizr/modernizr.development.js.map
