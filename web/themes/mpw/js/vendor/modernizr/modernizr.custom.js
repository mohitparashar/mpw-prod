/*!
 * modernizr v3.3.1
 * Build http://modernizr.com/download?-csstransitions-details-inputtypes-touchevents-addtest-prefixed-prefixes-setclasses-teststyles-dontmin
 *
 * Copyright (c)
 *  Faruk Ates
 *  Paul Irish
 *  Alex Sexton
 *  Ryan Seddon
 *  Patrick Kettner
 *  Stu Cox
 *  Richard Herrera

 * MIT License
 */
!function(e,t,n){function i(e,t){return typeof e===t}function r(e){var t=S.className,n=w._config.classPrefix||"";if(T&&(t=t.baseVal),w._config.enableJSClass){var i=new RegExp("(^|\\s)"+n+"no-js(\\s|$)");t=t.replace(i,"$1"+n+"js$2")}w._config.enableClasses&&(t+=" "+n+e.join(" "+n),T?S.className.baseVal=t:S.className=t)}function o(e,t){if("object"==typeof e)for(var n in e)x(e,n)&&o(n,e[n]);else{e=e.toLowerCase();var i=e.split("."),s=w[i[0]];if(2==i.length&&(s=s[i[1]]),void 0!==s)return w;t="function"==typeof t?t():t,1==i.length?w[i[0]]=t:(!w[i[0]]||w[i[0]]instanceof Boolean||(w[i[0]]=new Boolean(w[i[0]])),w[i[0]][i[1]]=t),r([(t&&0!=t?"":"no-")+i.join("-")]),w._trigger(e,t)}return w}function s(e){return e.replace(/([a-z])-([a-z])/g,function(e,t,n){return t+n.toUpperCase()}).replace(/^-/,"")}function a(){return"function"!=typeof t.createElement?t.createElement(arguments[0]):T?t.createElementNS.call(t,"http://www.w3.org/2000/svg",arguments[0]):t.createElement.apply(t,arguments)}function l(){var e=t.body;return e||(e=a(T?"svg":"body"),e.fake=!0),e}function u(e,n,i,r){var o,s,u,f,c="modernizr",p=a("div"),d=l();if(parseInt(i,10))for(;i--;)u=a("div"),u.id=r?r[i]:c+(i+1),p.appendChild(u);return o=a("style"),o.type="text/css",o.id="s"+c,(d.fake?d:p).appendChild(o),d.appendChild(p),o.styleSheet?o.styleSheet.cssText=e:o.appendChild(t.createTextNode(e)),p.id=c,d.fake&&(d.style.background="",d.style.overflow="hidden",f=S.style.overflow,S.style.overflow="hidden",S.appendChild(d)),s=n(p,e),d.fake?(d.parentNode.removeChild(d),S.style.overflow=f,S.offsetHeight):p.parentNode.removeChild(p),!!s}function f(e,t){return!!~(""+e).indexOf(t)}function c(e,t){return function(){return e.apply(t,arguments)}}function p(e,t,n){var r;for(var o in e)if(e[o]in t)return!1===n?e[o]:(r=t[e[o]],i(r,"function")?c(r,n||t):r);return!1}function d(e){return e.replace(/([A-Z])/g,function(e,t){return"-"+t.toLowerCase()}).replace(/^ms-/,"-ms-")}function h(t,i){var r=t.length;if("CSS"in e&&"supports"in e.CSS){for(;r--;)if(e.CSS.supports(d(t[r]),i))return!0;return!1}if("CSSSupportsRule"in e){for(var o=[];r--;)o.push("("+d(t[r])+":"+i+")");return o=o.join(" or "),u("@supports ("+o+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return n}function m(e,t,r,o){function l(){c&&(delete q.style,delete q.modElem)}if(o=!i(o,"undefined")&&o,!i(r,"undefined")){var u=h(e,r);if(!i(u,"undefined"))return u}for(var c,p,d,m,v,y=["modernizr","tspan"];!q.style;)c=!0,q.modElem=a(y.shift()),q.style=q.modElem.style;for(d=e.length,p=0;p<d;p++)if(m=e[p],v=q.style[m],f(m,"-")&&(m=s(m)),q.style[m]!==n){if(o||i(r,"undefined"))return l(),"pfx"!=t||m;try{q.style[m]=r}catch(e){}if(q.style[m]!=v)return l(),"pfx"!=t||m}return l(),!1}function v(e,t,n,r,o){var s=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+A.join(s+" ")+s).split(" ");return i(t,"string")||i(t,"undefined")?m(a,t,r,o):(a=(e+" "+N.join(s+" ")+s).split(" "),p(a,t,n))}function y(e,t,i){return v(e,n,n,t,i)}var g=[],C=[],_={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,t){var n=this;setTimeout(function(){t(n[e])},0)},addTest:function(e,t,n){C.push({name:e,fn:t,options:n})},addAsyncTest:function(e){C.push({name:null,fn:e})}},w=function(){};w.prototype=_,w=new w;var b=_._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];_._prefixes=b;var x,S=t.documentElement,T="svg"===S.nodeName.toLowerCase();!function(){var e={}.hasOwnProperty;x=i(e,"undefined")||i(e.call,"undefined")?function(e,t){return t in e&&i(e.constructor.prototype[t],"undefined")}:function(t,n){return e.call(t,n)}}(),_._l={},_.on=function(e,t){this._l[e]||(this._l[e]=[]),this._l[e].push(t),w.hasOwnProperty(e)&&setTimeout(function(){w._trigger(e,w[e])},0)},_._trigger=function(e,t){if(this._l[e]){var n=this._l[e];setTimeout(function(){var e;for(e=0;e<n.length;e++)(0,n[e])(t)},0),delete this._l[e]}},w._q.push(function(){_.addTest=o});var k=a("input"),P="search tel url email datetime date month week time datetime-local number range color".split(" "),z={};w.inputtypes=function(e){for(var i,r,o,s=e.length,a=0;a<s;a++)k.setAttribute("type",i=e[a]),o="text"!==k.type&&"style"in k,o&&(k.value="1)",k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(i)&&k.style.WebkitAppearance!==n?(S.appendChild(k),r=t.defaultView,o=r.getComputedStyle&&"textfield"!==r.getComputedStyle(k,null).WebkitAppearance&&0!==k.offsetHeight,S.removeChild(k)):/^(search|tel)$/.test(i)||(o=/^(url|email)$/.test(i)?k.checkValidity&&!1===k.checkValidity():"1)"!=k.value)),z[e[a]]=!!o;return z}(P);var j=_.testStyles=u;/*!
{
  "name": "Touch Events",
  "property": "touchevents",
  "caniuse" : "touch",
  "tags": ["media", "attribute"],
  "notes": [{
    "name": "Touch Events spec",
    "href": "https://www.w3.org/TR/2013/WD-touch-events-20130124/"
  }],
  "warnings": [
    "Indicates if the browser supports the Touch Events spec, and does not necessarily reflect a touchscreen device"
  ],
  "knownBugs": [
    "False-positive on some configurations of Nokia N900",
    "False-positive on some BlackBerry 6.0 builds – https://github.com/Modernizr/Modernizr/issues/372#issuecomment-3112695"
  ]
}
!*/
w.addTest("touchevents",function(){var n;if("ontouchstart"in e||e.DocumentTouch&&t instanceof DocumentTouch)n=!0;else{var i=["@media (",b.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");j(i,function(e){n=9===e.offsetTop})}return n}),/*!
{
  "name": "details Element",
  "caniuse": "details",
  "property": "details",
  "tags": ["elem"],
  "builderAliases": ["elem_details"],
  "authors": ["@mathias"],
  "notes": [{
    "name": "Mathias' Original",
    "href": "https://mathiasbynens.be/notes/html5-details-jquery#comment-35"
  }]
}
!*/
w.addTest("details",function(){var e,t=a("details");return"open"in t&&(j("#modernizr details{display:block}",function(n){n.appendChild(t),t.innerHTML="<summary>a</summary>b",e=t.offsetHeight,t.open=!0,e=e!=t.offsetHeight}),e)});var E="Moz O ms Webkit",A=_._config.usePrefixes?E.split(" "):[];_._cssomPrefixes=A;var L=function(t){var i,r=b.length,o=e.CSSRule;if(void 0===o)return n;if(!t)return!1;if(t=t.replace(/^@/,""),(i=t.replace(/-/g,"_").toUpperCase()+"_RULE")in o)return"@"+t;for(var s=0;s<r;s++){var a=b[s];if(a.toUpperCase()+"_"+i in o)return"@-"+a.toLowerCase()+"-"+t}return!1};_.atRule=L;var N=_._config.usePrefixes?E.toLowerCase().split(" "):[];_._domPrefixes=N;var O={elem:a("modernizr")};w._q.push(function(){delete O.elem});var q={style:O.elem.style};w._q.unshift(function(){delete q.style}),_.testAllProps=v;_.prefixed=function(e,t,n){return 0===e.indexOf("@")?L(e):(-1!=e.indexOf("-")&&(e=s(e)),t?v(e,t,n):v(e,"pfx"))};_.testAllProps=y,/*!
{
  "name": "CSS Transitions",
  "property": "csstransitions",
  "caniuse": "css-transitions",
  "tags": ["css"]
}
!*/
w.addTest("csstransitions",y("transition","all",!0)),function(){var e,t,n,r,o,s,a;for(var l in C)if(C.hasOwnProperty(l)){if(e=[],t=C[l],t.name&&(e.push(t.name.toLowerCase()),t.options&&t.options.aliases&&t.options.aliases.length))for(n=0;n<t.options.aliases.length;n++)e.push(t.options.aliases[n].toLowerCase());for(r=i(t.fn,"function")?t.fn():t.fn,o=0;o<e.length;o++)s=e[o],a=s.split("."),1===a.length?w[a[0]]=r:(!w[a[0]]||w[a[0]]instanceof Boolean||(w[a[0]]=new Boolean(w[a[0]])),w[a[0]][a[1]]=r),g.push((r?"":"no-")+a.join("-"))}}(),r(g),delete _.addTest,delete _.addAsyncTest;for(var $=0;$<w._q.length;$++)w._q[$]();e.Modernizr=w}(window,document);
//# sourceMappingURL=../../maps/vendor/modernizr/modernizr.custom.js.map
