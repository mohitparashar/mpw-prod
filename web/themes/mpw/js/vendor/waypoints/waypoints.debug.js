/*!
Waypoints Debug - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
!function(){"use strict";var i=["You have a Waypoint element with display none. For more information on ","why this is a bad idea read ","http://imakewebthings.com/waypoints/guides/debugging/#display-none"].join(""),o=["You have a Waypoint element with fixed positioning. For more ","information on why this is a bad idea read ","http://imakewebthings.com/waypoints/guides/debugging/#fixed-position"].join("");!function(){var n=window.Waypoint.Context.prototype.refresh;window.Waypoint.Context.prototype.refresh=function(){for(var e in this.waypoints)for(var t in this.waypoints[e]){var a=this.waypoints[e][t],r=window.getComputedStyle(a.element);a.enabled&&(r&&"none"===r.display&&console.error(i),r&&"fixed"===r.position&&console.error(o))}return n.call(this)}}()}();
//# sourceMappingURL=../../maps/vendor/waypoints/waypoints.debug.js.map
