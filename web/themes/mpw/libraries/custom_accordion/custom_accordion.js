/**
 * jQuery Custom Accordion Plugin v1.0
 * Author: Areg Mikoyan
 * Email: aregmk@gmail.com
 * License: Released under the MIT license
 * Date: 2015-11-22T14:11Z
 *
 * HTML Structure:
    <div class="cstm-acc">
        <div class="cstm-acc-item">
            <div class="cstm-acc-item-title">Item Title (can be any tag with class "cstm-acc-item-title" or define your custom class)</div>
            <div class="cstm-acc-item-body">Item body (can be any tag with class "cstm-acc-item-body" or define your custom class)</div>
        </div>
    </div>
 *
 *
 * Usage: 
    $('.cstm-acc').cstmAccordion();
    or
    custom_options = {}, // object with modified options
    $('.cstm-acc').cstmAccordion(custom_options);
 *
 *
 */
(function($, undefined) {
	$.fn.cstmAccordion = function(custom_options) {
        var acc = $(this),
            opts = {
                multiple: false,
                first_item_opened: true,
                item_selector: '.cstm-acc-item',
                item_title_selector: '.cstm-acc-item-title',
                item_body_selector: '.cstm-acc-item-body'
            };
        
        // merge custom options with defeult ones
        $.extend(opts, custom_options, true);
        
        // get all items
        var acc_items = acc.children(opts.item_selector);
        
        // show first item if needed
        if(opts.first_item_opened) acc_items.eq(0).addClass('cstm-acc-item-opened').children(opts.item_body_selector).slideDown();
        
        // bind click event handler for items
        acc_items.children(opts.item_title_selector).on('click', function(e) {
            e.preventDefault();
            
            var el = $(e.currentTarget),
                acc_item = el.parent();
            
            acc_item.toggleClass('cstm-acc-item-opened');
            el.next().slideToggle();
            
            if(!opts.multiple) acc_item.siblings().removeClass('cstm-acc-item-opened').children(opts.item_body_selector).slideUp();
        });
	}
})(jQuery);