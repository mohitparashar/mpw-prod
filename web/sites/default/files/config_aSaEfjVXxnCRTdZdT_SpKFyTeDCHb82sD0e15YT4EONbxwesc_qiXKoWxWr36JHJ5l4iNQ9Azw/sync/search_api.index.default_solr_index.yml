uuid: 6c982fcf-6a38-4a12-a0c8-05d34c05afaf
langcode: en
status: true
dependencies:
  config:
    - field.storage.node.field_address
    - field.storage.node.field_lat_long
    - field.storage.node.field_listing_type
    - field.storage.node.field_property_terms
    - field.storage.node.field_property_type
    - search_api.server.default_solr_server
  module:
    - search_api_solr
    - node
    - search_api_location
    - search_api
third_party_settings:
  search_api_solr:
    finalize: false
    commit_before_finalize: false
    commit_after_finalize: false
    highlighter:
      maxAnalyzedChars: 51200
      fragmenter: regex
      usePhraseHighlighter: true
      highlightMultiTerm: true
      preserveMulti: false
      regex:
        slop: 0.5
        pattern: blank
        maxAnalyzedChars: 10000
      highlight:
        mergeContiguous: false
        requireFieldMatch: false
        snippets: 3
        fragsize: 0
    mlt:
      mintf: 1
      mindf: 1
      maxdf: 0
      maxdfpct: 0
      minwl: 0
      maxwl: 0
      maxqt: 100
      maxntp: 2000
      boost: false
      interestingTerms: none
    term_modifiers:
      slop: 10000000
      fuzzy: 2
    advanced:
      index_prefix: ''
      collection: ''
      timezone: ''
    multilingual:
      limit_to_content_language: false
      include_language_independent: true
_core:
  default_config_hash: lcYnKd7EBuLnAdRQR3jS8E4SqChn3zAaFqUx9DdoTqA
id: default_solr_index
name: 'Default Solr content index'
description: 'Default content index created by the Solr Search Defaults module'
read_only: false
field_settings:
  address_line1:
    label: 'Address » The first line of the address block.'
    datasource_id: 'entity:node'
    property_path: 'field_address:address_line1'
    type: solr_text_wstoken
    boost: !!float 5
    dependencies:
      config:
        - field.storage.node.field_address
  administrative_area:
    label: 'Address » The top-level administrative subdivision of the country'
    datasource_id: 'entity:node'
    property_path: 'field_address:administrative_area'
    type: solr_text_wstoken
    dependencies:
      config:
        - field.storage.node.field_address
  dependent_locality:
    label: 'Address » The dependent locality (i.e. neighbourhood).'
    datasource_id: 'entity:node'
    property_path: 'field_address:dependent_locality'
    type: solr_text_wstoken
    boost: !!float 8
    dependencies:
      config:
        - field.storage.node.field_address
  field_lat_long:
    label: Lat/Long
    datasource_id: 'entity:node'
    property_path: field_lat_long
    type: location
    dependencies:
      config:
        - field.storage.node.field_lat_long
  field_listing_type:
    label: 'Listing Type'
    datasource_id: 'entity:node'
    property_path: field_listing_type
    type: string
    dependencies:
      config:
        - field.storage.node.field_listing_type
  field_property_terms:
    label: 'Property Terms'
    datasource_id: 'entity:node'
    property_path: field_property_terms
    type: text
    dependencies:
      config:
        - field.storage.node.field_property_terms
  field_property_type:
    label: 'Property Type'
    datasource_id: 'entity:node'
    property_path: field_property_type
    type: text
    dependencies:
      config:
        - field.storage.node.field_property_type
  langcode:
    label: 'Address » The language code.'
    datasource_id: 'entity:node'
    property_path: 'field_address:langcode'
    type: solr_text_wstoken
    dependencies:
      config:
        - field.storage.node.field_address
  lat:
    label: 'Lat/Long » Centroid Latitude'
    datasource_id: 'entity:node'
    property_path: 'field_lat_long:lat'
    type: integer
    dependencies:
      config:
        - field.storage.node.field_lat_long
  latlon:
    label: 'Lat/Long » LatLong Pair'
    datasource_id: 'entity:node'
    property_path: 'field_lat_long:latlon'
    type: string
    dependencies:
      config:
        - field.storage.node.field_lat_long
  locality:
    label: 'Address » The locality (i.e. city).'
    datasource_id: 'entity:node'
    property_path: 'field_address:locality'
    type: solr_text_wstoken
    boost: !!float 13
    dependencies:
      config:
        - field.storage.node.field_address
  lon:
    label: 'Lat/Long » Centroid Longitude'
    datasource_id: 'entity:node'
    property_path: 'field_lat_long:lon'
    type: integer
    dependencies:
      config:
        - field.storage.node.field_lat_long
  nid:
    label: ID
    datasource_id: 'entity:node'
    property_path: nid
    type: integer
    dependencies:
      module:
        - node
  node_grants:
    label: 'Node access information'
    property_path: search_api_node_grants
    type: string
    indexed_locked: true
    type_locked: true
    hidden: true
  organization:
    label: 'Address » The organization'
    datasource_id: 'entity:node'
    property_path: 'field_address:organization'
    type: solr_text_wstoken
    dependencies:
      config:
        - field.storage.node.field_address
  postal_code:
    label: 'Address » The postal code.'
    datasource_id: 'entity:node'
    property_path: 'field_address:postal_code'
    type: solr_text_wstoken
    boost: !!float 2
    dependencies:
      config:
        - field.storage.node.field_address
  status:
    label: 'Publishing status'
    datasource_id: 'entity:node'
    property_path: status
    type: boolean
    indexed_locked: true
    type_locked: true
    dependencies:
      module:
        - node
  title:
    label: Title
    datasource_id: 'entity:node'
    property_path: title
    type: solr_text_wstoken
    dependencies:
      module:
        - node
  uid:
    label: 'Author ID'
    datasource_id: 'entity:node'
    property_path: uid
    type: integer
    indexed_locked: true
    type_locked: true
    dependencies:
      module:
        - node
  url:
    label: URI
    property_path: search_api_url
    type: string
    configuration:
      absolute: false
datasource_settings:
  'entity:node':
    bundles:
      default: false
      selected:
        - property
    languages:
      default: false
      selected:
        - en
processor_settings:
  add_url:
    weights:
      preprocess_index: -30
  aggregated_field:
    weights:
      add_properties: 20
  content_access:
    weights:
      preprocess_index: -6
      preprocess_query: -4
  entity_status:
    weights:
      preprocess_index: -10
  highlight:
    weights:
      postprocess_query: 0
    prefix: '<strong>'
    suffix: '</strong>'
    excerpt: true
    excerpt_length: 256
    exclude_fields: {  }
    highlight: always
    highlight_partial: true
  html_filter:
    weights:
      preprocess_index: -3
      preprocess_query: -6
    all_fields: false
    fields: {  }
    title: true
    alt: true
    tags:
      b: 2
      h1: 5
      h2: 3
      h3: 2
      string: 2
  ignore_character:
    weights:
      preprocess_index: -10
      preprocess_query: -10
    all_fields: false
    fields:
      - address_line1
      - dependent_locality
      - locality
      - organization
      - postal_code
      - title
    ignorable: '[''¿¡!?,.:;,]'
    ignorable_classes:
      - Pc
      - Pd
      - Pe
      - Pf
      - Pi
      - Po
      - Ps
  language_with_fallback: {  }
  rendered_item:
    weights:
      add_properties: 0
      pre_index_save: -10
  solr_date_range:
    weights:
      preprocess_index: 0
tracker_settings:
  default:
    indexing_order: fifo
options:
  cron_limit: 50
  index_directly: true
  track_changes_in_references: true
server: default_solr_server
